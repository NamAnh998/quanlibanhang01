<!-- Footer -->
<footer class="sticky-footer bg-white">
    <div class="container my-auto">
        <div class="copyright text-center my-auto">
            <span>Copyright &copy; AUTO 368 - Nam Anh Design</span>
        </div>
    </div>
</footer>
<!-- End of Footer -->

</div>
<!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>

<!-- Logout Modal-->
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Bạn chắc chắn muốn đăng xuất?</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">Chọn nút đăng xuất bên dưới nếu bạn đã sẵn sàng kết thúc phiên làm việc của mình.</div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Hủy</button>
                <a class="btn btn-success" href="{{ route('logoutAdmin') }}">Đăng xuất</a>
            </div>
        </div>
    </div>
</div>

<!-- Bootstrap core JavaScript-->
<script src="assets/admin/vendor/jquery/jquery.min.js"></script>
<script src="assets/admin/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="https://printjs-4de6.kxcdn.com/print.min.js"></script>

<script src="{{ asset('plugins/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.3.1/js/dataTables.buttons.min.js"></script> 
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.html5.min.js"></script>
<!-- Core plugin JavaScript-->
<script src="assets/admin/vendor/jquery-easing/jquery.easing.min.js"></script>
<script type="text/javascript" src="{{ asset('plugins/moment/moment.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
@stack('js')
<!-- Custom scripts for all pages-->
<script src="assets/admin/js/sb-admin-2.min.js"></script>
<script src="assets/admin/js/ajax.js"></script>
<script src="assets/admin/js/toastr.min.js"></script>
<script type="text/javascript" src="ckeditor/ckeditor.js"></script>
<script type="text/javascript">
    CKEDITOR.replace( 'demo' );
</script>

@if(session('thongbao'))
    <script type="text/javascript">
        toastr.success('{{ session('thongbao') }}', 'Thông báo', {timeOut: 5000});
    </script>
@endif
@if(session('error'))
    <script type="text/javascript">
        toastr.error('{{ session('error') }}', 'Thông báo', {timeOut: 5000});
    </script>
@endif
<!-- Google chart-->
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
{{-- <script type="text/javascript">
    var arr = [['Ngày', 'Tổng tiền']];
    var orders = JSON.parse(document.getElementById("order").value);
    google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(drawChart);
    for(x of orders){
      arr.push([x.date,parseInt(x.total_price)])
    } 
    function drawChart() {

      var data = google.visualization.arrayToDataTable(arr);

      var options = {
        title: 'Thống kê doanh thu theo ngày'
      };

      var chart = new google.visualization.PieChart(document.getElementById('piechart'));

      chart.draw(data, options);
    }

    var option = [['Ngày', 'Tổng số đơn hàng']];
    var counts = JSON.parse(document.getElementById("count").value);
    google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(drawCountChart);
    for(x of counts){
      option.push([x.date,parseInt(x.count_order)])
    } 
    function drawCountChart() {

      var data = google.visualization.arrayToDataTable(option);

      var options = {
        title: 'Thống kê tổng số đơn hàng hoàn thành theo ngày'
      };

      var chart = new google.visualization.PieChart(document.getElementById('orderchart'));

      chart.draw(data, options);
    }
</script> --}}
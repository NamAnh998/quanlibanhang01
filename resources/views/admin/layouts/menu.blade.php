<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="/">
        <div class="sidebar-brand-icon rotate-n-15">
            <i class="fas fa-car"></i>
        </div>
        <div class="sidebar-brand-text mx-3">AUTO 368</div>
    </a>
    <!-- Divider -->
    @if(Auth::user()->role == 1)
    <hr class="sidebar-divider my-0">
    <!-- Nav Item - Dashboard -->
    <li class="nav-item">
        <a class="nav-link" href="{{ route('dashboard.index') }}">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Thống Kê</span>
        </a>
    </li>
    @endif
    @if(Auth::user()->role == 1)
    <!-- Divider -->
        <hr class="sidebar-divider">
        <!-- Heading -->
        <div class="sidebar-heading">
            Thành viên hệ thống
        </div>
        <!-- Nav Item - Pages Collapse Menu -->
        <li class="nav-item">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUser" aria-expanded="true" aria-controls="collapseTwo">
                <i class="fas fa-users"></i>
                <span>Thành viên</span>
            </a>
            <div id="collapseUser" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <h6 class="collapse-header">Thành viên</h6>
                    <a class="collapse-item" href="{{ route('user.getList') }}">Danh sách</a>
                        <a class="collapse-item" href="{{ route('user.create') }}">Thêm mới</a>
                </div>
            </div>
        </li>
    @endif
    @if(Auth::user()->role == 1 || Auth::user()->role == 2)
        <!-- Divider -->
        <hr class="sidebar-divider">
        <!-- Heading -->
        <div class="sidebar-heading">
            Quản lý sản phẩm
        </div>
        <!-- Nav Item - Pages Collapse Menu -->
        <li class="nav-item">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                <i class="fas fa-fw fa-cog"></i>
                <span>Nhóm sản phẩm</span>
            </a>
            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <h6 class="collapse-header">Nhóm sảm phẩm</h6>
                    <a class="collapse-item" href="{{ route('category.index') }}">Danh sách</a>
                    <a class="collapse-item" href="{{ route('category.create') }}">Thêm mới</a>
                </div>
            </div>
        </li>
        <li class="nav-item">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#producttype" aria-expanded="true" aria-controls="collapseTwo">
                <i class="fas fa-fw fa-cog"></i>
                <span> Nhà cung cấp</span>
            </a>
            <div id="producttype" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <h6 class="collapse-header"> Nhà cung cấp</h6>
                    <a class="collapse-item" href="{{ route('producttype.index') }}">Danh sách</a>
                    <a class="collapse-item" href="{{ route('producttype.create') }}">Thêm mới</a>
                </div>
            </div>
        </li>
    @endif
    @if(Auth::user()->role == 1 || Auth::user()->role == 2)
        <hr class="sidebar-divider">
        <!-- Heading -->
        <div class="sidebar-heading">
            Sản phẩm
        </div>
        <!-- Nav Item - Pages Collapse Menu -->
        <li class="nav-item">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#product" aria-expanded="true" aria-controls="collapseTwo">
                <i class="fas fa-fw fa-cog"></i>
                <span>Sản phẩm</span>
            </a>
            <div id="product" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <h6 class="collapse-header">Quản Lý Sảm Phẩm</h6>
                    <a class="collapse-item" href="{{ route('product.index') }}">Danh sách</a>
                    <a class="collapse-item" href="{{ route('product.create') }}">Thêm mới</a>
                </div>
            </div>
        </li>

        <hr class="sidebar-divider">
        <!-- Heading -->
        <div class="sidebar-heading">
            Đơn hàng
        </div>
        <!-- Nav Item - Dashboard -->
        <li class="nav-item">
            <a class="nav-link" href="{{ route('order.index') }}">
                <i class="far fa-money-bill-alt"></i>
                <span>Đơn đặt hàng</span>
            </a>
        </li>
        <hr class="sidebar-divider">
        @endif
        @if(Auth::user()->role == 1 || Auth::user()->role == 3)
        <!-- Heading -->
        <div class="sidebar-heading">
            Đơn bảo hành
        </div>
        <li class="nav-item">
            <a class="nav-link" href="{{ route('warranty.index') }}">
                <i class="fas fa-tools"></i>
                <span>Đơn bảo hành</span>
            </a>
        </li>
        @endif
</ul>
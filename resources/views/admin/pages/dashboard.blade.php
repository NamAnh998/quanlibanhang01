@extends('admin.layouts.master')

@section('title')
    Thống kê
@endsection

@section('content')
    <input type="hidden" id="order" value='{{ json_encode($order) }}' />
    <input type="hidden" id="count" value='{{ json_encode($count) }}' />

    {{-- <div id="piechart" style="width: 900px; height: 500px;"></div>
    <div id="orderchart" style="width: 900px; height: 500px;"></div> --}}

    <div class="row mb-4">
        <div class="col-md-6">
            <div class="widget widget-orange-light">
                <div>
                    <div class="number">{{ number_format($statistical['totalRevenues']) }} VNĐ</div>
                    <span>Tổng doanh thu</span>
                </div>
                <i class="fa fa-money-bill-wave icon"></i>
            </div>
        </div>
        <div class="col-md-6">
            <div class="widget widget-primary-light">
                <div>
                    <div class="number">{{ number_format($statistical['totalOrders']) }}</div>
                    <span>Tổng đơn hàng</span>
                </div>
                <i class="fa fa-shopping-bag icon"></i>
            </div>
        </div>
    </div>

    <div class="card shadown mb-3 form-filter-statistical">
        <form action="{{ route('dashboard') }}" method="GET" class="form-statistical">
            <div class="input-hidden d-none">
                <input type="hidden" value="{{ $start_date }}" id="start_date">
                <input type="hidden" value="{{ $end_date }}" id="end_date">
            </div>
            <div class="row">
                <div class="col-md-4">
                    <input type="text" class="form-control" id="daterange-statistical" name="date" placeholder="Select">
                </div>
                <div class="col-md-3">
                    <button type="submit" class="btn btn-success">
                        <i class="fa fa-search"></i>
                        Thực hiện
                    </button>
                </div>
            </div>
        </form>
    </div>

    <div class="card pd-20 mb-3">
        <div class="row">
            <div class="col-md-12 mb-3">
                <h5 class="d-flex align-items-center" style="justify-content: space-between">
                    <span>Doanh thu</span>
                    {{-- <button type="button" class="btn btn-warning" onclick="printRevenues()">
                        <i class="fa fa-file-pdf"></i>
                        Xuất báo cáo
                    </button> --}}
                </h5>
                <div style="max-height: 490px; overflow-y: auto">
                    <table class="table table-bordered table-hover table-striped datatable-revenues" id="table-revenues">
                        <thead>
                        <tr>
                            <th>STT</th>
                            <th>Ngày</th>
                            <th>Tổng doanh thu</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $i = 0; ?>
                        @foreach ($statistical['revenues'] as $day => $monney)
                            <?php $i++; ?>
                            <tr>
                                <td>{{ $i }}</td>
                                <td>{{ $day }}</td>
                                <td>{{ number_format($monney) }} VNĐ</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col-md-6">
                <h5 class="d-flex align-items-center" style="justify-content: space-between">
                    <span>Sản phẩm bán chạy</span>
                    {{-- <button type="button" class="btn btn-warning" onclick="printProductSellings()">
                        <i class="fa fa-file-pdf"></i>
                        Xuất báo cáo
                    </button> --}}
                </h5>
                <div class="div" style="max-height: 490px; overflow-y: auto">
                    <table class="table table-bordered table-hover table-striped datatable-productSellings">
                        <thead>
                        <tr>
                            <th>STT</th>
                            <th>Tên sản phẩm</th>
                            <th>Số lượng</th>
                            <th>Đơn giá</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $i = 0; ?>
                        @foreach ($statistical['productSellings'] as $item)
                            <?php $i++; ?>
                            <tr>
                                <td>{{ $i }}</td>
                                <td>
                                    <a href="{{ route('product-detail', ['slug' => $item['slug']]) }}" target="_blank">
                                        {{ $item['name'] }}
                                    </a>
                                </td>
                                <td>{{ $item['quantity'] }}</td>
                                <td>{{ number_format($item['price']) }} VNĐ</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col-md-6">
                <h5 class="d-flex align-items-center" style="justify-content: space-between">
                    <span>Sản phẩm tồn kho</span>
                    {{-- <button type="button" class="btn btn-warning" onclick="printProductStocks()">
                        <i class="fa fa-file-pdf"></i>
                        Xuất báo cáo
                    </button> --}}
                </h5>
                <div class="div" style="max-height: 490px; overflow-y: auto">
                    <table class="table table-bordered table-hover table-striped datatable-productStocks">
                        <thead>
                        <tr>
                            <th>STT</th>
                            <th>Tên sản phẩm</th>
                            <th>Số lượng</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $i = 0; ?>
                        @foreach ($statistical['productStocks'] as $item)
                            <?php $i++; ?>
                            <tr>
                                <td>{{ $i }}</td>
                                <td>
                                    <a href="{{ route('product-detail', ['slug' => $item->slug]) }}" target="_blank">
                                        {{ $item->name }}
                                    </a>
                                </td>
                                <td>{{ $item->quantity }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col-md-12 mb-3">
                <h5 class="d-flex align-items-center" style="justify-content: space-between">
                    <span>Đơn hàng</span>
                    {{-- <button type="button" class="btn btn-warning" onclick="printOrderList()">
                        <i class="fa fa-file-pdf"></i>
                        Xuất báo cáo
                    </button> --}}
                </h5>
                <div style="max-height: 500px; overflow-y: auto">
                    <table class="table table-bordered table-hover table-striped datatable-orderList">
                        <thead>
                        <tr>
                            <th>STT</th>
                            <th>Mã đơn hàng</th>
                            <th>Ngày tạo</th>
                            {{-- <th>Sản phẩm</th> --}}
                        </tr>
                        </thead>
                        <tbody>
                        <?php $i=0; ?>
                        @foreach ($statistical['orderList'] as $item)
                            <?php $i++; ?>
                            <tr>
                                <td>{{ $i }}</td>
                                <td>
                                    <a href="{{ route('order.show',['id' => $item->id]) }}">
                                        {{ $item->code_order }}
                                    </a>
                                </td>
                                <td>{{ date('d-m-Y', strtotime($item->created_at)) }}</td>
                                {{-- <td>
                                    @foreach ($item->orderDetails as $detail)
                                        <p>{{ $detail->name }}</p>
                                    @endforeach
                                </td> --}}
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    @include('admin.pages.includes.printRevenues', ['revenues' => $statistical['revenues']])
    @include('admin.pages.includes.printProductStocks', ['productStocks' => $statistical['productStocks']])
    @include('admin.pages.includes.printProductSellings', ['productSellings' => $statistical['productSellings']])
    @include('admin.pages.includes.printOrderList', ['orderList' => $statistical['orderList']])
@endsection

@push('js')
    <script>
        let start_date = $('#start_date').val()
        let end_date = $('#end_date').val()

        $('#daterange-statistical').daterangepicker(
            {
                ranges: {
                    'Hôm nay': [moment(), moment()],
                    'Hôm qua': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    '7 ngày trước': [moment().subtract(6, 'days'), moment()],
                    '30 ngày trước': [moment().subtract(29, 'days'), moment()],
                    'Tháng này': [moment().startOf('month'), moment().endOf('month')],
                    'Tháng trước': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                },
                startDate: start_date ? start_date : moment(),
                endDate  : end_date ? end_date : moment(),
                locale: {
                    "customRangeLabel": "Chọn khoảng thời gian",
                },
            },
            function (start, end) {
                // $('#daterange-statistical span').html(start.format('DD-MM-YYYY') + ' - ' + end.format('DD-MM-YYYY'))
            }
        )

        $('.range_inputs .applyBtn').text('Áp dụng')
        $('.range_inputs .cancelBtn').text('Hủy bỏ')

        $('.datatable-revenues').DataTable({
            dom: 'Bfrtip',
            buttons: [
                {
                    extend: 'excel',
                    text: 'Xuất Excel',
                    filename: 'Doanh thu'
                }
            ],
            pageLength: 500
        });
        $('.datatable-productSellings').DataTable({
            dom: 'Bfrtip',
            buttons: [
                {
                    extend: 'excel',
                    text: 'Xuất Excel',
                    filename: 'Sản phẩm bán chạy'
                }
            ],
            pageLength: 500
        });
        $('.datatable-productStocks').DataTable({
            dom: 'Bfrtip',
            buttons: [
                {
                    extend: 'excel',
                    text: 'Xuất Excel',
                    filename: 'Sản phẩm tồn kho'
                }
            ],
            pageLength: 500
        });
        $('.datatable-orderList').DataTable({
            dom: 'Bfrtip',
            buttons: [
                {
                    extend: 'excel',
                    text: 'Xuất Excel',
                    filename: 'Đơn hàng'
                }
            ],
            pageLength: 500
        });
    </script>
@endpush
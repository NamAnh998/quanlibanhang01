@extends('admin.layouts.master')

@section('title')
    {{ isset($user['id']) ? 'Cập nhật người dùng':'Thêm người dùng' }}
@endsection

@section('content')
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Người dùng</h6>
        </div>
        <div class="row" style="margin: 5px">
            <div class="col-lg-12">
                <form role="form" action="{{ route(isset($user['id']) ? 'user.update':'user.add')}}" method="post">
                    @csrf
                    @if (isset($user['id']))
                        <input type="hidden" name="id" value="{{ $user['id'] }}" />
                    @endif
                    <fieldset class="form-group">
                        <label>Tên người dùng</label>
                        <input type="text" class="form-control" name="name" placeholder="Nhập tên người dùng" value='{{ isset($user['id']) ? $user['name']:'' }}' required>
                    </fieldset>
                    <fieldset class="form-group">
                        <label>Email</label>
                        <input type="email" class="form-control" name="email" placeholder="Nhập email" value='{{ isset($user['id']) ? $user['email']:'' }}' required>
                    </fieldset>
                    <fieldset class="form-group">
                        <label>Mật khẩu</label>
                        <input type="password" class="form-control" name="pass" placeholder="Nhập mật khẩu" required>
                    </fieldset>
                    <div class="form-group">
                        <label>Vị trí</label>
                        <select class="form-control" name="role">
                            <option value="2" {{ isset($user['id']) && $user['role'] == 2 ? 'selected' :'' }}>Nhân viên bán hàng</option>
                            <option value="3" {{ isset($user['id']) && $user['role'] == 3 ? 'selected' :'' }}>Nhân viên kĩ thuật</option>
                            <option value="1" {{ isset($user['id']) && $user['role'] == 1 ? 'selected' :'' }}>Admin</option>
                        </select>
                    </div>
                    <button type="submit" class="btn btn-success">{{ isset($user['id']) ? 'Cập nhật':'Thêm' }}</button>
                    <button type="reset" class="btn btn-primary">Nhập lại</button>
                </form>
            </div>
        </div>
    </div>
@endsection
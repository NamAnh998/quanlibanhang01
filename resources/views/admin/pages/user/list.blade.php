@extends('admin.layouts.master')

@section('title')
    Danh sách người dùng
@endsection

@section('content')
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Danh sách người dùng</h6>
            @if(Auth::user()->role == 1)
                <a href="{{route('user.create')}}" class="btn btn-success float-right mr-3"> Thêm mới  <i class="fas fa-plus"></i></a>
            @endif
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th>STT</th>
                        <th>Tên người dùng</th>
                        <th>Email</th>
                        <th>Vị trí</th>
                        <th>Tùy chọn</th>
                    </tr>
                    </thead>
                    <tbody>
                        @php $count = 1 @endphp
                        @foreach($user as $item)
                            <tr>
                                <td>{{ $count }}</td>
                                <td>{{ $item['name'] }}</td>
                                <td>{{ $item['email'] }}</td>
                                <td>
                                    @if($item['role']==1)
                                        {{ "Admin" }}
                                    @elseif($item['role']==2)
                                        {{ "Nhân viên bán hàng" }}
                                    @elseif($item['role']==3)
                                        {{ "Nhân viên kĩ thuật" }}
                                    @else 
                                        {{ "Khách hàng" }}
                                    @endif
                                </td>
                                <td>
                                @if($item['role'] != 1)                  
                                    <a class="btn btn-primary edit" href="{{ route('user.edit',['id' => $item['id']]) }}" type="button"><i class="fas fa-edit"></i></a>
                                    <a class="btn btn-danger delete" href="{{ route('user.delete',['id' => $item['id']]) }}" type="button"><i class="fas fa-trash-alt"></i></a>
                                @endif
                                </td>
                            </tr>
                            @php $count++ @endphp
                        @endforeach
                    </tbody>
                </table>
                <div class="float-right">{{ $user->links() }}</div>
            </div>
        </div>
    </div>
@endsection
<div class="row" id="dashboard-statistical" style="display: none">
    <div class="col-md-12 mb-3">
        <h5>Doanh thu</h5>
        <table class="table table-bordered table-hover table-striped">
            <thead>
                <tr>
                    <th>STT</th>
                    <th>Ngày</th>
                    <th>Tổng doanh thu</th>
                </tr>
            </thead>
            <tbody>
                <?php $i = 0; ?>
                @foreach ($revenues as $day => $monney)
                    <?php $i++; ?>
                    <tr>
                        <td>{{ $i }}</td>
                        <td>{{ $day }}</td>
                        <td>{{ number_format($monney) }} VNĐ</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

    <div class="col-md-12">
        <h5>Sản phẩm bán chạy</h5>
        <table class="table table-bordered table-hover table-striped">
            <thead>
                <tr>
                    <th>STT</th>
                    <th>Tên sản phẩm</th>
                    <th>Số lượng</th>
                    <th>Đơn giá</th>
                </tr>
            </thead>
            <tbody>
                <?php $i = 0; ?>
                @foreach ($productSellings as $item)
                    <?php $i++; ?>
                    <tr>
                        <td>{{ $i }}</td>
                        <td>
                            <a href="{{ route('product-detail', ['slug' => $item['slug']]) }}" target="_blank">
                                {{ $item['name'] }}
                            </a>
                        </td>
                        <td>{{ $item['quantity'] }}</td>
                        <td>{{ number_format($item['price']) }} VNĐ</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

    <div class="col-md-12">
        <h5>Sản phẩm tồn kho</h5>
        <table class="table table-bordered table-hover table-striped">
            <thead>
                <tr>
                    <th>STT</th>
                    <th>Tên sản phẩm</th>
                    <th>Số lượng</th>
                    <th>Đơn giá</th>
                </tr>
            </thead>
            <tbody>
                <?php $i = 0; ?>
                @foreach ($productStocks as $item)
                    <?php $i++; ?>
                    <tr>
                        <td>{{ $i }}</td>
                        <td>
                            <a href="{{ route('product-detail', ['slug' => $item->slug]) }}" target="_blank">
                                {{ $item->name }}
                            </a>
                        </td>
                        <td>{{ $item->quantity }}</td>
                        <td>{{ number_format($item->price) }} VNĐ</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

    <div class="col-md-12 mb-3">
        <h5>Đơn hàng</h5>
        <table class="table table-bordered table-hover table-striped">
            <thead>
                <tr>
                    <th>STT</th>
                    <th>Mã đơn hàng</th>
                    <th>Sản phẩm</th>
                </tr>
            </thead>
            <tbody>
                <?php $i=0; ?>
                @foreach ($orderList as $item)
                    <?php $i++; ?>
                    <tr>
                        <td>{{ $i }}</td>
                        <td>{{ $item->code_order }}</td>
                        <td>
                            @foreach ($item->orderDetails as $detail)
                                <p>{{ $detail->name }}</p>
                            @endforeach
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
<div class="row" id="printProductSellings" style="display: none">
    <div class="col-md-12">
        <h5>Sản phẩm bán chạy</h5>
        <table class="table table-bordered table-hover table-striped">
            <thead>
                <tr>
                    <th>STT</th>
                    <th>Tên sản phẩm</th>
                    <th>Số lượng</th>
                    <th>Đơn giá</th>
                </tr>
            </thead>
            <tbody>
                <?php $i = 0; ?>
                @foreach ($productSellings as $item)
                    <?php $i++; ?>
                    <tr>
                        <td>{{ $i }}</td>
                        <td>
                            <a href="{{ route('product-detail', ['slug' => $item['slug']]) }}" target="_blank">
                                {{ $item['name'] }}
                            </a>
                        </td>
                        <td>{{ $item['quantity'] }}</td>
                        <td>{{ number_format($item['price']) }} VNĐ</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
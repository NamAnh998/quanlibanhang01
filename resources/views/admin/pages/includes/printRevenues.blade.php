<div class="row" id="printRevenues" style="display: none">
    <div class="col-md-12 mb-3">
        <h5>Doanh thu</h5>
        <table class="table table-bordered table-hover table-striped">
            <thead>
                <tr>
                    <th>STT</th>
                    <th>Ngày</th>
                    <th>Tổng doanh thu</th>
                </tr>
            </thead>
            <tbody>
                <?php $i = 0; ?>
                @foreach ($revenues as $day => $monney)
                    <?php $i++; ?>
                    <tr>
                        <td>{{ $i }}</td>
                        <td>{{ $day }}</td>
                        <td>{{ number_format($monney) }} VNĐ</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
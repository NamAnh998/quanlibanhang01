<div class="row" id="print-order" style="display: none">
    <div class="col-md-12">
        <div>
            <p><b> AUTO 368 </b></p>
            <p><b> Địa chỉ: 79 Dương Đình Nghệ - Cầu Giấy - Hà Nội</b></p>
            <p><b> Số điện thoại: 0989 888 999</b></p>
            <hr>
            <p>Khách hàng: {{ $order[0]->name }}</p>
            <p>Địa chỉ: {{ $order[0]->address }}</p>
            <p>Email: {{ $order[0]->email }}</p>
            <p>Số điện thoại: {{ $order[0]->phone }}</p>
            <p>Ghi chú: {{ $order[0]->message != '' ? $order[0]->message : 'Không có' }}</p>
            <p>Ngày thanh toán: {{ $order[0]->created_at }}</p>
        </div>
        <div class="table-responsive">
            <table class="table table-bordered"  id="dataTable" width="100%" cellspacing="0">
                <thead>
                <tr>
                    <th>STT</th>
                    <th>Sản phẩm</th>
                    <th>Số lượng</th>
                    <th>Đơn giá</th>
                    <th>Thành tiền</th>
                </tr>
                </thead>
                <tbody>
                    @php $count = 1; @endphp
                    @foreach ($order as $item)
                        <tr>
                            <td>{{ $count }}</td>
                            <td>{{ $item->p_name }}</td>
                            <td>{{ $item->qty }}</td>
                            <td>{{ number_format($item->price) }} VNĐ</td>
                            <td>{{ number_format($item->qty * $item->price) }} VNĐ</td>
                        </tr>
                        @php $count++; @endphp
                    @endforeach
                    <tr>
                        <td colspan="4" class="text-center">VAT</td>
                        <td>{{ number_format($vat) }} VNĐ (+10 %)</td>
                    </tr>
                    <tr>
                        <td colspan="4" class="text-center"><b>Tổng trị giá</b></td>
                        <td><b>{{ number_format($totalOrder) }} VNĐ</b></td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="d-flex" style="justify-content: space-between; padding: 10px 180px 50px 180px">
            <p>Người bán</p>
            <p>Người mua</p>
        </div>
    </div>
</div>
<div class="row" id="printOrderList" style="display: none">
    <div class="col-md-12">
        <h5>Đơn hàng</h5>
        <table class="table table-bordered table-hover table-striped">
            <thead>
                <tr>
                    <th>STT</th>
                    <th>Mã đơn hàng</th>
                    <th>Sản phẩm</th>
                </tr>
            </thead>
            <tbody>
                <?php $i=0; ?>
                @foreach ($orderList as $item)
                    <?php $i++; ?>
                    <tr>
                        <td>{{ $i }}</td>
                        <td>{{ $item->code_order }}</td>
                        <td>
                            @foreach ($item->orderDetails as $detail)
                                <p>{{ $detail->name }}</p>
                            @endforeach
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@extends('admin.layouts.master')

@section('title')
    Danh sách các đơn hàng
@endsection

@section('content')
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary"> Danh sách các đơn hàng</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered"  id="dataTable" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th>STT</th>
                        <th>Mã đơn hàng</th>
                        <th>Tổng tiền</th>
                        <th>Thời gian thanh toán</th>
                        <th>Trạng thái</th>
                        <th>Tùy chọn</th>
                    </tr>
                    </thead>
                    <tbody>
                        @php $count = 1; @endphp
                        @foreach ($orders as $item)
                            <tr>
                                <td>{{ $count }}</td>
                                <td>{{ $item['code_order'] }}</td>
                                <td>{{ number_format($item['monney']) }} VNĐ</td>
                                <td>{{ $item['created_at'] }}</td>
                                <td>
                                    @if($item['status'] === 0)
                                        <label class="label label-warning">Chờ xác nhận </label>
                                    @elseif($item['status'] === 1)
                                        <label class="label label-info">Xác nhận </label>
                                    @elseif($item['status'] === 3)
                                        <label class="label label-success">Hoàn thành </label>
                                    @else
                                        <label class="label label-danger">Đã hủy</label>
                                    @endif
                                </td>
                                <td>
                                    <div class="dropdown">
                                        <button class="btn btn-primary btn-sm dropdown-toggle" type="button" data-toggle="dropdown">Hành động
                                        <span class="caret"></span></button>
                                        <ul class="dropdown-menu dropdown-order-admin">
                                            <li><a href="{{ route('order.show',['id' => $item['id']]) }}">Xem chi tiết</a></li>
                                            {{-- @if ($item['status'] != 3)
                                                <li><a href="{{ route('order.check',['id' => $item['id']]) }}">Xác nhận đơn hàng</a></li>
                                                <li><a href="{{ route('order.complete',['id' => $item['id']]) }}"  onclick="return confirm('Bạn chắc chắn đơn hàng này đã hoàn thành?');">Hoàn thành</a></li>
                                                <li><a href="{{ route('order.cancle',['id' => $item['id']]) }}">Hủy đơn hàng</a></li>
                                            @endif --}}

                                            @if ($item['status'] === 0)
                                                <li><a href="{{ route('order.check',['id' => $item['id']]) }}">Xác nhận đơn hàng</a></li>
                                                <li><a href="{{ route('order.cancle',['id' => $item['id']]) }}">Hủy đơn hàng</a></li>
                                            @endif

                                            @if ($item['status'] === 1)
                                                <li><a href="{{ route('order.complete',['id' => $item['id']]) }}"  onclick="return confirm('Bạn chắc chắn đơn hàng này đã hoàn thành?');">Hoàn thành</a></li>
                                                <li><a href="{{ route('order.cancle',['id' => $item['id']]) }}">Hủy đơn hàng</a></li>
                                            @endif
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                            @php $count++; @endphp
                        @endforeach
                    </tbody>
                </table>
                <div class="float-right">{{ $orders->links() }}</div>
            </div>
        </div>
    </div>
@endsection
@extends('admin.layouts.master')

@php
    use Carbon\Carbon;
@endphp

@section('title')
    Danh sách các đơn bảo hành
@endsection

@section('content')
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary"> Danh sách các đơn bảo hành</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered"  id="dataTable" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th>STT</th>
                        <th>Mã đơn hàng</th>
                        <th>Email khách hàng</th>
                        <th>Tên khách hàng</th>
                        <th>Số điện thoại</th>
                        <th>Sản phẩm</th>
                        <th>Thời gian bảo hành còn lại</th>
                        <th>Lý do bảo hành</th>
                        <th>Trạng thái</th>
                        <th>Tùy chọn</th>
                    </tr>
                    </thead>
                    <tbody>
                        @php $count = 1; @endphp
                        @foreach ($warranty as $item)
                            <tr>
                                <td>{{ $count }}</td>
                                <td>{{ $item->code_order }}</td>
                                <td>{{ $item->email }}</td>
                                <td>{{ $item->u_name }}</td>
                                <td>{{ $item->phone }}</td>
                                <td>{{ $item->name }}</td>
                                <td>
                                    @php
                                        $today = new \DateTime(Carbon::now());
                                        $dateWarranty = $item->warranty_period;
                                        $daysOfWarranty = $today->diff(new DateTime($dateWarranty))->days;
                                    @endphp     
                                    @if ($item->warranty_period != '0000-00-00 00:00:00')
                                        {{ $daysOfWarranty }} ngày
                                    @endif
                                </td>
                                <td>{{ $item->reason }}</td>
                                <td>
                                    @if($item->status === 0)
                                        <label class="label label-warning">Chờ xác nhận </label>
                                    @elseif($item->status === 1)
                                        <label class="label label-info">Xác nhận </label>
                                    @elseif($item->status === 2)
                                        <label class="label label-success">Hoàn thành </label>
                                    @else
                                        <label class="label label-danger">Đã hủy</label>
                                    @endif
                                </td>
                                <td>
                                    <div class="dropdown">
                                        <button class="btn btn-primary btn-sm dropdown-toggle" type="button" data-toggle="dropdown">Hành động
                                        <span class="caret"></span></button>
                                        <ul class="dropdown-menu">
                                            @if($item->status == 0)
                                                <li><a href="{{ route('warranty.update',['id' => $item->id]) }}">Xác nhận</a></li>
                                            @endif
                                                @if($item->status == 1 || $item->status == 2)
                                                <li><a href="{{ route('warranty.complete',['id' => $item->id]) }}">Hoàn thành</a></li>
                                            @endif
                                            
                                            @if ($item->status == 0)
                                                <li><a href="{{ route('warranty.cancel',['id' => $item->id]) }}">Hủy bảo hành</a></li>
                                            @endif
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                            @php $count++; @endphp
                        @endforeach
                    </tbody>
                </table>
                <div class="float-right">{{ $warranty->links() }}</div>
            </div>
        </div>
    </div>
@endsection
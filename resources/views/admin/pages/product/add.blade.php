@extends('admin.layouts.master')

@section('title')
    Thêm mới sản phẩm
@endsection

@section('content')
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Thêm sản phẩm</h6>
        </div>
        <div class="row" style="margin: 5px">
            <div class="col-lg-12">
                <form role="form" action="{{ route('product.store') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <fieldset class="form-group">
                        <label>Tên sản phẩm</label>
                        <input class="form-control" name="name" placeholder="Nhập tên sản phẩm" value="{{ old('name') }}">
                        @if($errors->has('name'))
                            <span class="required">{{ $errors->first('name') }}</span>
                        @endif
                    </fieldset>
                    <div class="form-group">
                        <label for="quantity">Số lượng</label>
                        <input type="number" name="quantity" min="1" value="1" class="form-control" value="{{ old('quantity') }}">
                        @if($errors->has('quantity'))
                            <span class="required">{{ $errors->first('quantity') }}</span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="price">Đơn giá</label>
                        <input type="text" name="price" placeholder="Nhập đơn giá" class="form-control" value="{{ old('price') }}">
                        @if($errors->has('price'))
                            <span class="required">{{ $errors->first('price') }}</span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="price">Giá khuyến mại</label>
                        <input type="text" name="promotional" value="{{ old('promotional') }}" value="0" placeholder="Nhập giá khuyến mại nếu có" class="form-control">
                        @if($errors->has('promotional'))
                            <span class="required">{{ $errors->first('promotional') }}</span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="warranty">Thời gian bảo hành</label>
                        @php
                            $warranties = [
                                '0' => '3 tháng',
                                '1' => '6 tháng',
                                '2' => '1 năm',
                                '3' => '2 năm',
                                '4' => '3 năm',
                            ];
                        @endphp
                        <select name="warranty" id="warranty" class="form-control">
                            <option value="">---Lựa chọn---</option>
                            @foreach ($warranties as $key => $item)
                                <option value="{{ $key }}">{{ $item }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="price">Ảnh minh họa</label>
                        <input type="file" name="image" class="form-control" accept="image/*">
                        @if($errors->has('image'))
                            <span class="required">{{ $errors->first('image') }}</span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>Mô tả sản phẩm</label>
                        <textarea name="description" id="demo" cols="5" rows="5" class="form-control"></textarea>
                        @if($errors->has('description'))
                            <span class="required">{{ $errors->first('description') }}</span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>Nhóm sản phẩm</label>
                        <select class="form-control cateProduct" name="idCategory">
                            @foreach($category as $cate)
                                <option value="{{ $cate->id }}">{{ $cate->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Tên nhà cung cấp</label>
                        <select class="form-control proTypeProduct" name="idProductType">
                            @foreach($producttype as $pro)
                                <option value="{{ $pro->id }}">{{ $pro->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Trạng thái</label>
                        <select class="form-control" name="status">
                            <option value="1">Hiển Thị</option>
                            <option value="0">Không Hiển Thị</option>
                        </select>
                    </div>
                    <button type="submit" class="btn btn-success">Thêm</button>
                    <button type="reset" class="btn btn-primary">Nhập Lại</button>
                </form>
            </div>
        </div>
    </div>
@endsection
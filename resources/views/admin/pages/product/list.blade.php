@extends('admin.layouts.master')

@section('title')
    Danh sách các sản phẩm
@endsection

@section('content')
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary"> Danh sách các sản phẩm</h6>
            <a href="{{route('product.create')}}" class="btn btn-success float-right"> Thêm mới  <i class="fas fa-plus"></i></a>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered"  id="dataTable" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th>STT</th>
                        <th style="min-width: 135px">Tên sản phẩm</th>
                        <th style="min-width: 400px">Mô tả</th>
                        <th>Thông tin</th>
                        <th>Hình ảnh </th>
                        <th style="min-width: 190px">Nhóm sản phẩm</th>
                        <th style="min-width: 170px">Nhà cung cấp</th>
                        <th style="min-width: 190px">Thời gian bảo hành</th>
                        <th style="min-width: 150px">Trạng thái</th>
                        <th style="min-width: 120px">Tùy chọn</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($product as $key => $value)
                        <tr>
                            <td>{{ $key+1 }}</td>
                            <td>{{ $value->name }}</td>
                            <td>
                                <div class="description">
                                    {!! $value->description !!}
                                </div>
                            </td>
                            <td>
                                <b>Số lượng</b>: {{ $value->quantity }}
                                <br/>
                                <b>Đơn giá</b>: {{ number_format($value->price) }} VNĐ
                                <br/>
                                <b>Khuyến mại</b>: {{ number_format($value->promotional) }} VNĐ
                                <br/>
                            </td>
                            <td>
                                <img src="{{('/img/upload/product')}}{{ '/'.$value->image }}" width="100" height="100">
                            </td>
                            <td>{{ $value->categories->name }}</td>
                            <td>{{ $value->productTypes ? $value->productTypes->name : '' }}</td>
                            <td>
                                @php
                                    $warranties = [
                                        '0' => '3 tháng',
                                        '1' => '6 tháng',
                                        '2' => '1 năm',
                                        '3' => '2 năm',
                                        '4' => '3 năm',
                                    ];
                                @endphp
                                {{ $warranties[$value->warranty] }}
                            </td>
                            <td>
                                @if($value->status == 1)
                                    <label class="label label-success">Hiển thị</label>
                                @else
                                    <label class="label label-danger">Không Hiển Thị</label>
                                @endif
                            </td>
                            <td>
                                <button class="btn btn-primary editProduct" title="{{ "Sửa ".$value->name }}" data-toggle="modal" data-target="#edit" type="button" data-id="{{ $value->id }}"><i class="fas fa-edit"></i></button>
                                <button class="btn btn-danger deleteProduct" title="{{ "Xóa ".$value->name }}" data-toggle="modal" data-target="#delete" type="button" data-id="{{ $value->id }}"><i class="fas fa-trash-alt"></i></button>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            
            <div class="float-right mt-3">{{ $product->links() }}</div>
        </div>
    </div>
    
    @include('admin.pages.product.includes.modal')
@endsection
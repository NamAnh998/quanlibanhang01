<!-- Edit Modal-->
<div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Chỉnh sửa sản phẩm <span class="title"></span></h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body modal-edit-product">
                <div class="row" style="margin: 5px">
                    <div class="col-lg-12">
                        <form role="form" id="updateProduct" method="post" enctype="multipart/form-data">
                            <input type="hidden" name="id" class="idProduct">
                            <fieldset class="form-group">
                                <label>Tên sản phẩm</label>
                                <input class="form-control name" name="name" placeholder="Nhập tên của sản phẩm">
                                <div class="alert alert-danger errorName"></div>
                            </fieldset>
                            <div class="form-group">
                                <label for="quantity">Số lượng</label>
                                <input type="number" name="quantity" min="1" value="1" class="form-control quantity">
                                <div class="alert alert-danger errorQuantity"></div>
                            </div>
                            <div class="form-group">
                                <label for="price">Đơn giá</label>
                                <input type="text" name="price" placeholder="Nhập đơn giá" class="form-control price">
                                <div class="alert alert-danger errorPrice"></div>
                            </div>
                            <div class="form-group">
                                <label for="price">Giá khuyến mại</label>
                                <input type="text" name="promotional" value="0" placeholder="Nhập giá khuyến mại nếu có" class="form-control promotional">
                                <div class="alert alert-danger errorPromotional"></div>
                            </div>
                            <div class="form-group">
                                <label for="warranty">Thời gian bảo hành</label>
                                @php
                                    $warranties = [
                                        '0' => '3 tháng',
                                        '1' => '6 tháng',
                                        '2' => '1 năm',
                                        '3' => '2 năm',
                                        '4' => '3 năm',
                                    ];
                                @endphp
                                <select name="warranty" id="warranty" class="form-control">
                                    <option value="">---Lựa chọn---</option>
                                    @foreach ($warranties as $key => $item)
                                        <option value="{{ $key }}">{{ $item }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <img class="img img-thumbnail imageThum" width="100" height="100" lign="center">
                            <div class="form-group">
                                <label for="price">Ảnh minh họa</label>
                                <input type="file" name="image" class="form-control image" accept="image/*">
                                <div class="alert alert-danger errorImage"></div>
                            </div>
                            <div class="form-group">
                                <label>Mô tả sản phẩm</label>
                                <textarea name="description" id="demo" cols="5" rows="5" class="form-control description"></textarea>
                                <div class="alert alert-danger errorDescription"></div>
                            </div>
                            <div class="form-group">
                                <label>Nhóm sản phẩm</label>
                                <select class="form-control cateProduct" name="idCategory"></select>
                            </div>
                            <div class="form-group">
                                <label>Nhà cung cấp</label>
                                <select class="form-control proTypeProduct" name="idProductType"></select>
                            </div>
                            <div class="form-group">
                                <label>Trạng thái</label>
                                <select class="form-control status" name="status">
                                    <option value="1" class="ht">Hiển Thị</option>
                                    <option value="0" class="kht">Không Hiển Thị</option>
                                </select>
                            </div>
                            <input type="submit" class="btn btn-success" value="Sửa">
                            <button type="reset" class="btn btn-primary">Nhập Lại</button>
                            <button class="btn btn-secondary" type="button" data-dismiss="modal">Hủy bỏ</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- delete Modal-->
<div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Bạn có muốn xóa ?</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body" style="margin-left: 183px;">
                <button type="button" class="btn btn-success delProduct">Có</button>
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Không</button>
            </div>
        </div>
    </div>
</div>
<div class="navbar-inner sticky-menu" rel="">
	<div class="container">
		<nav class="navbar navbar-expand-lg navbar-light bg-light">
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
					aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav text-center mr-xl-5">
					<li class="nav-item active mr-lg-2 mb-lg-0 mb-2">
						<a class="nav-link" href="/">Trang Chủ
							<span class="sr-only">(current)</span>
						</a>
					</li>
					@foreach($category as $cate)
						<li class="nav-item dropdown mr-lg-2 mb-lg-0 mb-2">
							<a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								{{ $cate->name }}
							</a>
							@if (count($cate->productType)>0)
									<div class="dropdown-menu">
										@foreach($cate->productType as $protype)
											<a class="dropdown-item" href="{{ route('producttype.get-by-category', [
                            'slug' => $protype->slug,
                            'id' => $protype->id
                        ]) }}">{{ $protype->name }}</a>
										@endforeach
										{{--<div class="dropdown-divider"></div>--}}
									</div>
							@endif
						</li>
					@endforeach
					<li class="nav-item">
						<a class="nav-link" href="#contact">Liên hệ</a>
					</li>
				</ul>
			</div>
            @if (Session::has('thongbao'))
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    {{ Session::get('thongbao') }}
                </div>
            @elseif (Session::has('error'))
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    {{ Session::get('error') }}
                </div>
            @endif
		</nav>
	</div>
</div>
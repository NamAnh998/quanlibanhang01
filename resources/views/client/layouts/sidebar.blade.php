<div class="col-lg-3 mt-lg-0 mt-4 p-lg-0 sidebar">
    <div class="side-bar p-sm-4 p-3">
        <div class="search-hotel border-bottom py-2">
            <h3 class="agileits-sear-head mb-3">Tìm kiếm nhà sản xuất</h3>
            <form action="{{ route('search.type') }}" method="post" style="display: flex">
                @csrf
                <input type="search" placeholder="Nhập tên nhà sản xuất" name="q" required>
                <input type="submit" value=" ">
            </form>
        </div>
        <!-- reviews -->
        <div class="customer-rev border-bottom left-side py-2">
            <h3 class="agileits-sear-head mb-3">Đánh giá của khách hàng</h3>
            <ul>
                <li>
                    <a href="#">
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <span>5.0</span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <span>4.0</span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star-half"></i>
                        <span>3.5</span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <span>3.0</span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star-half"></i>
                        <span>2.5</span>
                    </a>
                </li>
            </ul>
        </div>
        <!-- //reviews -->
        <!-- electronics -->
        <div class="left-side border-bottom py-2">
            <h3 class="agileits-sear-head mb-3">Các danh mục sản phẩm</h3>
            <ul>
                @foreach ($category as $item)
                    <li>
                        <a class="span" href="{{ route('product.get-by-category', [
                            'slug' => $item->slug,
                            'id' => $item->id
                        ]) }}"
                            style="font-size: 15px"
                        >{{ $item->name }}</a>
                    </li>
                @endforeach
            </ul>
        </div>
        <!-- //electronics -->
        <!-- delivery -->
        <div class="left-side border-bottom py-2">
            <h3 class="agileits-sear-head mb-3">Chính sách đổi trả hàng</h3>
            <ul>
                <li>
                    <span class="span">Đổi trả hàng trong vòng 7 ngày với các lỗi đến từ nhà sản xuất</span>
                </li>
            </ul>
        </div>
        <!-- //delivery -->
    </div>
    <!-- //product right -->
</div>
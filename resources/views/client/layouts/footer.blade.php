<footer id="contact">
    <div class="footer-top-first">
        <div class="container py-md-5 py-sm-4 py-3">
            <!-- footer first section -->
            <h2 class="footer-top-head-w3l font-weight-bold mb-2">AUTO 368 :</h2>
            <p class="footer-main mb-4">
               Sự hài lòng của quý khách hàng là niềm vui và là thành công lớn nhất của công ty chúng tôi.!</p>
            <!-- //footer first section -->
            <!-- footer second section -->
            <div class="row w3l-grids-footer border-top border-bottom py-sm-4 py-3">
                <div class="col-md-4 offer-footer">
                    <div class="row">
                        <div class="col-4 icon-fot">
                            <i class="fas fa-dolly"></i>
                        </div>
                        <div class="col-8 text-form-footer">
                            <h3>Chất lượng phục vụ tốt nhất </h3>
                            <p>Khách hàng sẽ được đáp ứng mọi mong muốn của mình với sản phẩm</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 offer-footer my-md-0 my-4">
                    <div class="row">
                        <div class="col-4 icon-fot">
                            <i class="fas fa-shipping-fast"></i>
                        </div>
                        <div class="col-8 text-form-footer">
                            <h3>Giao hàng nhanh chóng</h3>
                            <p>Toàn quốc</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 offer-footer">
                    <div class="row">
                        <div class="col-4 icon-fot">
                            <i class="far fa-thumbs-up"></i>
                        </div>
                        <div class="col-8 text-form-footer">
                            <h3>Uy tín của chúng tôi</h3>
                            <p>Sự lựa chọn đúng đắn của bạn.!</p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- //footer second section -->
        </div>
    </div>
    <!-- footer third section -->
    <div class="w3l-middlefooter-sec">
        <div class="container py-md-5 py-sm-4 py-3">
            <div class="row footer-info w3-agileits-info">
                <div class="col-md-3 col-sm-6 footer-grids mt-md-0 mt-4">
                    <h3 class="text-white font-weight-bold mb-3">Liên hệ với chúng tôi</h3>
                    <ul>
                        <li class="mb-3">
                            <i class="fas fa-map-marker"></i> 79 Dương Đình Nghệ - Cầu Giấy - HN</li>
                        <li class="mb-3">
                            <i class="fas fa-mobile"></i> 024 8888 99999 </li>
                        <li class="mb-3">
                            <i class="fas fa-phone"></i> 09 8888 9999 </li>
                        <li class="mb-3">
                            <i class="fas fa-envelope-open"></i>
                            <a href="mailto:example@mail.com"> auto368@gmail.com</a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-3 col-sm-6 footer-grids w3l-agileits mt-md-0 mt-4">
                    <!-- newsletter -->
                    <h3 class="text-white font-weight-bold mb-3">Những tin tức mới nhất</h3>
                    <p class="mb-3">Cập nhật thường xuyên về các sản phẩm mới</p>
                    <form action="#" method="post">
                        <div class="form-group">
                            <input type="email" class="form-control" placeholder="Email" name="email" required="">
                            <input type="submit" value="Nhập">
                        </div>
                    </form>
                    <!-- //newsletter -->
                    <!-- social icons -->
                    <div class="footer-grids  w3l-socialmk mt-3">
                        <h3 class="text-white font-weight-bold mb-3">Theo dõi </h3>
                        <div class="social">
                            <ul>
                                <li>
                                    <a class="icon fb" href="#">
                                        <i class="fab fa-facebook-f"></i>
                                    </a>
                                </li>
                                <li>
                                    <a class="icon tw" href="#">
                                        <i class="fab fa-twitter"></i>
                                    </a>
                                </li>
                                <li>
                                    <a class="icon gp" href="#">
                                        <i class="fab fa-google-plus-g"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!-- //social icons -->
                </div>
            </div>
            <!-- //quick links -->
        </div>
    </div>
    <!-- //footer third section -->
</footer>
<div class="copy-right py-3">
    <div class="container">
        <p class="text-center text-white">© 2021 AUTO 368 All rights reserved | Design by
            <a href="/"> Nam Anh</a>
        </p>
    </div>
</div>
<script src="{{ asset('assets/client/js/jquery-2.2.3.min.js') }}"></script>
<script src="{{ asset('assets/client/js/jquery.magnific-popup.js') }}"></script>
<script src="{{ asset('assets/client/js/minicart.js') }}"></script>
<!-- //cart-js -->
<!-- scroll seller -->
<script src="{{ asset('assets/client/js/scroll.js') }}"></script>
<!-- //scroll seller -->
<!-- smoothscroll -->
{{-- <script src="{{ asset('assets/client/js/SmoothScroll.min.js') }}"></script> --}}
<!-- //smoothscroll -->
<!-- start-smooth-scrolling -->
<script src="{{ asset('assets/client/js/move-top.js') }}"></script>
<script src="{{ asset('assets/client/js/easing.js') }}"></script>
<!-- for bootstrap working -->
<script src="{{ asset('assets/client/js/bootstrap.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/client/js/custom.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/client/js/toastr.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/client/js/ajax.js') }}"></script>

<!-- imagezoom -->
<script src="{{ asset('assets/client/js/imagezoom.js') }}"></script>
<!-- //imagezoom -->
<!-- flexslider -->
<link rel="stylesheet" href="{{ asset('assets/client/css/flexslider.css') }}" type="text/css" media="screen" />

<script src="{{ asset('assets/client/js/jquery.flexslider.js') }}"></script>
<script>
    // Can also be used with $(document).ready()
    $(window).load(function () {
        $('.flexslider').flexslider({
            animation: "slide",
            controlNav: "thumbnails"
        });
    });
</script>
<!-- //FlexSlider-->
<!-- smoothscroll -->
<script src="{{ asset('assets/client/js/SmoothScroll.min.js') }}"></script>
<!-- //smoothscroll -->
@if( isset($user) && count($user->customer) == 0 )
    <script>
        $('#address').modal('show');
    </script>
@endif

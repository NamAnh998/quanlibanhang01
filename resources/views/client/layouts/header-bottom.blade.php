<div class="header-bot">
    <div class="container">
        <div class="row header-bot_inner_wthreeinfo_header_mid">
            <!-- logo -->
            <div class="col-md-3 logo_agile">
                <a href="/">
                    <img src="{{ asset('assets/client/images/logo2.png') }}" alt="" width=150>
                </a>
            </div>
            <!-- //logo -->
            <!-- header-bot -->
            <div class="col-md-9 header mt-4 mb-md-0 mb-4">
                <div class="row">
                    <!-- search -->
                    <div class="col-10 agileits_search">
                        <form class="form-inline" action="{{ route('search.name') }}" method="post">
                            @csrf
                            <input class="form-control mr-sm-2" name="q" type="search" placeholder="Nhập tên sản phẩm mà bạn muốn tìm kiếm" aria-label="Search" required>
                            <button class="btn my-2 my-sm-0" type="submit">Tìm kiếm</button>
                        </form>
                    </div>
                    <!-- //search -->
                    <!-- cart details -->
                    <div class="col-2 top_nav_right text-center mt-sm-0 mt-2">
                        <div class="wthreecartaits wthreecartaits2 cart cart box_1" >
                                <a @if(Auth::check())  href=" {{ route('cart.index') }} " @else data-toggle="modal" data-target="#login" href="#"
                                   @endif title="Bạn đã chọn  {{ Cart::count() }} mặt hàng" class="btn w3view-cart">
                                    <i class="fas fa-cart-arrow-down"></i>
                                </a>
                        </div>
                    </div>
                    <!-- //cart details -->
                </div>
            </div>
        </div>
    </div>
</div>
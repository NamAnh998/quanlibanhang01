<div class="agile-main-top">
    <div class="container-fluid">
        <div class="row main-top-w3l py-2">
            <div class="col-lg-6 header-most-top">
                <p class="text-white text-lg-left text-center">79 Dương Đình Nghệ - Cầu Giấy - Hà Nội
                    <i class="fas fa-phone mr-2"></i> 0989 888 999
                </p>
            </div>
            <div class="col-lg-6 header-right mt-lg-0 mt-2">
                <!-- header lists -->
                <ul>
                    @if(Auth::check())
                    <li class="text-center border-right text-white">
                        <a href="{{ route('myorder') }}" class="text-white">
                            <i class="fas fa-truck mr-2"></i>Xem các đơn hàng</a>
                    </li>
                    @endif
                    @if(Auth::check())
                        <li class="text-center border-right text-white">
                                <a href="{{ route('logout') }}" style="color: #ff4d4d;">
                                    <i class="fas fa-sign-in-alt mr-2"></i>
                                </a>
                                {{ Auth::user()->name }}
                        </li>
                    @else
                    <li class="text-center border-right text-white">
                        <a href="#" data-toggle="modal" data-target="#login" class="text-white">
                            <i class="fas fa-sign-in-alt mr-2"></i> Đăng Nhập </a>
                    </li>
                    <li class="text-center text-white">
                        <a href="#" data-toggle="modal" data-target="#register" class="text-white">
                            <i class="fas fa-sign-out-alt mr-2"></i>Đăng ký </a>
                    </li>
                    @endif
                </ul>
                <!-- //header lists -->
            </div>
        </div>
    </div>
</div>
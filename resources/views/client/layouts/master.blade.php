
<!DOCTYPE html>
<html lang="vi">
<head>
    <title> AUTO 368 - @yield('title') </title>
    <!-- Meta tag Keywords -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="UTF-8" />
    <meta name="keywords" content="Electro Store Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- //Meta tag Keywords -->
    <!-- Custom-Files -->
    <link href="{{ asset('assets/client/css/bootstrap.css') }}" rel="stylesheet" type="text/css" media="all" />
    <!-- Bootstrap css -->
    <link href="{{ asset('assets/client/css/style.css') }}" rel="stylesheet" type="text/css" media="all" />
    <!-- Main css -->
    <link href="{{ asset('assets/admin/vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">
    <!-- Font-Awesome-Icons-CSS -->
    <link href="{{ asset('assets/client/css/popuo-box.css') }}" rel="stylesheet" type="text/css" media="all" />
    <!-- pop-up-box -->
    <link href="{{ asset('assets/client/css/menu.css') }}" rel="stylesheet" type="text/css" media="all" />
    <!-- menu style -->
    <!-- //Custom-Files -->
    <!-- web fonts -->
    <link href="{{ asset('assets/client/css/lato.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/client/css/opensan.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/admin/js/toastr.min.js') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/client/css/easy-responsive-tabs.css') }}">
    <link rel="stylesheet" href="{{ asset('css/common.css') }}">

    <!-- //web fonts -->
</head>
<body>
 @include('client.layouts.header-top')
<!-- top-header -->
<!-- modals -->
<!-- log in -->
<div class="modal fade" id="login" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title text-center">Đăng Nhập</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ route('login') }}" method="post">
                    @csrf
                    <div class="form-group">
                        <label class="col-form-label">Tên đăng nhập</label>
                        <input type="text" class="form-control" placeholder="Tên đăng nhập " name="name">
                    </div>
                    <div class="form-group">
                        <label class="col-form-label">Mật khẩu</label>
                        <input type="password" class="form-control" placeholder="Mật khẩu " name="Password" required="">
                    </div>
                    <div class="right-w3l">
                        <input type="submit" class="form-control" value="Đăng Nhập">
                    </div>
                    <div class="sub-w3l">
                        <div class="custom-control custom-checkbox mr-sm-2">
                            <input type="checkbox" class="custom-control-input" id="customControlAutosizing" name="remember">
                            <label class="custom-control-label" for="customControlAutosizing">Lưu thông tin đăng nhập</label>
                        </div>
                    </div>
                    <p class="text-center dont-do mt-3">Bạn chưa có tài khoản?
                        <a href="#" data-toggle="modal" data-target="#register">
                            Đăng ký ngay</a>
                    </p>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- register -->
<div class="modal fade" id="register" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Đăng ký</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ route('register') }}" method="post">
                    @csrf
                    <div class="form-group">
                        <label class="col-form-label">Họ và tên</label>
                        <input type="text" class="form-control" placeholder="Nhập họ và tên " name="name" required="">
                        @if($errors->has('name'))
                            <div class="alert alert-danger">
                                {{$errors->first('name') }}
                            </div>
                        @endif
                    </div>
                    <div class="form-group">
                        <label class="col-form-label">Email</label>
                        <input type="email" class="form-control" placeholder=" Nhập Email" name="email" required="">
                        @if($errors->has('email'))
                            <div class="alert alert-danger">
                                {{$errors->first('email') }}
                            </div>
                        @endif
                    </div>
                    <div class="form-group">
                        <label class="col-form-label">Mật khẩu</label>
                        <input type="password" class="form-control" placeholder="Nhập mật khẩu " name="password" id="password1" required="">
                        @if($errors->has('password'))
                            <div class="alert alert-danger">
                                {{$errors->first('password') }}
                            </div>
                        @endif
                    </div>
                    <div class="form-group">
                        <label class="col-form-label">Nhập lại mật khẩu</label>
                        <input type="password" class="form-control" placeholder="Nhập lại mật khẩu " name="re_password" id="password2" required="">
                        @if($errors->has('re_password'))
                            <div class="alert alert-danger">
                                {{$errors->first('re_password') }}
                            </div>
                        @endif
                    </div>
                    <div class="right-w3l">
                        <input type="submit" class="form-control" value="Đăng ký">
                    </div>
                    <div class="sub-w3l">
                        <div class="custom-control custom-checkbox mr-sm-2">
                            <input type="checkbox" class="custom-control-input" id="customControlAutosizing2">
                            <label class="custom-control-label" for="customControlAutosizing2">Đồng ý với điều khoản của chúng tôi</label>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- //modal -->
<!-- //top-header -->
<!-- header-bottom-->
 @include('client.layouts.header-bottom')
<!-- shop locator (popup) -->
<!-- //header-bottom -->
<!-- navigation -->
@include('client.layouts.menu')
<!-- //navigation -->
<!-- banner -->
@yield('client.layouts.slide')
<!-- //banner -->
<!-- top Products -->
@yield('slider')
<div class="ads-grid py-sm-5 py-4">
    <div class="container py-xl-4 py-lg-2">
        @yield('content')
    </div>
</div>
<!-- //top products -->
<!-- footer -->
@include('client.layouts.footer')
</body>
</html>
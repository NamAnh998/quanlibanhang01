@extends('client.layouts.master')

@section('title')
    Hóa đơn của tôi
@endsection

@section('content')
<h2>Lịch sử mua hàng của tôi</h2>      
<table class="table table-bordered">
    <thead>
    <tr>
        <th>STT</th>
        <th>Mã đơn hàng</th>
        <th>Tổng tiền</th>
        <th>Thời gian thanh toán</th>
        <th>Trạng thái</th>
        <th>Tùy chọn</th>
    </tr>
    </thead>
    <tbody>
        @php $count = 1; @endphp
        @foreach ($orders as $item)
            <tr>
                <td>{{ $count }}</td>
                <td>{{ $item->code_order }}</td>
                <td>{{ number_format($item->monney) }} VNĐ</td>
                <td>{{ $item->created_at }}</td>
                <td>@php
                if($item->status == 0){
                    echo 'Chờ thanh toán';
                }elseif($item->status == 1){
                    echo 'Đã thanh toán';
                }elseif($item->status == 3){
                    echo 'Hoàn thành';
                }else{
                    echo 'Hủy';
                }
            @endphp</td>
                <td>
                    <div class="dropdown">
                        <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                          Chức năng
                        </button>
                        <div class="dropdown-menu">
                          <a class="dropdown-item" href="{{ route('myorder.show',['id' => $item->id ]) }}">Xem chi tiết</a>
                          @if ($item->status == 3)
                            <a class="dropdown-item" href="{{ route('myorder.warranty',['id' => $item->id ]) }}">Bảo hành</a>
                          @endif
                        </div>
                    </div>
                </td>
            </tr>
            @php $count++; @endphp
        @endforeach
    </tbody>
</table>
@endsection
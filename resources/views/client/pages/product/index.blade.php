@extends('client.layouts.master')
@section('title')
    Trang Chủ
@endsection

@section('slide')
    @include('client.layouts.slide')
@endsection

@section('content')
    <!-- //tittle heading -->
    <div class="row">
        <!-- product left -->
        <div class="agileinfo-ads-display col-lg-9">
            <div class="wrapper">
                <!-- first section -->
                <div class="product-sec1 px-sm-4 px-3 py-sm-5  py-3 mb-4">
                    <h3 class="heading-tittle text-center font-italic">{{ $categoryName }}</h3>
                    <div class="row">
                        @if(isset($products))
                            @foreach($products as $pro)
                                <div class="col-md-4 product-men mt-5">
                                    <div class="men-pro-item simpleCart_shelfItem">
                                        <div class="men-thumb-item text-center">
                                            <img src="/img/upload/product/{{ $pro->image }}" class="img-fluid" alt=" {{ $pro->name }}">
                                            <div class="men-cart-pro">
                                                <div class="inner-men-cart-pro">
                                                    <a href="{{ route('product-detail', $pro->slug) }}" class="link-product-add-cart">Chi tiết</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="item-info-product text-center border-top mt-4">
                                            <h4 class="pt-1">
                                                <a href="{{ route('product-detail', $pro->slug) }}">{{ $pro->name }}</a>
                                            </h4>
                                            <div class="info-product-price my-2">
                                                @if($pro->promotional>0)
                                                    <span class="item_price">
                                                        {{ number_format($pro->promotional) }} VNĐ
                                                    </span>
                                                    <del>{{ number_format($pro->price) }} VNĐ</del>
                                                @else
                                                    <span class="item_price">
                                                        {{ number_format($pro->price) }} VNĐ
                                                    </span>
                                                @endif
                                            </div>
                                            <div class="snipcart-details top_brand_home_details item_add single-item hvr-outline-out">
                                                <a href="{{ route('addCart', ['id' => $pro->id]) }}"> Thêm vào giỏ hàng</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @endif
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="float-right">
                            {{ $products->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- //product left -->
        <!-- product right -->
        @include('client.layouts.sidebar')
    </div>
@endsection
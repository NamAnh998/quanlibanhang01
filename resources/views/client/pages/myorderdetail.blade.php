@extends('client.layouts.master')

@section('title')
    Chi tiết hóa đơn
@endsection

@section('content')
<h2>#{{ $orders[0]->code_order }}</h2>      
<table class="table table-bordered">
    <tr>
      <th>STT</th>
      <th>Sản phẩm</th>
      <th>Số lượng</th>
      <th>Đơn giá</th>
      <th>Thành tiền</th>
    </tr>
    @php $count = 1; @endphp
    @foreach ($orders as $item)
        <tr>
            <td>{{ $count }}</td>
            <td>{{ $item->p_name }}</td>
            <td>{{ $item->qty }}</td>
            <td>{{ number_format($item->price) }} VNĐ</td>
            <td>{{ number_format($item->price * $item->qty) }} VNĐ</td>
        </tr>
        @php $count++; @endphp
    @endforeach
    @php
        $total = 0;
        foreach ($orders as $item) {
            $total += $item->qty * $item->price;
        }
        $vat = ($total * 10) / 100;
        $sum = $total + $vat;
    @endphp
    <tr>
        <td colspan="4" style="text-align: center;">
            VAT
        </td>
        <td>{{ number_format($vat) }} VNĐ (+10%)</td>
    </tr>
    <tr>
        <td colspan="4" style="text-align: center;">
            Tổng thanh toán
        </td>
        <td>{{ number_format($sum) }} VNĐ</td>
    </tr>
</table>
@endsection
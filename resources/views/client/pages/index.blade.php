@extends('client.layouts.master')
@section('title')
    Trang Chủ
@endsection

@section('slide')
    @include('client.layouts.slide')
@endsection

@section('slider')
    @include('client.pages.slider')
@endsection

@section('content')
    <h3 class="tittle-w3l text-center mb-lg-5 mb-sm-4 mb-3">
        <span>C</span>ác
        <span>S</span>ản
        <span>P</span>hẩm
        

    <!-- //tittle heading -->
    <div class="row">
        <!-- product left -->
        <div class="agileinfo-ads-display col-lg-9">
            <div class="wrapper">
                <!-- first section -->
                <div class="product-sec1 px-sm-4 px-3 mb-4 pt-3">
                    <h3 class="heading-tittle text-center font-italic">{{ $proNew[0]->categories->name ?? ''}}</h3>
                    <div class="row">
                        @if(isset($proNew))
                            @foreach($proNew as $pro)
                                <div class="col-md-4 product-men mt-5">
                                    <div class="men-pro-item simpleCart_shelfItem">
                                        <div class="men-thumb-item text-center">
                                            <img src="/img/upload/product/{{ $pro->image }}" class="img-fluid" alt=" {{ $pro->name }}">
                                            <div class="men-cart-pro">
                                                <div class="inner-men-cart-pro">
                                                    <a href="{{ $pro->slug }}.html" class="link-product-add-cart">Chi tiết</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="item-info-product text-center border-top mt-4">
                                            <h4 class="pt-1">
                                                <a href="{{ $pro->slug }}.html">{{ $pro->name }}</a>
                                            </h4>
                                            <div class="info-product-price my-2">
                                                @if($pro->promotional>0)
                                                    <span class="item_price">
                                                        {{ number_format($pro->promotional) }} VNĐ
                                                    </span>
                                                    <del>{{ number_format($pro->price) }} VNĐ</del>
                                                @else
                                                    <span class="item_price">
                                                        {{ number_format($pro->price) }} VNĐ
                                                    </span>
                                                @endif
                                            </div>
                                            <div class="snipcart-details top_brand_home_details item_add single-item hvr-outline-out">
                                                <a href="{{ route('addCart', ['id' => $pro->id]) }}"> Thêm vào giỏ hàng</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach

                            <div class="btn-read-more">
                                <a target="_blank" href="{{ route('product.get-by-category', [
                                    'slug' => $proNew[0]->categories->slug,
                                    'id' => $proNew[0]->categories->id
                                ]) }}"> Xem tất cả</a>
                            </div>
                        @endif
                    </div>
                </div>
                <!-- //first section -->
                <!-- second section -->
                <div class="product-sec1 px-sm-4 px-3 mb-4 pt-3">
                    <h3 class="heading-tittle text-center font-italic">{{ $proUsed[0]->categories->name ?? '' }}</h3>
                    <div class="row">
                        @if(isset($proUsed))
                            @foreach($proUsed as $pro)
                                <div class="col-md-4 product-men mt-5">
                                    <div class="men-pro-item simpleCart_shelfItem">
                                        <div class="men-thumb-item text-center">
                                            <img src="img/upload/product/{{ $pro->image }}" class="img-fluid" alt=" {{ $pro->name }}">
                                            <div class="men-cart-pro">
                                                <div class="inner-men-cart-pro">
                                                    <a href="{{ $pro->slug }}.html" class="link-product-add-cart">Chi tiết</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="item-info-product text-center border-top mt-4">
                                            <h4 class="pt-1">
                                                <a href="{{ $pro->slug }}.html">{{ $pro->name }}</a>
                                            </h4>
                                            <div class="info-product-price my-2">
                                                @if($pro->promotional>0)
                                                    <span class="item_price">
                                                        {{ number_format($pro->promotional) }} VNĐ
                                                    </span>
                                                    <del>{{ number_format($pro->price) }} VNĐ</del>
                                                @else
                                                    <span class="item_price">
                                                        {{ number_format($pro->price) }} VNĐ
                                                    </span>
                                                @endif
                                            </div>
                                            <div class="snipcart-details top_brand_home_details item_add single-item hvr-outline-out">
                                                <a href="{{ route('addCart', ['id' => $pro->id]) }}"> Thêm vào giỏ hàng</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach

                            <div class="btn-read-more">
                                <a href="{{ route('product.get-by-category', [
                                    'slug' => $proUsed[0]->categories->slug,
                                    'id' => $proUsed[0]->categories->id
                                ]) }}"> Xem tất cả</a>
                            </div>
                        @endif
                    </div>
                </div>
                <!-- //second section -->
                <!-- third section -->
                <div class="1">
                    <div class="row">
                        <div class="col-md-8 bg-right-nut">
                            <img src="assets/client/images/a11.jpg" style="height: 380px; width: 825px;" alt="">
                        </div>
                    </div>
                </div>
                <!-- //third section -->
                <!-- fourth section -->
                <div class="product-sec1 px-sm-4 px-3 mb-4 pt-3">
                    <h3 class="heading-tittle text-center font-italic">{{ $proItem[0]->categories->name ?? '' }}</h3>
                    <div class="row">
                        @if(isset($proItem))
                            @foreach($proItem as $pro)
                                <div class="col-md-4 product-men mt-5">
                                    <div class="men-pro-item simpleCart_shelfItem">
                                        <div class="men-thumb-item text-center">
                                            <img src="img/upload/product/{{ $pro->image }}" class="img-fluid" alt=" {{ $pro->name }}">
                                            <div class="men-cart-pro">
                                                <div class="inner-men-cart-pro">
                                                    <a href="{{ $pro->slug }}.html" class="link-product-add-cart">Chi tiết</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="item-info-product text-center border-top mt-4">
                                            <h4 class="pt-1">
                                                <a href="{{ $pro->slug }}.html">{{ $pro->name }}</a>
                                            </h4>
                                            <div class="info-product-price my-2">
                                                @if($pro->promotional > 0)
                                                    <span class="item_price">
                                                        {{ number_format($pro->promotional) }} VNĐ
                                                    </span>
                                                    <del>{{ number_format($pro->price) }} VNĐ</del>
                                                @else
                                                    <span class="item_price">
                                                        {{ number_format($pro->price) }} VNĐ
                                                    </span>
                                                @endif
                                            </div>
                                            <div class="snipcart-details top_brand_home_details item_add single-item hvr-outline-out">
                                                <a href="{{ route('addCart', ['id' => $pro->id]) }}"> Thêm vào giỏ hàng</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach

                            <div class="btn-read-more">
                                <a href="{{ route('product.get-by-category', [
                                    'slug' => $proItem[0]->categories->slug,
                                    'id' => $proItem[0]->categories->id
                                ]) }}"> Xem tất cả</a>
                            </div>
                        @endif
                    </div>
                </div>
                <!-- //fourth section -->
            </div>
        </div>
        <!-- //product left -->
        <!-- product right -->
        @include('client.layouts.sidebar')
    </div>
@endsection
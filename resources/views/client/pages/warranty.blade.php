@extends('client.layouts.master')

@section('title')
    Phiếu yêu cầu bảo hành
@endsection

@section('content')
<h2>Phiếu yêu cầu bảo hành</h2> 
    <form method="post" action="{{ route('warranty.send') }}" enctype="multipart/form-data">
        @csrf
        <input type="hidden" name="idUser" value="{{ $customer[0]->id }}" id="idUser"/>
        <input type="hidden" name="idOrder" value='{{ $idOrder }}' id="idOrder"/>
        <div class="form-group">
            <label for="name">Họ tên:</label>
            <input type="name" class="form-control" value='{{ Auth::user()->name }}' id="name" readonly>
        </div>
        <div class="form-group">
            <label for="email">Địa chỉ email:</label>
            <input type="email" class="form-control" value='{{ Auth::user()->email }}' id="email" readonly>
        </div>
        <div class="form-group">
            <label for="add">Địa chỉ:</label>
            <input type="text" class="form-control" value="{{ $customer[0]->address }}" id="add" readonly>
        </div>
        <div class="form-group">
            <label for="phone">Số điện thoại:</label>
            <input type="text" class="form-control" value='{{ $customer[0]->phone }}' id="phone" readonly>
        </div>
        <div class="form-group">
            <label for="product">Chọn sản phẩm muốn bảo hành:</label>
            <select class="form-control select-product-warranty" data-order-id="{{ $idOrder }}" name="product" id="product" required>
                @foreach ($product as $item)
                    <option value='{{ $item->idProduct }}'>{{ $item->name }}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="phone">Thời gian bảo hành còn lại:</label>
            <input type="text" class="form-control day-product-warranty" value='{{ $daysOfWarranty }} ngày' readonly>
        </div>
        <div class="form-group">
            <label for="reason">Lý do bảo hành:</label>
            <textarea class="form-control" rows="5" name="reason" id="reason" required></textarea>
          </div>
        <button type="submit" class="btn btn-success">Gửi yêu cầu</button>
    </form>   
@endsection
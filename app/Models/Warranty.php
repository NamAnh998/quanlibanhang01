<?php

namespace  App\Models;

use Illuminate\Database\Eloquent\Model;

class Warranty extends Model
{
    protected $table = 'warranties';
    protected $fillable = [
        'idOrder', 'idUser', 'idProduct', 'reason', 'status', 'warranty_period'
    ];
}

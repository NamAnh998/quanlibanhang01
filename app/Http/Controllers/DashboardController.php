<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Product;
use App\Models\Order;
use App\Models\OrderDetail;
use Carbon\Carbon;

class DashboardController extends Controller
{
    private $product;

    private $order;

    private $order_detail;

    const ITEM_LIMITED = 10;

    public function __construct(
        Product $product, 
        Order $order,
        OrderDetail $order_detail
    ) {
        $this->product = $product;
        $this->order = $order;
        $this->order_detail = $order_detail;
    }

    public function index(Request $request)
    {
        $data = $request->all();
        if (isset($data['date']) && $data['date']) {
            $date = explode('-', $data['date']);
            $startDate = $date['0'];
            $startDateConvert = Carbon::parse($date['0'])->format('Y-m-d');
            $endDate = $date['1'];
            $endDateConvert = Carbon::parse($date['1'])->format('Y-m-d');
        } else {
            $startDateConvert = Carbon::now()->format('Y-m-d');
            $endDateConvert = Carbon::now()->format('Y-m-d');
        }

        // Sản phẩm tồn kho
        $productStocks = $this->product
            ->select('id', 'name', 'slug', 'quantity', 'price', 'image')
            // ->whereDate('updated_at', '>=', $startDateConvert)
            // ->whereDate('updated_at', '<=', $endDateConvert)
            ->orderBy('quantity', 'desc')
            ->get();

        $productSellings = [];
        $orderDetails = $this->order_detail
            ->join('order', 'order.id', 'orderdetail.idOrder')
            ->whereDate('orderdetail.created_at', '>=', $startDateConvert)
            ->whereDate('orderdetail.created_at', '<=', $endDateConvert)
            ->where('order.status', '>=', 2)
            ->orderBy('orderdetail.idProduct', 'desc')
            ->get();
        
        // Sản phẩm bán chạy
        $tmpProductSellings = [];
        foreach ($orderDetails as $order) {
            $tmpProductSellings[$order->idProduct] = isset($tmpProductSellings[$order->idProduct]) ? $tmpProductSellings[$order->idProduct] + $order->quantity : $order->quantity;
        }
        arsort($tmpProductSellings);
        foreach ($tmpProductSellings as $key => $value) {
            $product = $this->product->find($key);
            if ($product) {
                $productSellings[] = [
                    'id' => $product->id,
                    'name' => $product->name,
                    'slug' => $product->slug,
                    'quantity' => $value,
                    'price' => $product->price,
                    'total' => $product->price * $value
                ];
            }
        }

        // Thống kê doanh thu theo ngày/khoảng thời gian
        $orders = $this->order
            ->whereDate('created_at', '>=', $startDateConvert)
            ->whereDate('created_at', '<=', $endDateConvert)
            ->where('status', 3)
            ->get();
        
        $revenues = [];
        $totalRevenues = 0;
        foreach ($orders as $item) {
            $key = Carbon::parse($item->created_at)->format('d-m-Y');
            $revenues[$key] = isset($revenues[$key]) ? $revenues[$key] + $item->monney : $item->monney;
            $totalRevenues += $item->monney;
        }

        // Danh sách đơn hàng
        $orderList = $this->order
            ->whereDate('created_at', '>=', $startDateConvert)
            ->whereDate('created_at', '<=', $endDateConvert)
            ->where('status', 3)
            ->with('orderDetails')
            ->orderBy('updated_at', 'desc')
            ->limit(self::ITEM_LIMITED)
            ->get();

        $order = DB::select("select SUM(monney) total_price,DATE_FORMAT(created_at,'%d/%m/%Y') date from `order` where status = 3 group by  DATE_FORMAT(created_at,'%d/%m/%Y')");
        $count = DB::select("select COUNT(*) count_order,DATE_FORMAT(created_at,'%d/%m/%Y') date from `order` where status = 3 group by  DATE_FORMAT(created_at,'%d/%m/%Y')");
        $product = Product::all();
        return view('admin.pages.dashboard',[
            'order' => $order, 
            'count' => $count,
            'product' => $product,
            'statistical' => [
                'productStocks' => $productStocks ?? [],
                'productSellings' => $productSellings ?? [],
                'revenues' => $revenues ?? [],
                'totalRevenues' => $totalRevenues,
                'totalOrders' => $orderList->count(),
                'orderList' => $orderList ?? [],
            ],
            'start_date' => $startDate ?? '',
            'end_date' => $endDate ?? '',
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

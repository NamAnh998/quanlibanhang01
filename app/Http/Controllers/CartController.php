<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Categories;
use App\Models\Product;
use App\Models\ProductTypes;
use Cart;
use Auth;
use App\Models\Order;
use App\Models\OrderDetail;
use Mail;
use App\Mail\ShoppingMail;
use DB;
use Carbon\Carbon;

class CartController extends Controller
{
    public function __construct(){
        $category = Categories::where('status',1)->get();
        $producttype = ProductTypes::where('status',1)->get();
        view()->share(['category' => $category,'producttype' => $producttype]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cart = Cart::content();
        $totalCart = 0;
        foreach ($cart as $item) {
            $totalCart += $item->qty * $item->price;
        }
        $vatCart = $totalCart * 0.1;
        $sumCart = $totalCart + $vatCart;
        return view('client.pages.cart',compact('cart', 'vatCart', 'sumCart'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $total = 0;
        $data = $request->all();
        foreach (Cart::content() as $cart){
            $total += $cart->qty * $cart->price;
        }
        $total += ($total * 10 /100);
        $data['monney'] = $total;
        $data['idUser'] = Auth::user()->id;
        $data['code_order'] = 'order'.rand();
        $data['status'] = 0;
        $dateWarranties = [
            '0' => Carbon::now()->addMonths(3)->format('Y-m-d h:i:s'), // 3 tháng
            '1' => Carbon::now()->addMonths(6)->format('Y-m-d h:i:s'), // 6 tháng
            '2' => Carbon::now()->addMonths(12)->format('Y-m-d h:i:s'), // 1 năm
            '3' => Carbon::now()->addMonths(24)->format('Y-m-d h:i:s'), // 2 năm
            '4' => Carbon::now()->addMonths(36)->format('Y-m-d h:i:s'), // 3 năm
        ];
        DB::beginTransaction();
        try {
            $order = Order::create($data);
            $idOder = $order->id;
            $orderdetail = [];
            foreach (Cart::content() as $cart){
                $now = Carbon::now();
                $warrantyPeriod = '';
                $findWarrantyProduct = Product::find($cart->id)->warranty;
                if ($findWarrantyProduct != 'null' && isset($dateWarranties[$findWarrantyProduct])) {
                    $warrantyPeriod = $dateWarranties[$findWarrantyProduct];
                }
                $orderdetail[] = [
                    'idOrder' => $idOder,
                    'idProduct' => $cart->id,
                    'name' => $cart->name,
                    'quantity' => $cart->qty,
                    'price' => $cart->price,
                    'created_at' => $now,
                    'updated_at' => $now,
                    'warranty_period' => $warrantyPeriod
                ];
                $product = Product::find($cart->id);
                Product::where('id',$cart->id)->update(['quantity' => $product['quantity'] - $cart->qty,'qty_buy' => $product['qty_buy'] + $cart->qty]);
            }
            OrderDetail::insert($orderdetail);
            Mail::to($order->email)->send(new ShoppingMail($order,$orderdetail));
            Cart::destroy();
            DB::commit();
            return response()->json(' Đã mua hàng thành công', 200);
        } catch (Exception $e) {
            DB::rollback();
            return response()->json($e->getMessage(), 200);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Cart $cart)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
           if($request->ajax()) {
               if ($request->qty == 0) {
                   return response()->json(['error'=> 'Số lượng tối thiểu là 1 sản phẩm'],200);
               } else {
                   Cart::update($id, $request->qty);
                   return response()->json(['result' => 'Đã cập nhật số lượng sản phẩm thành công']);
               }
           }
           dd(ajax);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Cart::remove($id);
        return response()->json(['result' => 'Đã xóa thành công sản phẩm']);
    }

    public function addCart($id,Request $request){
        if(Auth::check()){
            $product = Product::find($id);
            if($request->qty) {
                $qty = $request->qty;
            }else{
                $qty = 1;
            }
            if($product->promotional>0){
                $price = $product->promotional;
            }else {
                $price = $product->price;
            }
            $cart = [
                'id' => $id, 
                'name'=> $product->name, 
                'qty' => $qty, 
                'price' => $price, 
                'options' => [
                    'img' => $product->image
                ]
            ];
            Cart::add($cart);
            return back()->with('thongbao', 'Đã thêm thành công' .$product->name.'vào giỏ hàng');
        }else{
            return back()->with('error', 'Xin vui lòng đăng nhập trước khi thêm giỏ hàng');
        }
    }
    public function checkout(){
        $user = Auth::user();
        $price = str_replace(',','',Cart::total());
        $cart = Cart::content();
        $totalCart = 0;
        foreach ($cart as $item) {
            $totalCart += $item->qty * $item->price;
        }
        $vatCart = $totalCart * 0.1;
        $sumCart = $totalCart + $vatCart;
        return view('client.pages.checkout',compact('user', 'sumCart', 'price'));
    }
}

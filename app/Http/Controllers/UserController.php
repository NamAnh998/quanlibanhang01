<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Hash;
use Auth;
use App\Models\Categories;
use App\Models\ProductTypes;

class UserController extends Controller
{

    public function __construct(){
        $category = Categories::where('status',1)->get();
        $producttype = ProductTypes::where('status',1)->get();
        view()->share(['category' => $category,'producttype' => $producttype]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $order = DB::select('select * from `order` where idUser = ?', [Auth::user()->id]);
        return view('client.pages.myorder',['orders' => $order]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.user.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Form validation
        $this->validate($request, [
            'name'  => 'required',
            'email' => 'required',
            'pass'  => 'required',
            'role'  => 'required'
        ]);
        //  Store data in database
        $user = new User([
            'name'  => $request->input('name'),
            'email' => $request->input('email'),
            'password' => bcrypt($request->input('pass')),
            'role' => $request->input('role')
        ]);
        $user->save();
        return redirect()->route('user.getList')->with("thongbao","Thêm thành công");
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order = DB::select('select o.*, d.name p_name, d.quantity qty, d.price price from `order` o, `orderdetail` d where o.id = d.idOrder and d.idOrder = ?', [$id]);
        return view('client.pages.myorderdetail',['orders' => $order]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        return view('admin.pages.user.add',['user' => $user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $user = User::find($request->input('id'));
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->password = bcrypt($request->input('pass'));
        $user->role = $request->input('role');
        $user->save();
        return redirect()->route('user.getList')->with("thongbao","Cập nhật thành công");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();
        return back()->with('thongbao', 'Xóa tài khoản thành công');
    }

    public function registerClient(Request $request)
    {
        $this->validate($request,
            [
                'name' => 'required|min:2|max:255',
                'email' => 'required|email|unique:users,email',
                'password' => 'required|min:6|max:255',
                're_password' => 'required|same:password',
            ],
            [
                'name.required' => 'Họ và tên không được bỏ trống',
                'name.min' => 'Họ và tên phải có tối thiểu 6 ký tự',
                'name.max' => 'Họ và tên tối đa có 255 ký tự',
                'email.required' => 'Email không được bỏ trống',
                'email,email' => 'Email nhập không đúng định dạng',
                'email.unique' => 'Email đã tồn tại trong hệ thống',
                'password.required' => 'Mật khẩu không được bỏ trống',
                'password.min' => 'Mật khẩu phải có tối thiểu 6 ký tự',
                'password.max' => 'Mật khẩu tối đa có 255 ký tự',
                're_password.required' => 'Không được bỏ trống',
                're_password.same' => 'Nhập lại mật khẩu không đúng',
            ]
        );
        $data = $request->all();
        $data = array_merge($data, [
            'role' => 0 // client
        ]);
        $data['password'] = Hash::make($request->password);
        $user = User::create($data);
        Auth::login($user);
        return back()->with('thongbao', 'Đăng ký tài khoản thành công');
    }

    public function loginClient(Request $request)
    {
        if (Auth::attempt(['email' => $request->input('name'), 'password' => $request->input('Password')])) {
            return back()->with('thongbao', 'Đăng nhập thành công');
        } else {
            return back()->with('thongbao', 'Đăng nhập không thành công, xin vui lòng kiểm tra lại tài khoản hoặc mật khẩu!');
        }
    }
    public function logout(){
        if (Auth::check()){
            Auth::logout();
            return redirect('/');
        }
    }
    public function loginAdmin(Request $request)
    {
        $data = $request->only('email', 'password');
        if (Auth::attempt($data, $request->has('remember'))) {
            if (Auth::user()->role == 1)
                return redirect()->route('dashboard.index')->with('thongbao', 'Đăng nhập thành công');
            else if (Auth::user()->role == 2)
                return redirect()->route('category.index');
            else if (Auth::user()->role == 3)
                return redirect()->route('warranty.index');
            else
                return back()->with('error', 'Bạn không có quyền truy cập vào hệ thống của chúng tôi');
        } else {
            return back()->with('error', 'Đăng nhập không thành công, xin vui lòng kiểm tra lại tài khoản hoặc mật khẩu!');
        }
    }

    public function logoutAdmin(){
        Auth::logout();
        return redirect('admin/login')->with('thongbao', 'Đăng xuất thành công');
    }

    public function getList()
    {
        $user = User::paginate(10);
        return view('admin.pages.user.list',['user' => $user]);
    }
}

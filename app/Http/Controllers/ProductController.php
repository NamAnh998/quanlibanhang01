<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Categories;
use App\Models\ProductTypes;
use App\Http\Requests\StoreProductRequest;
use File;
use Validator;
use App\Services\ImageService;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    protected $image_service;

    public function __construct(ImageService $imageService)
    {
        $this->image_service = $imageService;
        $category = Categories::where('status',1)->get();
        $producttype = ProductTypes::where('status',1)->get();
        view()->share(['category' => $category,'producttype' => $producttype]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $product = Product::paginate(10);
        return view('admin.pages.product.list', compact('product'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = Categories::where('status', 1)->get();
        $producttype = ProductTypes::where('status', 1)->get();
        return view('admin.pages.product.add', compact('category', 'producttype'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreProductRequest;  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreProductRequest $request)
    {
        $data = $request->all();

        if ($request->hasFile('image')) {
            $image = $data['image'];
            $file_type = $image->getMimeType();
            $file_size = $image->getSize();

            if ($file_type == 'image/png' || $file_type == 'image/jpg' || $file_type == 'image/jpeg' || $file_type == 'image/gif') {
                if ($file_size <= 1048576) {
                    $image = $data['image'];
                    $folderName = 'img/upload/product/';
                    $fileName = time().'.'.$image->getClientOriginalExtension();
                    if (!File::exists(public_path($folderName))) {
                        File::makeDirectory(public_path($folderName), 0777, true);
                    }
                    $image->move(public_path($folderName), $fileName);
                    $data['image'] = $fileName;

                    $data['slug'] = utf8tourl($request->name);
                    Product::create($data);
                    return redirect()->route('product.index')->with('thongbao', 'Đã thêm thành công sản phẩm mới');
                } else {
                    return back()->with('error', 'Bạn không thể upload ảnh quá 1mb');
                }
            } else {
                return back()->with('error', 'File bạn chọn không là hình ảnh');
            }
        } else {
            return back()->with('error', 'Bạn chưa thêm ảnh minh họa cho sản phẩm');
        }
    }


    /**
     * Display the specified resource.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Categories::where('status',1)->get();
        $producttype = ProductTypes::where('status',1)->get();
        $product = Product::find($id);
        return response()->json([
            'category' => $category, 
            'producttype' => $producttype, 
            'product' => $product
        ],200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(),
            [
                'name' => 'required|min:2|max:255',
                'description' => 'required|min:2',
                'quantity' => 'required|numeric',
                'price' => 'required|numeric',
                'promotional' => 'numeric',
                'image' => 'file',
            ],
            [
                'required' => ':attribute không được bỏ trống',
                'min' => ':attribute tối thiểu có 2 kí tự',
                'max' => ':attribute tối đa có 255 kí tự',
                'numeric' => ':attribute phải là một số',
                'file' => ':attribute không phải là hình ảnh',
            ],
            [
                'name' => 'Tên sản phẩm',
                'description' => 'Mô tả sản phẩm',
                'quantity' => 'Số lượng sản phẩm',
                'price' => 'Đơn giá sản phẩm',
                'promotional' => 'Giá khuyến mại',
                'file' => 'Ảnh minh họa',
            ]
        );
        if($validator->fails()) {
            return response()->json(['error' => 'true','message' => $validator->errors()], 200);
        }
        $product = Product::find($id);
        $data = $request->all();
        $data['slug'] = utf8tourl($request->name);

        if (isset($data['image'])) {
            $image = $data['image'];
            $folderName = 'img/upload/product/';
            $fileName = time().'.'.$image->getClientOriginalExtension();
            if (!File::exists(public_path($folderName))) {
                File::makeDirectory(public_path($folderName), 0777, true);
            }
            $image->move(public_path($folderName), $fileName);
            $data['image'] = $fileName;
        }
        $product->update($data);
        return response()->json(['result' => 'Đã sửa thành công sản phẩm có id là ' . $id], 200);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::find($id);
        $this->image_service->deleteFile($product->image, 'img/upload/product');
        $product->delete();
        return response()->json(['result' => 'Đã xóa thành công sản phẩm có id là '. $id], 200);
    }

    public function searchName(Request $request)
    {
        $q = $request->input('q');
        $product = DB::table('product')->where('name','like','%'.$q.'%')->get();
        return view('client.pages.product-name',['product' => $product, 'q' => $q]);
    }

    public function searchType(Request $request)
    {
        $q = $request->input('q');
        $product = DB::table('producttype')->join('product','producttype.id','=','product.idProductType')->where('producttype.name','like','%'.$q.'%')->select('product.*')->get();
        return view('client.pages.product-type',['product' => $product, 'q' => $q]);
    }
}

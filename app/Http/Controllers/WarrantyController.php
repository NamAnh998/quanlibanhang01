<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Categories;
use App\Models\ProductTypes;
use App\Models\Customer;
use App\Models\Warranty;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use DateTime;
use App\Models\OrderDetail;

class WarrantyController extends Controller
{
    public function __construct(){
        $category = Categories::where('status',1)->get();
        $producttype = ProductTypes::where('status',1)->get();
        view()->share(['category' => $category,'producttype' => $producttype]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $warranty = DB::table('warranties')
        ->join('product', 'warranties.idProduct', '=', 'product.id')
        ->join('customer', 'warranties.idUser', '=', 'customer.idUser')
        ->join('order', 'warranties.idOrder', '=', 'order.id')
        ->join('users','users.id','=','customer.idUser')
        ->select('warranties.id', 'warranties.reason', 'warranties.status', 'warranties.warranty_period', 'product.name', 'customer.email', 'customer.phone', 'users.name AS u_name', 'order.code_order')
        ->paginate(10);
        return view('admin.pages.warranty.list',['warranty' => $warranty]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $customer =  DB::select('select u.id,u.name,c.address,c.phone from users u, customer c where u.id = c.idUser and u.id = ?',[Auth::user()->id]);
        $product = DB::table('orderdetail')->where('idOrder','=',$id)->get();

        $today = new DateTime(Carbon::now());
        $dateWarranty = DB::table('orderdetail')->where('idOrder', $id)->first()->warranty_period;
        $daysOfWarranty = $today->diff(new DateTime($dateWarranty))->days;
        return view('client.pages.warranty',[
            'customer' => $customer, 
            'product' => $product, 
            'idOrder' => $id,
            'daysOfWarranty' => $daysOfWarranty,
        ]);
    }

    public function getPeriod($order_id, $product_id) 
    {
        $today = new DateTime(Carbon::now());
        $dateWarranty = DB::table('orderdetail')
            ->where([
                'idOrder' => $order_id,
                'idProduct' => $product_id
            ])
            ->first()
            ->warranty_period;
        $daysOfWarranty = $today->diff(new DateTime($dateWarranty))->days;

        return response()->json([
            'code' => 200,
            'day' => $daysOfWarranty
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $dateWarranty = OrderDetail::where('idOrder', $request->input('idOrder'))->first()->warranty_period;
        //  Store data in database
        $warranty = new Warranty([
            'idOrder' => $request->input('idOrder'),
            'idUser'  => $request->input('idUser'),
            'idProduct' => $request->input('product'),
            'reason'    => $request->input('reason'),
            'warranty_period' => $dateWarranty
        ]);
        $warranty->save();
        return redirect('/')->with('thongbao','Gửi yêu cầu thành công, chúng tôi sẽ liên lạc với bạn sau');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Warranty  $warranty
     * @return \Illuminate\Http\Response
     */
    public function show(Warranty $warranty)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Warranty  $warranty
     * @return \Illuminate\Http\Response
     */
    public function edit(Warranty $warranty)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $warranty = Warranty::find($id);
        $warranty->status = 1;
        $warranty->save();
        return redirect()->back()->with('thongbao', 'Xác nhận đơn bảo hành thành công');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Warranty  $warranty
     * @return \Illuminate\Http\Response
     */
    public function destroy(Warranty $warranty)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function complete($id)
    {
        $warranty = Warranty::find($id);
        $warranty->status = 2;
        $warranty->save();
        return redirect()->back()->with('thongbao', 'Đơn bảo hành đã được hoàn thành, vui lòng kiểm tra');
    }

    public function cancel(Request $request, $id)
    {
        $warranty = Warranty::find($id);
        $warranty->status = 3;
        $warranty->save();
        return redirect()->back()->with('thongbao', 'Đơn bảo hành đã được hủy, vui lòng kiểm tra');
    }
}

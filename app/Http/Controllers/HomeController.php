<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Categories;
use App\Models\Product;
use App\Models\ProductTypes;
use Cart;
use Auth;

class HomeController extends Controller
{
    const PAGE_LIMITED = 9;

    public function __construct(){
        $category = Categories::where('status',1)->get();
        $producttype = ProductTypes::where('status',1)->get();
        view()->share(['category' => $category,'producttype' => $producttype]);
    }

    public function index(){
        $product1 = Product::where('status',1)->where('idCategory',1)->paginate(9);
        $product2 = Product::where('status',1)->where('idCategory',2)->paginate(9);
        $product3 = Product::where('status',1)->where('idCategory',4)->paginate(9);

        return view('client.pages.index',['proNew' => $product1, 'proUsed' => $product2, 'proItem' => $product3]);
    }

   public function getDetail($slug) {
       $productDetail = Product::where('slug',$slug)->first();
       $idProType = ProductTypes::where('slug',$slug)->first();
       if(!is_null($productDetail)){
            return view('client.pages.detail', ['product' => $productDetail]);
       }else{
            $productByProdType = Product::where('idProductType',$idProType->id)->get();
            return view('client.pages.detail_protype',['product' => $productByProdType,'producttype' => $idProType]);
       }
   }

   public function getProductByCategory($slug, $id) {
       $products = Product::where('idCategory', $id)->paginate(self::PAGE_LIMITED);
       $categoryName = Categories::find($id)->name;
       
       $data = [
        'products' => $products,
        'categoryName' => $categoryName,
       ];
       return view('client.pages.product.index', $data);
   }
    public function getProductByProductTypes($slug, $id) {
        $products = Product::where('idProductType', $id)->paginate(self::PAGE_LIMITED);
        $categoryName = ProductTypes::find($id)->name;

        $data = [
            'products' => $products,
            'categoryName' => $categoryName,
        ];
        return view('client.pages.product.index', $data);
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\OrderDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = Order::paginate(10);
        return view('admin.pages.order.list',['orders' => $orders]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order = DB::select('select o.*, d.name p_name, d.quantity qty, d.price price from `order` o, `orderdetail` d where o.id = d.idOrder and d.idOrder = ?', [$id]);
        $dateWarranty = OrderDetail::where('idOrder', $id)->first()->warranty_period;
        $totalOrder = 0;
        foreach ($order as $item) {
            $totalOrder += $item->price * $item->qty;
        }
        $vat = ($totalOrder * 10) / 100;
        $totalOrder = $totalOrder + $vat;

        // $order = Order::find($id);
        // $orderdetail = OrderDetail::where('idOrder', $id)->get()->toArray();
        // return view('mail.shopping', [
        //     'orderdetail' => $orderdetail,
        //     'order' => $order,
        // ]);

        return view('admin.pages.order.detail',[
            'order' => $order,
            'dateWarranty' => $dateWarranty,
            'vat' => $vat,
            'totalOrder' => $totalOrder
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function cancle(Request $request, $id)
    {
        $order = Order::find($id);
        $order->status = 2;
        $order->save();
        return redirect()->back()->with('thongbao', 'Hủy đơn hàng thành công');
    }

     /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function check(Request $request, $id)
    {
        $order = Order::find($id);
        $order->status = 1;
        $order->save();
        return redirect()->back()->with('thongbao', 'Xác nhận đơn hàng thành công');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function complete(Request $request, $id)
    {
        $order = Order::find($id);
        $order->status = 3;
        $order->save();
        return redirect()->back()->with('thongbao', 'Đơn hàng đã hoàn tất, vui lòng kiểm tra trạng thái');
    }
}

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
function printDashboard () {
    printJS({ 
        printable: 'dashboard-statistical', 
        type: 'html', 
        targetStyles: ['*'],
        header: null,
        documentTitle: "",
        footer: null,
        onLoadingStart: function () {
            $("#dashboard-statistical").css("display", "block");
        },
        onLoadingEnd: function () {
            $("#dashboard-statistical").css("display", "none");
        },
        css: "https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css",
        style: '@page { size: Letter landscape; } @media print { .bulding-print-item { page-break-after: always; } }'
    })
}
function printRevenues () {
    printJS({ 
        printable: 'printRevenues', 
        type: 'html', 
        targetStyles: ['*'],
        header: null,
        documentTitle: "",
        footer: null,
        onLoadingStart: function () {
            $("#printRevenues").css("display", "block");
        },
        onLoadingEnd: function () {
            $("#printRevenues").css("display", "none");
        },
        css: "https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css",
        style: '@page { size: Letter landscape; } @media print { .bulding-print-item { page-break-after: always; } }'
    })
}
function printProductSellings () {
    printJS({ 
        printable: 'printProductSellings', 
        type: 'html', 
        targetStyles: ['*'],
        header: null,
        documentTitle: "",
        footer: null,
        onLoadingStart: function () {
            $("#printProductSellings").css("display", "block");
        },
        onLoadingEnd: function () {
            $("#printProductSellings").css("display", "none");
        },
        css: "https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css",
        style: '@page { size: Letter landscape; } @media print { .bulding-print-item { page-break-after: always; } }'
    })
}
function printProductStocks () {
    printJS({ 
        printable: 'printProductStocks', 
        type: 'html', 
        targetStyles: ['*'],
        header: null,
        documentTitle: "",
        footer: null,
        onLoadingStart: function () {
            $("#printProductStocks").css("display", "block");
        },
        onLoadingEnd: function () {
            $("#printProductStocks").css("display", "none");
        },
        css: "https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css",
        style: '@page { size: Letter landscape; } @media print { .bulding-print-item { page-break-after: always; } }'
    })
}
function printOrderList () {
    printJS({ 
        printable: 'printOrderList', 
        type: 'html', 
        targetStyles: ['*'],
        header: null,
        documentTitle: "",
        footer: null,
        onLoadingStart: function () {
            $("#printOrderList").css("display", "block");
        },
        onLoadingEnd: function () {
            $("#printOrderList").css("display", "none");
        },
        css: "https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css",
        style: '@page { size: Letter landscape; } @media print { .bulding-print-item { page-break-after: always; } }'
    })
}
function printOrder () {
    printJS({ 
        printable: 'print-order', 
        type: 'html', 
        targetStyles: ['*'],
        header: null,
        documentTitle: "",
        footer: null,
        onLoadingStart: function () {
            $("#print-order").css("display", "block");
        },
        onLoadingEnd: function () {
            $("#print-order").css("display", "none");
        },
        css: "https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css",
        style: '@page { size: Letter landscape; } @media print { .bulding-print-item { page-break-after: always; } }'
    })
}
$(document).ready(function () {
    $('.edit').click(function () {
        $('.erorr').hide();
        let id = $(this).data('id');
        //Edit
        $.ajax ({
            url : 'admin/category/'+id+'/edit',
            dataType : 'json',
            type : 'get',
            success :function ($result) {
                $('.name').val($result.name);
                $('.title').text($result.name);
                if($result.status == 1){
                    $('.ht').attr('selected','selected');
                }
                else{
                    $('.kht').attr('selected','selected');
                    }
            }
        });
        $('.update').click(function () {
            let ten = $('.name').val();
            let status = $('.status').val();
            $.ajax({
                url : 'admin/category/'+id,
                data : {
                    name : ten,
                    status : status,
                    id : id
                },
                type : 'put',
                dataType: 'json',
                success : function ($result) {
                        toastr.success($result.success, 'Thông báo', {timeOut: 8000});
                        $('#edit').modal('hide');
                        location.reload();
                },
                error : function (error) {
                    var errors = JSON.parse(error.responseText);
                    $('.error').show();
                    $('.error').text(errors.errors.name);
                }
            });
        });
    });
    //Delete Category
    $('.delete').click(function () {
        let id = $(this).data('id');
        $('.del').click(function () {
            $.ajax({
                url : 'admin/category/'+id,
                dataType : 'json',
                type : 'delete',
                success : function ($result) {
                    toastr.success($result.success, 'Thông báo', {timeOut: 8000});
                    $('#edit').modal('hide');
                    location.reload();
                }
            });
        });
    });
    // Edit ProductType
    $('.editProducttype').click(function(){
        $('.error').hide();
        let id = $(this).data('id');
        $.ajax({
            url : 'admin/producttype/'+id+'/edit',
            dataType : 'json',
            type : 'get',
            success : function($result){
                $('.name').val($result.producttype.name);
                var html = '';
                $.each($result.category,function($key,$value){
                    if($value['id'] == $result.producttype.idCategory){
                        html += '<option value='+$value['id']+' selected>';
                        html += $value['name'];
                        html += '</option>';
                    }else{
                        html += '<option value='+$value['id']+'>';
                        html += $value['name'];
                        html += '</option>';
                    }
                });
                $('.idCategory').html(html);
                if($result.producttype.status == 1){
                    $('.ht').attr('selected','selected');
                }else{
                    $('.kht').attr('selected','selected');
                }
            }
        });
        $('.updateProductType').click(function(){
            let idCategory = $('.idCategory').val();
            let name = $('.name').val();
            let status = $('status').val();
            $.ajax({
                url : 'admin/producttype/'+id,
                dataType : 'json',
                data : {
                    idCategory : idCategory,
                    name : name,
                    status : status,
                },
                type : 'put',
                success : function($data){
                    if($data.error == 'true'){
                        $('.error').show();
                        $('.error').text($data.message.name[0]);
                    }else{
                        toastr.success($data.result, 'Thông báo', {timeOut: 5000});
                        $('#edit').modal('hide');
                        location.reload();
                    }
                }
            })
        });
    });
    //Delete ProductType
    $('.deleteProducttype').click(function(){
        let id = $(this).data('id');
        $('.delProductType').click(function(){
            $.ajax({
                url : 'admin/producttype/'+id,
                dataType : 'json',
                type : 'delete',
                success : function($data){
                    toastr.success($data.result, 'Thông báo', {timeOut: 5000});
                    $('#delete').modal('hide');
                    location.reload();
                }
            });
        });
    });
    $('.cateProduct').change(function(){
        let idCate = $(this).val();
        $.ajax({
            url : 'getproducttype',
            data : {
                idCate : idCate
            },
            type : 'get',
            dataType : 'json',
            success : function(data){
                let html = '';
                $.each(data,function($key,$value){
                    html += '<option value='+$value['id']+'>';
                    html += $value['name'];
                    html += '</option>';
                });
                $('.proTypeProduct').html(html);
            }
        });
    });
    // Edit Product
    $('.editProduct').click(function(){
        $('.errorName').hide();
        $('.errorQuantity').hide();
        $('.errorPrice').hide();
        $('.errorPromotional').hide();
        $('.errorImage').hide();
        $('.errorDescription').hide();
        let id = $(this).data('id');
        $('.idProduct').val(id);
        $.ajax({
            url : 'admin/product/'+id+'/edit',
            dataType : 'json',
            type : 'get',
            success : function(data){
                console.log(data)
                $('.name').val(data.product.name);
                $('.quantity').val(data.product.quantity);
                $('.price').val(data.product.price);
                $('.promotional').val(data.product.promotional);
                $('.imageThum').attr('src','img/upload/product/'+data.product.image);
                $('#warranty').val(data.product.warranty)
                if(data.product.status == 1){
                    $('.ht').attr('selected','selected');
                }else{
                    $('.kht').attr('selected','selected');
                }
                CKEDITOR.instances['demo'].setData(data.product.description);
                let html1 = '';
                $.each(data.category,function(key,value){
                    if(data.product.idCategory == value['id']){
                        html1 += '<option value="'+value['id']+'" selected>';
                        html1 += value['name'];
                        html1 += '</option>';
                    }else{
                        html1 += '<option value="'+value['id']+'">';
                        html1 += value['name'];
                        html1 += '</option>';
                    }
                });
                $('.cateProduct').html(html1);
                let html2 = '';
                $.each(data.producttype,function(key,value){
                    if(data.product.idProductType == value['id']){
                        html2 += '<option value="'+value['id']+'" selected>';
                        html2 += value['name'];
                        html2 += '</option>';
                    }else{
                        html2 += '<option value="'+value['id']+'">';
                        html2 += value['name'];
                        html2 += '</option>';
                    }
                });
                $('.proTypeProduct').html(html2);
            }
        });
        $('#updateProduct').on('submit',function(event){
            //chặn form submit
            event.preventDefault();
            $.ajax({
                url : 'admin/updatePro/'+id,
                data : new FormData(this),
                contentType : false,
                processData : false,
                cache : false,
                type : 'post',
                success : function(data){
                    toastr.success(data.result, 'Thông báo', {timeOut: 5000});
                    $('#edit').modal('hide');
                    location.reload();
                },
                error: function(error) {
                    var errors = JSON.parse(error.responseText);
                    if(errors.errors.name != '') {
                        $('.errorName').show();
                        $('.errorName').text(errors.errors.name);
                    }
                    if(errors.errors.description != '') {
                        $('.errorDescription').show();
                        $('.errorDescription').text(errors.errors.description);
                    }
                    if(errors.errors.quantity != '') {
                        $('.errorQuantity').show();
                        $('.errorQuantity').text(errors.errors.quantity);
                    }
                    if(errors.errors.price != '') {
                        $('.errorPrice').show();
                        $('.errorPrice').text(errors.errors.price);
                    }
                    if(errors.errors.promotional != '') {
                        $('.errorPromotional').show();
                        $('.errorPromotional').text(errors.errors.promotional);
                    }
                    if(errors.errors.image != '') {
                        $('.errorImage').show();
                        $('.errorImage').text(errors.errors.image);
                    }
                }
            });
        });
    });
//    Delete Product
    $('.deleteProduct').click(function () {
        let id = $(this).data('id');
        $('.delProduct').click(function () {
            $.ajax({
                url : 'admin/product/'+id,
                type : 'delete',
                dataType : 'json',
                success : function ($data) {
                    toastr.success($data.result, 'Thông báo', {timeOut: 5000});
                    $('#delete').modal('hide');
                    location.reload();
                }
            });
        });
    });
});
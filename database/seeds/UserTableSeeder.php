<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user')->insert([
            'name' => 'Chu Văn Nam Anh',
            'email' => 'abc@gmail.com',
            'password' => Hash::make('123123'),
            'role' => 1
        ]);
    }
}

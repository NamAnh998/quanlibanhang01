<?php
//use Illuminate\Support\Facades\Route;
//use App\Http\Controllers\HomeController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Admin
Route::post('admin/login','UserController@loginAdmin')->name('admin.login');
Route::view('admin/login','admin.pages.login')->name('login.admin');
Route::get('/logoutAdmin','UserController@logoutAdmin')->name('logoutAdmin');

Route::get('getproducttype','AjaxController@getProductType');
Route::group(['prefix' => 'admin', 'middleware' => 'adminMiddleware'],function(){
    Route::resource('dashboard','DashboardController');
    Route::get('/', 'DashboardController@index')->name('dashboard');
    Route::resource('category','CategoryController');
    Route::resource('producttype','ProductTypeController');
    Route::resource('product','ProductController');
    Route::resource('order','OrderController');
    Route::resource('warranty','WarrantyController');

    Route::get('getlist','UserController@getList')->name('user.getList');

    Route::get('deleteUser/{id}','UserController@destroy')->name('user.delete');

    Route::get('createUser','UserController@create')->name('user.create');

    Route::post('addUser','UserController@store')->name('user.add');

    Route::get('editUser/{id}','UserController@edit')->name('user.edit');

    Route::post('updateUser','UserController@update')->name('user.update');

    Route::get('updateWar/{id}','WarrantyController@update')->name('warranty.update');

    Route::get('completeWar/{id}','WarrantyController@complete')->name('warranty.complete');

    Route::get('cancelWar/{id}','WarrantyController@cancel')->name('warranty.cancel');

    Route::post('updatePro/{id}','ProductController@update');

    Route::get('cancle/{id}','OrderController@cancle')->name('order.cancle');

    Route::get('check/{id}','OrderController@check')->name('order.check');

    Route::get('complete/{id}','OrderController@complete')->name('order.complete');
});

//Client
Route::post('login','UserController@loginClient')->name('login');
Route::get('/logout','UserController@logout')->name('logout');
Route::post('register','UserController@registerClient')->name('register');
Route::get('/myorder','UserController@index')->name('myorder');
Route::get('/myorder/show/{id}','UserController@show')->name('myorder.show');
Route::get('/myorder/warranty/{id}','WarrantyController@create')->name('myorder.warranty');
Route::post('/myorder/warranty/send','WarrantyController@store')->name('warranty.send');

Route::get('/','HomeController@index')->name('home-page');
Route::get('trangchu.html','HomeController@index');
Route::resource('cart','CartController');
Route::get('addcart/{id}','CartController@addCart')->name('addCart');

Route::view('checkout','client.pages.checkout');
Route::get('checkout','CartController@checkout')->name('cart.checkout');
Route::resource('customer','CustomerController');
Route::get('/{slug}.html', 'HomeController@getDetail')->name('product-detail');

Route::post('searchname','ProductController@searchName')->name('search.name');
Route::post('searchtype','ProductController@searchType')->name('search.type');

Route::get('/warranty-period/{order_id}/{product_id}', 'WarrantyController@getPeriod');

Route::get('/danh-muc/{slug}/{id}', 'HomeController@getProductByCategory')->name('product.get-by-category');
Route::get('/danh-muc-loai/{slug}/{id}', 'HomeController@getProductByProductTypes')->name('producttype.get-by-category');
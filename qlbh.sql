-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th7 06, 2021 lúc 03:41 PM
-- Phiên bản máy phục vụ: 10.4.14-MariaDB
-- Phiên bản PHP: 7.4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `qlbh`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `carts`
--

CREATE TABLE `carts` (
  `id` int(10) UNSIGNED NOT NULL,
  `idProduct` int(11) NOT NULL,
  `quantity` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `price` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `total` varchar(45) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `category`
--

CREATE TABLE `category` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `category`
--

INSERT INTO `category` (`id`, `name`, `slug`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Ô tô mới', 'o-to-moi', 1, '2021-04-27 08:46:43', '2021-04-27 08:51:27'),
(2, 'Ô tô đã qua sử dụng', 'o-to-da-qua-su-dung', 1, '2021-04-27 08:49:16', '2021-04-27 08:49:16'),
(4, 'Phụ kiện cho ô tô', 'phu-kien-cho-o-to', 1, '2021-04-27 08:51:49', '2021-04-27 08:51:49');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `customer`
--

CREATE TABLE `customer` (
  `id` int(10) UNSIGNED NOT NULL,
  `idUser` int(11) NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `customer`
--

INSERT INTO `customer` (`id`, `idUser`, `email`, `address`, `phone`, `active`, `created_at`, `updated_at`) VALUES
(1, 1, 'abc@gmail.com', 'Hà Nội', '0908552259', 1, '2021-06-01 05:45:06', '2021-06-01 05:45:06'),
(4, 2, 'b@gmail.com', 'Tôn Đức Thắng - HN', '094855556666', 1, '2021-06-29 04:30:14', '2021-06-29 04:30:14'),
(5, 11, 'anh@gmail.com', 'Đà Nẵng', '023648169', 0, '2021-06-30 09:56:14', '2021-07-06 05:01:47'),
(6, 13, 'hau@gmail.com', 'Lào Cai', '02364894321', 1, '2021-07-01 07:20:47', '2021-07-01 07:20:47'),
(7, 14, 'hoang@gmail.com', 'Yên Bái', '0235694135', 1, '2021-07-01 07:32:33', '2021-07-01 07:32:33'),
(8, 17, 'ng@gmail.com', 'Hội An', '0235478621', 1, '2021-07-02 09:00:07', '2021-07-02 09:00:07'),
(9, 18, 'bui@gmail.com', 'Vĩnh Phúc', '0235694135', 1, '2021-07-02 09:05:48', '2021-07-02 09:05:48'),
(10, 16, 'phanh@gmail.com', 'Quang Binh', '01235479', 1, '2021-07-05 06:46:48', '2021-07-05 06:46:48'),
(11, 19, 'phong@gmail.com', 'Nghệ An', '0123448236', 1, '2021-07-05 20:28:26', '2021-07-05 20:28:26'),
(12, 20, 'hong@gmail.com', 'Nam Từ Liêm', '0136498451', 1, '2021-07-05 21:21:31', '2021-07-05 21:21:31'),
(13, 11, 'anh@gmail.com', 'Quảng Bình', '01235479', 1, '2021-07-06 05:01:47', '2021-07-06 05:01:47');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2021_06_25_071248_add_warranty_column_to_products_table', 1),
(2, '2021_06_25_074502_add_column_to_product_table', 2),
(3, '2021_06_28_133444_add_warranty_period_column_to_order_table', 3),
(4, '2021_06_28_135743_add_warranty_period_to_orderdetail_table', 4),
(5, '2021_06_28_145852_add_warranty_period_column_to_waranties_table', 5);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `order`
--

CREATE TABLE `order` (
  `id` int(10) UNSIGNED NOT NULL,
  `code_order` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `idUser` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `monney` float NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `order`
--

INSERT INTO `order` (`id`, `code_order`, `idUser`, `name`, `address`, `email`, `phone`, `monney`, `message`, `status`, `created_at`, `updated_at`) VALUES
(1, 'order1339492939', 1, 'Chu Văn Nam Anh', 'Hà Nội', 'abc@gmail.com', '0908552259', 656868, 'Thank', 3, '2021-06-01 05:53:20', '2021-06-20 18:55:29'),
(2, 'order1833164746', 1, 'Chu Văn Nam Anh', 'Hà Nội', 'abc@gmail.com', '0908552259', 3321940, NULL, 3, '2021-06-01 05:58:54', '2021-06-20 18:55:19'),
(4, 'order1197043924', 1, 'Chu Văn Nam Anh', 'Hà Nội', 'abc@gmail.com', '0908552259', 55150, NULL, 3, '2021-06-01 18:49:13', '2021-06-20 18:55:16'),
(5, 'order1695959942', 1, 'Chu Văn Nam Anh', 'Hà Nội', 'abc@gmail.com', '0908552259', 829867, NULL, 3, '2021-06-01 19:15:32', '2021-06-20 18:55:07'),
(6, 'order1581899216', 1, 'Chu Văn Nam Anh', 'Hà Nội', 'abc@gmail.com', '0908552259', 10530000, 'Ship tới tận nơi', 3, '2021-06-18 11:34:43', '2021-06-20 00:07:51'),
(7, 'order1639034744', 1, 'Chu Văn Nam Anh', 'Hà Nội', 'abc@gmail.com', '0908552259', 930000, 'Ship tận nơi', 3, '2021-06-18 21:16:12', '2021-06-20 05:03:52'),
(10, 'order1102586617', 1, 'Chu Văn Nam Anh', 'Hà Nội', 'abc@gmail.com', '0908552259', 193300000, NULL, 3, '2021-06-20 00:47:30', '2021-06-20 05:04:19'),
(11, 'order1714581622', 1, 'Chu Văn Nam Anh', 'Hà Nội', 'abc@gmail.com', '0908552259', 190330000, 'Ship tận nhà', 3, '2021-06-20 04:58:53', '2021-06-20 07:37:52'),
(19, 'order2080858652', 1, 'Chu Văn Nam Anh', 'Hà Nội', 'abc@gmail.com', '0908552259', 8830000, 'sfdfdfdfds', 3, '2021-06-23 06:20:15', '2021-06-29 01:00:53'),
(22, 'order1136363121', 1, 'Chu Văn Nam Anh', 'Hà Nội', 'abc@gmail.com', '0908552259', 16530000, 'afdfdfdf', 3, '2021-06-28 06:58:46', '2021-06-29 01:00:36'),
(25, 'order1113733601', 1, 'Chu Văn Nam Anh', 'Hà Nội', 'abc@gmail.com', '0908552259', 135330000, 'FFFF', 3, '2021-06-28 08:06:58', '2021-06-28 08:07:26'),
(26, 'order1463657674', 1, 'Chu Văn Nam Anh', 'Hà Nội', 'abc@gmail.com', '0908552259', 135330000, 'f', 3, '2021-06-28 08:11:44', '2021-06-28 08:12:04'),
(27, 'order666186096', 1, 'Chu Văn Nam Anh', 'Hà Nội', 'abc@gmail.com', '0908552259', 135330000, 'fff', 3, '2021-06-28 08:16:45', '2021-06-28 08:17:08'),
(28, 'order389851320', 2, 'Nguyễn Văn B', 'Tôn Đức Thắng - HN', 'b@gmail.com', '094855556666', 7150030000, 'thank', 3, '2021-06-29 04:30:25', '2021-06-29 04:31:36'),
(30, 'order1313794974', 1, 'Chu Văn Nam Anh', 'Hà Nội', 'abc@gmail.com', '0908552259', 858030000, 'cảm ơn', 3, '2021-06-30 09:28:56', '2021-06-30 09:52:33'),
(31, 'order735939024', 11, 'Trần Quang Anh', 'Đà Nẵng', 'anh@gmail.com', '023648169', 11557200000, 'abc', 3, '2021-06-30 09:56:36', '2021-06-30 10:01:56'),
(32, 'order1101713500', 13, 'Đoàn Văn Hậu', 'Lào Cai', 'hau@gmail.com', '02364894321', 873430000, 'Ship giờ hành chính', 3, '2021-07-01 07:21:20', '2021-07-01 07:22:58'),
(33, 'order1898028129', 14, 'Nguyễn Trọng Hoàng', 'Yên Bái', 'hoang@gmail.com', '0235694135', 292509000, 'ádfhk', 3, '2021-07-01 07:32:57', '2021-07-01 07:33:33'),
(34, 'order291975930', 17, 'Nguyễn Phương Anh', 'Hội An', 'ng@gmail.com', '0235478621', 1723940000, 'ship nhanh nhé', 3, '2021-07-02 09:01:22', '2021-07-02 09:07:19'),
(36, 'order487347277', 18, 'Bùi Bích Phương', 'Vĩnh Phúc', 'bui@gmail.com', '0235694135', 7150030000, 'thank', 3, '2021-07-02 09:06:09', '2021-07-02 09:07:42'),
(37, 'order1079369748', 18, 'Bùi Bích Phương', 'Vĩnh Phúc', 'bui@gmail.com', '0235694135', 1063180000, 'thank', 3, '2021-07-02 09:13:30', '2021-07-02 09:16:39'),
(39, 'order1813237428', 17, 'Nguyễn Phương Anh', 'Hội An', 'ng@gmail.com', '0235478621', 1071210000, 'abc', 3, '2021-07-03 19:04:14', '2021-07-03 19:06:09'),
(40, 'order1082273386', 11, 'Trần Quang Anh', 'Đà Nẵng', 'anh@gmail.com', '023648169', 1579950000, 'ship nhanh nhe', 3, '2021-07-03 19:37:20', '2021-07-03 19:37:57'),
(41, 'order278548355', 16, 'Trần Phương Anh', 'Quang Binh', 'phanh@gmail.com', '01235479', 1786310000, 'good', 3, '2021-07-05 06:47:02', '2021-07-05 06:48:17'),
(42, 'order155508982', 17, 'Nguyễn Phương Anh', 'Hội An', 'ng@gmail.com', '0235478621', 8043940000, 'good', 3, '2021-07-05 06:59:04', '2021-07-05 06:59:39'),
(43, 'order1134775556', 13, 'Đoàn Văn Hậu', 'Lào Cai', 'hau@gmail.com', '02364894321', 1186930000, 'very good', 3, '2021-07-05 07:04:41', '2021-07-05 07:05:17'),
(44, 'order799307250', 18, 'Bùi Bích Phương', 'Vĩnh Phúc', 'bui@gmail.com', '0235694135', 2073980000, 'cảm ơn shop', 3, '2021-07-05 20:19:43', '2021-07-05 20:21:11'),
(45, 'order819044019', 19, 'Dương Gia Phong', 'Nghệ An', 'phong@gmail.com', '0123448236', 1379210000, 'ship sớm', 3, '2021-07-05 20:28:40', '2021-07-05 20:29:35'),
(46, 'order1329392540', 20, 'Nguyễn Phong Hồng', 'Nam Từ Liêm', 'hong@gmail.com', '0136498451', 2408480000, 'ok', 3, '2021-07-05 21:21:43', '2021-07-05 21:34:37'),
(47, 'order802423845', 11, 'Trần Quang Anh', 'Quảng Bình', 'anh@gmail.com', '01235479', 13337800000, 'ok shop', 3, '2021-07-06 05:02:51', '2021-07-06 05:03:44');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `orderdetail`
--

CREATE TABLE `orderdetail` (
  `id` int(10) UNSIGNED NOT NULL,
  `idOrder` int(11) NOT NULL,
  `idProduct` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantity` int(11) NOT NULL,
  `price` double NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `warranty_period` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Ngày bảo hành'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `orderdetail`
--

INSERT INTO `orderdetail` (`id`, `idOrder`, `idProduct`, `name`, `quantity`, `price`, `created_at`, `updated_at`, `warranty_period`) VALUES
(1, 1, 12, 'Seltots', 1, 626868, '2021-06-01 05:53:31', '2021-06-01 05:53:31', '0000-00-00 00:00:00'),
(2, 2, 11, 'G63-AMG', 3, 797979, '2021-06-01 05:58:54', '2021-06-01 05:58:54', '0000-00-00 00:00:00'),
(3, 2, 18, 'CX-5', 1, 898000, '2021-06-01 05:58:54', '2021-06-01 05:58:54', '0000-00-00 00:00:00'),
(4, 4, 16, 'Bọc vô lăng ô tô SPARCO', 1, 150, '2021-06-01 18:49:13', '2021-06-01 18:49:13', '0000-00-00 00:00:00'),
(5, 4, 14, 'Cruze', 1, 25000, '2021-06-01 18:49:13', '2021-06-01 18:49:13', '0000-00-00 00:00:00'),
(6, 5, 10, 'S650 Maybach', 1, 98999, '2021-06-01 19:15:32', '2021-06-01 19:15:32', '0000-00-00 00:00:00'),
(7, 5, 17, 'Màn hình Androi OWIN', 5, 2800, '2021-06-01 19:15:32', '2021-06-01 19:15:32', '0000-00-00 00:00:00'),
(8, 5, 8, 'Lux A 2.0', 1, 686868, '2021-06-01 19:15:32', '2021-06-01 19:15:32', '0000-00-00 00:00:00'),
(9, 6, 12, 'Seltots', 1, 8000000, '2021-06-18 11:34:46', '2021-06-18 11:34:46', '0000-00-00 00:00:00'),
(10, 6, 14, 'Cruze', 1, 2500000, '2021-06-18 11:35:29', '2021-06-18 11:35:29', '0000-00-00 00:00:00'),
(11, 7, 18, 'CX-5', 1, 900000, '2021-06-18 21:16:12', '2021-06-18 21:16:12', '0000-00-00 00:00:00'),
(17, 10, 9, 'Camry 2.0Q', 1, 123000000, '2021-06-20 00:47:30', '2021-06-20 00:47:30', '0000-00-00 00:00:00'),
(18, 10, 10, 'S650 Maybach', 1, 50000000, '2021-06-20 00:47:30', '2021-06-20 00:47:30', '0000-00-00 00:00:00'),
(19, 10, 18, 'CX-5', 3, 900000, '2021-06-20 00:47:30', '2021-06-20 00:47:30', '0000-00-00 00:00:00'),
(20, 11, 9, 'Camry 2.0Q', 1, 123000000, '2021-06-20 04:58:53', '2021-06-20 04:58:53', '0000-00-00 00:00:00'),
(21, 11, 10, 'S650 Maybach', 1, 50000000, '2021-06-20 04:58:53', '2021-06-20 04:58:53', '0000-00-00 00:00:00'),
(30, 19, 12, 'Seltots', 1, 8000000, '2021-06-23 06:20:15', '2021-06-23 06:20:15', '0000-00-00 00:00:00'),
(31, 22, 11, 'G63-AMG', 1, 15000000, '2021-06-28 06:58:46', '2021-06-28 06:58:46', '0000-00-00 00:00:00'),
(32, 24, 20, '5555Maz 3 Premium', 1, 666666, '2021-06-28 07:04:12', '2021-06-28 07:04:12', '2023-06-27 19:04:12'),
(33, 25, 9, 'Camry 2.0Q', 1, 123000000, '2021-06-28 08:06:58', '2021-06-28 08:06:58', '0000-00-00 00:00:00'),
(34, 26, 9, 'Camry 2.0Q', 1, 123000000, '2021-06-28 08:11:44', '2021-06-28 08:11:44', '0000-00-00 00:00:00'),
(35, 27, 9, 'Camry 2.0Q', 1, 123000000, '2021-06-28 08:16:45', '2021-06-28 08:16:45', '2021-09-27 20:16:45'),
(36, 28, 10, 'S650 Maybach', 1, 6500000000, '2021-06-29 04:30:25', '2021-06-29 04:30:25', '2024-06-29 04:30:25'),
(37, 30, 12, 'Seltots', 1, 780000000, '2021-06-30 09:28:56', '2021-06-30 09:28:56', '2023-06-29 21:28:55'),
(38, 31, 11, 'G63-AMG', 1, 10500000000, '2021-06-30 09:56:36', '2021-06-30 09:56:36', '2024-06-29 21:56:36'),
(39, 31, 23, 'Camera 360 DCT', 1, 6500000, '2021-06-30 09:56:36', '2021-06-30 09:56:36', '2022-06-29 21:56:36'),
(40, 32, 15, 'Camer hành trình IROAD X9', 1, 1200000, '2021-07-01 07:21:20', '2021-07-01 07:21:20', '2022-06-30 19:21:20'),
(41, 32, 12, 'Seltots', 1, 780000000, '2021-07-01 07:21:20', '2021-07-01 07:21:20', '2023-06-30 19:21:20'),
(42, 32, 24, 'Màn hình Gotech GT8', 1, 12800000, '2021-07-01 07:21:20', '2021-07-01 07:21:20', '2022-06-30 19:21:20'),
(43, 33, 16, 'Bọc vô lăng ô tô SPARCO', 10, 1050000, '2021-07-01 07:32:57', '2021-07-01 07:32:57', '2021-09-30 19:32:57'),
(44, 33, 26, 'Cảm biến áp suất lốp STELLMATE', 1, 3650000, '2021-07-01 07:32:57', '2021-07-01 07:32:57', '2021-12-31 19:32:57'),
(45, 33, 14, 'Cruze', 1, 250000000, '2021-07-01 07:32:57', '2021-07-01 07:32:57', '2021-09-30 19:32:57'),
(46, 33, 30, 'Ốp bậc lên xuống INOX', 1, 890000, '2021-07-01 07:32:57', '2021-07-01 07:32:57', '2021-09-30 19:32:57'),
(47, 33, 29, 'Túi treo da Areon Gold Star VIP', 1, 850000, '2021-07-01 07:32:57', '2021-07-01 07:32:57', '2021-09-30 19:32:57'),
(48, 34, 24, 'Màn hình Gotech GT8', 1, 12800000, '2021-07-02 09:01:22', '2021-07-02 09:01:22', '2022-07-01 21:01:22'),
(49, 34, 40, 'Mercedes Benz GLC 200', 1, 1550000000, '2021-07-02 09:01:22', '2021-07-02 09:01:22', '2022-07-01 21:01:22'),
(50, 34, 30, 'Ốp bậc lên xuống INOX', 1, 890000, '2021-07-02 09:01:22', '2021-07-02 09:01:22', '2021-10-01 21:01:22'),
(51, 34, 35, 'Màn hình OLED C2', 1, 3500000, '2021-07-02 09:01:22', '2021-07-02 09:01:22', '2022-07-01 21:01:22'),
(52, 36, 10, 'S650 Maybach', 1, 6500000000, '2021-07-02 09:06:09', '2021-07-02 09:06:09', '2024-07-01 21:06:09'),
(53, 37, 18, 'CX-5', 1, 950000000, '2021-07-02 09:13:30', '2021-07-02 09:13:30', '2022-07-01 21:13:30'),
(54, 37, 25, 'Màn hình Gotech GT10 PRO', 1, 16500000, '2021-07-02 09:13:30', '2021-07-02 09:13:30', '2022-07-01 21:13:30'),
(55, 39, 18, 'CX-5', 1, 950000000, '2021-07-03 19:04:14', '2021-07-03 19:04:14', '2022-07-03 19:04:14'),
(56, 39, 25, 'Màn hình Gotech GT10 PRO', 1, 16500000, '2021-07-03 19:04:14', '2021-07-03 19:04:14', '2022-07-03 19:04:14'),
(57, 39, 26, 'Cảm biến áp suất lốp STELLMATE', 2, 3650000, '2021-07-03 19:04:14', '2021-07-03 19:04:14', '2022-01-03 19:04:14'),
(58, 40, 46, 'SANTAFE 2.2 DẦU PREMIUM 2021', 1, 1410000000, '2021-07-03 19:37:20', '2021-07-03 19:37:20', '2024-07-03 19:37:20'),
(59, 40, 15, 'Camer hành trình IROAD X9', 1, 1200000, '2021-07-03 19:37:20', '2021-07-03 19:37:20', '2022-07-03 19:37:20'),
(60, 40, 16, 'Bọc vô lăng ô tô SPARCO', 1, 1050000, '2021-07-03 19:37:20', '2021-07-03 19:37:20', '2021-10-03 19:37:20'),
(61, 40, 25, 'Màn hình Gotech GT10 PRO', 1, 16500000, '2021-07-03 19:37:20', '2021-07-03 19:37:20', '2022-07-03 19:37:20'),
(62, 40, 29, 'Túi treo da Areon Gold Star VIP', 1, 850000, '2021-07-03 19:37:20', '2021-07-03 19:37:20', '2021-10-03 19:37:20'),
(63, 40, 36, 'Gối tựa đầu\\ lưng cao su non', 2, 550000, '2021-07-03 19:37:20', '2021-07-03 19:37:20', '2021-10-03 19:37:20'),
(64, 40, 30, 'Ốp bậc lên xuống INOX', 1, 890000, '2021-07-03 19:37:20', '2021-07-03 19:37:20', '2021-10-03 19:37:20'),
(65, 40, 34, 'Vè che mưa các dòng xe', 4, 950000, '2021-07-03 19:37:20', '2021-07-03 19:37:20', '2021-10-03 19:37:20'),
(66, 40, 38, 'Gối tựa đầu khi ngủ', 3, 300000, '2021-07-03 19:37:20', '2021-07-03 19:37:20', '2021-10-03 19:37:20'),
(67, 41, 44, 'New KIA Sorento Signature D2.2 Diesel (7 Ghế Máy dầu)', 1, 1350000000, '2021-07-05 06:47:02', '2021-07-05 06:47:02', '2024-07-04 18:47:02'),
(68, 41, 14, 'Cruze', 1, 250000000, '2021-07-05 06:47:02', '2021-07-05 06:47:02', '2021-10-04 18:47:02'),
(69, 41, 26, 'Cảm biến áp suất lốp STELLMATE', 4, 3650000, '2021-07-05 06:47:02', '2021-07-05 06:47:02', '2022-01-04 18:47:02'),
(70, 41, 30, 'Ốp bậc lên xuống INOX', 1, 890000, '2021-07-05 06:47:02', '2021-07-05 06:47:02', '2021-10-04 18:47:02'),
(71, 41, 29, 'Túi treo da Areon Gold Star VIP', 1, 850000, '2021-07-05 06:47:02', '2021-07-05 06:47:02', '2021-10-04 18:47:02'),
(72, 41, 31, 'Nước hoa ô tô Areon Car VIP', 1, 600000, '2021-07-05 06:47:02', '2021-07-05 06:47:02', '2021-10-04 18:47:02'),
(73, 41, 32, 'Bọc vô lăng SPARCO SPC105BK', 1, 1100000, '2021-07-05 06:47:02', '2021-07-05 06:47:02', '2021-10-04 18:47:02'),
(74, 41, 34, 'Vè che mưa các dòng xe', 1, 950000, '2021-07-05 06:47:02', '2021-07-05 06:47:02', '2021-10-04 18:47:02'),
(75, 41, 36, 'Gối tựa đầu\\ lưng cao su non', 2, 550000, '2021-07-05 06:47:02', '2021-07-05 06:47:02', '2021-10-04 18:47:02'),
(76, 41, 35, 'Màn hình OLED C2', 1, 3500000, '2021-07-05 06:47:02', '2021-07-05 06:47:02', '2022-07-04 18:47:02'),
(77, 41, 38, 'Gối tựa đầu khi ngủ', 1, 300000, '2021-07-05 06:47:02', '2021-07-05 06:47:02', '2021-10-04 18:47:02'),
(78, 42, 39, 'CR - V', 1, 795000000, '2021-07-05 06:59:04', '2021-07-05 06:59:04', '2022-07-04 18:59:04'),
(79, 42, 17, 'Màn hình Androi OWIN', 1, 3000000, '2021-07-05 06:59:04', '2021-07-05 06:59:04', '2022-01-04 18:59:04'),
(80, 42, 15, 'Camer hành trình IROAD X9', 1, 1200000, '2021-07-05 06:59:04', '2021-07-05 06:59:04', '2022-07-04 18:59:04'),
(81, 42, 23, 'Camera 360 DCT', 1, 6500000, '2021-07-05 06:59:04', '2021-07-05 06:59:04', '2022-07-04 18:59:04'),
(82, 42, 28, 'Nước hoa ô tô Areon Car Black Perfume 50ml', 2, 300000, '2021-07-05 06:59:04', '2021-07-05 06:59:04', '2021-10-04 18:59:04'),
(83, 42, 26, 'Cảm biến áp suất lốp STELLMATE', 1, 3650000, '2021-07-05 06:59:04', '2021-07-05 06:59:04', '2022-01-04 18:59:04'),
(84, 42, 37, 'Tấm tựa lưng lưới', 2, 750000, '2021-07-05 06:59:04', '2021-07-05 06:59:04', '2021-10-04 18:59:04'),
(85, 42, 38, 'Gối tựa đầu khi ngủ', 4, 300000, '2021-07-05 06:59:04', '2021-07-05 06:59:04', '2021-10-04 18:59:04'),
(86, 42, 10, 'S650 Maybach', 1, 6500000000, '2021-07-05 06:59:04', '2021-07-05 06:59:04', '2024-07-04 18:59:04'),
(87, 43, 43, 'New KIA Sorento Deluxe D2.2 (Máy dầu)', 1, 1079000000, '2021-07-05 07:04:41', '2021-07-05 07:04:41', '2024-07-04 19:04:41'),
(88, 44, 20, 'Maz 3 Premiu', 2, 666000000, '2021-07-05 20:19:43', '2021-07-05 20:19:43', '2023-07-05 20:19:43'),
(89, 44, 45, 'KIA Cerato 1.6 AT Deluxe', 1, 529000000, '2021-07-05 20:19:43', '2021-07-05 20:19:43', '2024-07-05 20:19:43'),
(90, 44, 27, 'Cảm biến áp suất lốp Caramd', 4, 2800000, '2021-07-05 20:19:43', '2021-07-05 20:19:43', '2022-01-05 20:19:43'),
(91, 44, 29, 'Túi treo da Areon Gold Star VIP', 1, 850000, '2021-07-05 20:19:43', '2021-07-05 20:19:43', '2021-10-05 20:19:43'),
(92, 44, 30, 'Ốp bậc lên xuống INOX', 4, 890000, '2021-07-05 20:19:43', '2021-07-05 20:19:43', '2021-10-05 20:19:43'),
(93, 44, 34, 'Vè che mưa các dòng xe', 4, 950000, '2021-07-05 20:19:43', '2021-07-05 20:19:43', '2021-10-05 20:19:43'),
(94, 44, 37, 'Tấm tựa lưng lưới', 2, 750000, '2021-07-05 20:19:43', '2021-07-05 20:19:43', '2021-10-05 20:19:43'),
(95, 44, 35, 'Màn hình OLED C2', 1, 3500000, '2021-07-05 20:19:43', '2021-07-05 20:19:43', '2022-07-05 20:19:43'),
(96, 45, 9, 'Camry 2.0Q', 1, 1230000000, '2021-07-05 20:28:40', '2021-07-05 20:28:40', '2024-07-05 20:28:40'),
(97, 45, 23, 'Camera 360 DCT', 1, 6500000, '2021-07-05 20:28:40', '2021-07-05 20:28:40', '2022-07-05 20:28:40'),
(98, 45, 25, 'Màn hình Gotech GT10 PRO', 1, 16500000, '2021-07-05 20:28:40', '2021-07-05 20:28:40', '2022-07-05 20:28:40'),
(99, 45, 33, 'Bọc vô lăng Sparco chính hãng 1113RS', 1, 250000, '2021-07-05 20:28:40', '2021-07-05 20:28:40', '2021-10-05 20:28:40'),
(100, 45, 36, 'Gối tựa đầu\\ lưng cao su non', 1, 550000, '2021-07-05 20:28:40', '2021-07-05 20:28:40', '2021-10-05 20:28:40'),
(101, 46, 48, 'Mercedes C300 AMG', 1, 1679000000, '2021-07-05 21:21:43', '2021-07-05 21:21:43', '2022-07-05 21:21:43'),
(102, 46, 25, 'Màn hình Gotech GT10 PRO', 1, 16500000, '2021-07-05 21:21:43', '2021-07-05 21:21:43', '2022-07-05 21:21:43'),
(103, 46, 27, 'Cảm biến áp suất lốp Caramd', 5, 2800000, '2021-07-05 21:21:43', '2021-07-05 21:21:43', '2022-01-05 21:21:43'),
(104, 46, 47, 'toyota Vios 2020 số sàn', 1, 480000000, '2021-07-05 21:21:43', '2021-07-05 21:21:43', '2022-01-05 21:21:43'),
(105, 47, 68, 'Lexus LX 570 2021 Super Sport MBS', 1, 10250000000, '2021-07-06 05:02:51', '2021-07-06 05:02:51', '2024-07-06 05:02:51'),
(106, 47, 53, 'Vinfast Fadil 2021', 1, 336000000, '2021-07-06 05:02:51', '2021-07-06 05:02:51', '2023-07-06 05:02:51'),
(107, 47, 90, 'Cảnh báo điểm mù ô tô - Cảnh báo va chạm sớm ô tô', 1, 7550000, '2021-07-06 05:02:51', '2021-07-06 05:02:51', '2023-07-06 05:02:51'),
(108, 47, 93, 'Phim cách nhiệt Classis AUDI A1 | 100% Chính Hãng', 1, 8600000, '2021-07-06 05:02:51', '2021-07-06 05:02:51', '2022-07-06 05:02:51'),
(109, 47, 97, 'Màn Hình Android Elliview S4 – Camera 360 độ liền màn thế hệ mới', 1, 18980000, '2021-07-06 05:02:51', '2021-07-06 05:02:51', '2023-07-06 05:02:51'),
(110, 47, 75, 'Móc treo chìa khóa bằng da', 3, 300000, '2021-07-06 05:02:51', '2021-07-06 05:02:51', '2021-10-06 05:02:51'),
(111, 47, 76, 'Thảm Taplo da Cacbon', 2, 749000000, '2021-07-06 05:02:51', '2021-07-06 05:02:51', '2022-01-06 05:02:51'),
(112, 47, 77, 'Máy khử mùi ô tô - Máy khử mùi xe Webvision A8', 1, 2450000, '2021-07-06 05:02:51', '2021-07-06 05:02:51', '2022-07-06 05:02:51'),
(113, 47, 79, 'Bậc bước chân ô tô cao cấp', 1, 2800000, '2021-07-06 05:02:51', '2021-07-06 05:02:51', '2022-07-06 05:02:51');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `product`
--

CREATE TABLE `product` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantity` int(11) NOT NULL,
  `price` float NOT NULL,
  `warranty` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `promotional` float NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `idCategory` int(11) NOT NULL,
  `idProductType` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `qty_buy` int(11) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `product`
--

INSERT INTO `product` (`id`, `name`, `slug`, `description`, `quantity`, `price`, `warranty`, `promotional`, `image`, `idCategory`, `idProductType`, `status`, `created_at`, `updated_at`, `qty_buy`) VALUES
(9, 'Camry 2.0Q', 'camry-2.0q', '<p><strong>Toyota Camry 2.5Q</strong></p>\r\n\r\n<p><strong>Số chỗ ngồi : 5 chỗ</strong></p>\r\n\r\n<p><strong>Kiểu d&aacute;ng : Sedan</strong></p>\r\n\r\n<p><strong>Nhi&ecirc;n liệu : Xăng</strong></p>\r\n\r\n<p><strong>Xuất xứ : Xe nhập Th&aacute;i Lan</strong></p>\r\n\r\n<p><strong>Số tự động 6 cấp</strong></p>\r\n\r\n<p><strong>Động cơ xăng dung t&iacute;ch 2.494 cc</strong></p>', 2, 1230000000, '4', 0, 'Wed-04-21212121-246790872-camry.png', 1, 1, 1, '2021-04-28 09:53:00', '2021-07-05 20:28:40', 8),
(10, 'S650 Maybach', 's650-maybach', '<p>Maybach S650 lu&ocirc;n được đánh giá cao, đi đ&acirc;̀u v&ecirc;̀ xu hướng ngoại th&acirc;́t. Xe được trang bị cụm đèn trước Multi-Beam LED với c&acirc;́u tạo 84 tinh th&ecirc;̉, tích hợp 3 dải LED chi&ecirc;́u sáng ban ngày th&ocirc;ng minh. H&ecirc;̣ th&ocirc;́ng có th&ecirc;̉ tự đ&ocirc;̣ng b&acirc;̣t/tắt đèn pha và tùy chỉnh đ&ocirc;̣ sáng dựa tr&ecirc;n đi&ecirc;̀u ki&ecirc;̣n mặt đường.</p>\r\n\r\n<p>Ph&acirc;̀n đu&ocirc;i xe là đèn h&acirc;̣u ki&ecirc;̉u pha l&ecirc; bo tròn với 3 dải LED hi&ecirc;̣u ứng ánh sao. Đi&ecirc;̉m nh&acirc;́n khác ở ph&acirc;̀n đu&ocirc;i xe chính là hai &ocirc;́ng xả đ&ocirc;́i xứng vi&ecirc;̀n mạ chrome.</p>\r\n\r\n<p>Nhìn từ sườn, b&ocirc;̣ m&acirc;m 20 inch Maybach c&ocirc;̉ đi&ecirc;̉n mang lại vẻ đẹp khó cưỡng cho S650. Xe còn được tích hợp h&ecirc;̣ th&ocirc;́ng MAGIC VISION CONTROL với gạt mưa tự đ&ocirc;̣ng, sưởi kính.</p>\r\n\r\n<p>N&ocirc;̣i th&acirc;́t b&ecirc;n trong&nbsp;Mercedes-Maybach S650</p>\r\n\r\n<p>L&ocirc;̣ng l&acirc;̃y, xa hoa, ti&ecirc;̣n nghi và đẳng c&acirc;́p là những từ dùng đ&ecirc;̉ mi&ecirc;u tả n&ocirc;̣i th&acirc;́t của Maybach S650. Sở hữu chi&ecirc;̀u dài cơ sở l&ecirc;n tới 3365mm, S650 cung c&acirc;́p kh&ocirc;ng gian n&ocirc;̣i th&acirc;́t cực kỳ thoải mái cho các vị khách VIP.</p>\r\n\r\n<p>Kh&ocirc;ng chỉ vậy, kh&ocirc;ng gian b&ecirc;n trong c&ograve;n được mở rộng, gần gũi với thi&ecirc;n nhi&ecirc;n hơn khi trang bị cửa sổ trời si&ecirc;u rộng với k&iacute;nh đổi m&agrave;u MAGIC SKY CONTROL. Đ&acirc;y ch&iacute;nh l&agrave; ưu thế lớn nhất gi&uacute;p c&aacute;c phi&ecirc;n bản Mercedes-Maybach v&agrave; mẫu xe n&agrave;y n&oacute;i ri&ecirc;ng khẳng định vị thế so với c&aacute;c đối thủ cạnh tranh.</p>\r\n\r\n<p>Các chi ti&ecirc;́t &ocirc;́p g&ocirc;̃ và &ocirc;́p da cao c&acirc;́p đan xen vào nhau tạo ra m&ocirc;̣t kh&ocirc;ng gian n&ocirc;̣i th&acirc;́t cực kỳ sang trọng. Đèn vi&ecirc;̀n n&ocirc;̣i th&acirc;́t có tới 64 màu và 10 ki&ecirc;̉u ph&ocirc;́i khác nhau, linh hoạt theo t&acirc;m trạng của hành khách.</p>', 3, 6500000000, '4', 0, 'Thu-04-21212121-985264480-s650.png.jpg', 1, 3, 1, '2021-04-28 23:51:50', '2021-07-05 06:59:04', 6),
(11, 'G63-AMG', 'g63-amg', '<p>L&agrave; một trong những d&ograve;ng SUV địa h&igrave;nh đến từ Mercedes-Benz, G-Class sở hữu khả năng off-road tuyệt vời. Trong đ&oacute;, bản hiệu suất cao Mercedes-AMG G 63 được nhiều kh&aacute;ch h&agrave;ng ưa chuộng bởi khả năng vận h&agrave;nh mạnh mẽ, hỗ trợ người l&aacute;i tối đa trong việc chinh phục những địa h&igrave;nh kh&oacute;. Sắp tới đ&acirc;y, h&atilde;ng sẽ cho ra mắt Mercedes-AMG G 63 phi&ecirc;n bản mới với nhiều cải tiến nhằm mang những trải nghiệm tốt hơn nữa đến kh&aacute;ch h&agrave;ng.</p>', 5, 10500000000, '4', 0, 'Thu-04-21212121-366387394-g63.jpg', 1, 3, 1, '2021-04-28 23:54:08', '2021-07-02 09:09:50', 4),
(12, 'Seltots', 'seltots', '<p><strong>KIA Seltos Premium 1.4 Turbo</strong>&nbsp;được lắp r&aacute;p tại nh&agrave; m&aacute;y đặt tại khu c&ocirc;ng nghiệp Chu Lai.&nbsp;Kia Seltos gia nhập ph&acirc;n kh&uacute;c SUV hạng B với những c&aacute;i t&ecirc;n quen thuộc như Ford EcoSport, Hyundai Kona, Honda HR-V v&agrave; mới nhất c&oacute; MG ZS.</p>\r\n\r\n<p>Mẫu SUV cỡ nhỏ Kia Seltos vừa được ra mắt tại thị trường Việt Nam. Được biết đối thủ của Ford EcoSport hay Hyundai Kona được b&aacute;n ra với gi&aacute; từ 589 triệu đồng.</p>', 3, 780000000, '3', 0, 'Thu-04-21212121-1452227066-tskt-kia-seltos-2020-viet-nam-tuvanmuaxe-5.jpg', 1, 4, 1, '2021-04-29 00:26:35', '2021-07-01 07:21:20', 3),
(13, 'Getz', 'getz', '<p>Sx năm 2010<br />\r\nĐăng k&yacute; năm 2010<br />\r\nT&igrave;nh trạng: Xe biển tỉnh đ&atilde; chạy 1 vạn 9 km</p>\r\n\r\n<p>Hộp số:Số tự động</p>\r\n\r\n<p>Dẫn động:FWD - Dẫn động cầu trước</p>\r\n\r\n<p>Ti&ecirc;u thụ nhi&ecirc;n liệu:L/100Km</p>\r\n\r\n<p>uất xứ:Nhập khẩu</p>\r\n\r\n<p>T&igrave;nh trạng:Xe đ&atilde; d&ugrave;ng</p>\r\n\r\n<p>D&ograve;ng xe: SUV</p>\r\n\r\n<p>Số Km đ&atilde; đi:19000 Km</p>\r\n\r\n<p>M&agrave;u ngoại thất:Đỏ đ&ocirc;</p>\r\n\r\n<p>M&agrave;u nội thất: Đen</p>\r\n\r\n<p>Số cửa:4 cửa</p>\r\n\r\n<p>Số chỗ ngồi:4&nbsp;chỗ</p>', 2, 300000000, '0', 295000000, 'Wed-05-21212121-745231880-hyundai-getz-11-mt-2-f4be.jpg', 2, 5, 1, '2021-05-12 06:50:54', '2021-07-06 04:57:07', 2),
(14, 'Cruze', 'cruze', '<p>Sx năm 2009<br />\r\nĐăng k&yacute; năm 2009<br />\r\nT&igrave;nh trạng: Xe biển tỉnh đ&atilde; chạy 1 vạn 9 km</p>\r\n\r\n<p>Hộp số:Số tự động</p>\r\n\r\n<p>Dẫn động:FWD - Dẫn động cầu trước</p>\r\n\r\n<p>Ti&ecirc;u thụ nhi&ecirc;n liệu:L/100Km</p>\r\n\r\n<p>uất xứ:Nhập khẩu</p>\r\n\r\n<p>T&igrave;nh trạng:Xe đ&atilde; d&ugrave;ng</p>\r\n\r\n<p>D&ograve;ng xe: Sedan</p>\r\n\r\n<p>Số Km đ&atilde; đi:19000 Km</p>\r\n\r\n<p>M&agrave;u ngoại thất:Trắng</p>\r\n\r\n<p>M&agrave;u nội thất: Đen</p>\r\n\r\n<p>Số cửa:4 cửa</p>\r\n\r\n<p>Số chỗ ngồi:4&nbsp;chỗ</p>', 3, 250000000, '0', 0, 'Wed-05-21212121-1950426168-gia-xe-chevrolet-cru-73f6.jpg', 2, 11, 1, '2021-05-12 06:53:00', '2021-07-06 04:48:09', 2),
(15, 'Camer hành trình IROAD X9', 'camer-hanh-trinh-iroad-x9', '<p>T&iacute;nh năng cơ bản của camera xe hơi iroad X9:</p>\r\n\r\n<p>- Cảm biến h&igrave;nh ảnh trước SONY STARVIS.</p>\r\n\r\n<p>- Cảm biến h&igrave;nh ảnh sau SONY Exmor.</p>\r\n\r\n<p>- Chế độ th&ocirc;ng minh NIGHT VISION.</p>\r\n\r\n<p>- Camera trước &amp; sau - FHD; Camera sau - FHD.</p>\r\n\r\n<p>- Hỗ trợ người l&aacute;i Warning System.</p>\r\n\r\n<p>- Kết nối Wifi &amp; IROAD App.</p>\r\n\r\n<p>- Ống k&iacute;nh xoay 90 độ.</p>\r\n\r\n<p>- Nguồn 2.9W.</p>\r\n\r\n<p>- G&oacute;c quay rộng 150 độ.</p>\r\n\r\n<p>- Ngăn ngừa cạn ắc quy.</p>\r\n\r\n<p>- Camera trước &amp; sau - 30fps.</p>\r\n\r\n<p>- Kh&ocirc;ng cần format - Auto Recovery.</p>', 997, 1200000, '2', 0, 'Wed-05-21212121-147378642-big_camera-hanh-trinh-iroad-x9.jpg', 4, 13, 1, '2021-05-12 07:01:07', '2021-07-05 06:59:04', 3),
(16, 'Bọc vô lăng ô tô SPARCO', 'boc-vo-lang-o-to-sparco', '<p>T&iacute;nh năng cơ bản của camera xe hơi iroad X9:</p>\r\n\r\n<p>- Cảm biến h&igrave;nh ảnh trước SONY STARVIS.</p>\r\n\r\n<p>- Cảm biến h&igrave;nh ảnh sau SONY Exmor.</p>\r\n\r\n<p>- Chế độ th&ocirc;ng minh NIGHT VISION.</p>\r\n\r\n<p>- Camera trước &amp; sau - FHD; Camera sau - FHD.</p>\r\n\r\n<p>- Hỗ trợ người l&aacute;i Warning System.</p>\r\n\r\n<p>- Kết nối Wifi &amp; IROAD App.</p>\r\n\r\n<p>- Ống k&iacute;nh xoay 90 độ.</p>\r\n\r\n<p>- Nguồn 2.9W.</p>\r\n\r\n<p>- G&oacute;c quay rộng 150 độ.</p>\r\n\r\n<p>- Ngăn ngừa cạn ắc quy.</p>\r\n\r\n<p>- Camera trước &amp; sau - 30fps.</p>\r\n\r\n<p>- Kh&ocirc;ng cần format - Auto Recovery.</p>', 989, 1050000, '0', 0, 'Wed-05-21212121-1691707065-big_boc-vo-lang-sparco-1111gr.jpg', 4, 14, 1, '2021-05-12 07:02:10', '2021-07-03 19:37:20', 11),
(17, 'Màn hình Androi OWIN', 'man-hinh-androi-owin', '<p><strong>M&agrave;n h&igrave;nh DVD Owin cho xe &ocirc; t&ocirc;, đầu DVD &ocirc; t&ocirc;</strong></p>\r\n\r\n<p>&nbsp; &nbsp;C&ocirc;ng nghệ hiện đại đang ph&aacute;t triển theo từng ng&agrave;y, c&aacute;c sản phẩm d&agrave;nh cho xe &ocirc; t&ocirc; cũng theo đ&oacute; m&agrave; ng&agrave;y c&agrave;ng ph&aacute;t triển theo. Ng&agrave;y nay nhu cầu giải tr&iacute; tr&ecirc;n xe &ocirc; t&ocirc; ng&agrave;y c&agrave;ng được c&aacute;c giới l&aacute;i xe quan t&acirc;m nhiều, đặc biệt đầu DVD &ocirc; t&ocirc; kh&ocirc;ng chỉ để đọc đĩa nhạc, nghe radio FM, m&agrave; kh&aacute;ch h&agrave;ng c&ograve;n đ&ograve;i hỏi nhiều hơn như thế. Đầu DVD cho &ocirc; t&ocirc;, m&agrave;n h&igrave;nh DVD Owin l&agrave; thiết bị tiện &iacute;ch gi&uacute;p phục vụ nhu cầu giải tr&iacute; cũng như cập nhật c&aacute;c th&ocirc;ng tin hữu &iacute;ch x&atilde; hội d&agrave;nh cho t&agrave;i xế l&aacute;i xe v&agrave; c&aacute;c h&agrave;nh kh&aacute;ch đồng h&agrave;nh.</p>\r\n\r\n<p>&nbsp; &nbsp;Đ&acirc;y cũng ch&iacute;nh l&agrave; l&yacute; do m&agrave; nhu cầu thay thế lắp đầu DVD android cho &ocirc; t&ocirc; ng&agrave;y c&agrave;ng trở n&ecirc;n phổ biến hơn. Vậy t&iacute;nh năng đầu DVD Owin &ocirc; t&ocirc; như thế n&agrave;o, h&atilde;y c&ugrave;ng t&igrave;m hiểu th&ocirc;ng qua b&agrave;i&nbsp;<a href=\"https://shopauto.vn/man-hinh-dvd-owin-cho-xe-o-to-dau-dvd-o-to.html\" target=\"_blank\">đầu DVD Owin &ocirc; t&ocirc;</a>&nbsp;tốt nhất hiện nay.</p>', 399, 4000000, '1', 3000000, 'Wed-05-21212121-533880241-big_man-hinh-dvd-owin-cho-xe-o-to-dau-dvd-o-to.jpg', 4, 15, 1, '2021-05-12 07:03:24', '2021-07-05 06:59:04', 1),
(18, 'CX-5', 'cx-5', '<p>CX-5&nbsp;&nbsp;2020</p>\r\n\r\n<p>K&iacute;ch thước tổng thể (DxRxC): 4.810 x 1.900 x 1.695 mm</p>\r\n\r\n<p>Chiều d&agrave;i cơ sở: 2.815 mm</p>\r\n\r\n<p>Số chỗ ngồi: 4&nbsp;chỗ</p>\r\n\r\n<p>Động cơ: Smartstream D2.2</p>\r\n\r\n<p>C&ocirc;ng suất cực đại: 198 Hp / 3.800 rpm</p>\r\n\r\n<p>M&ocirc; men xoắn cực đại: 440 Nm / 1.750 - 2.750 rpm</p>', 3, 995000000, '2', 950000000, '1625070925.jpg', 1, 16, 1, '2021-05-18 03:33:15', '2021-07-06 04:45:24', 6),
(20, 'Maz 3 Premiu', 'maz-3-premiu', '<p>Mazda 3 2020</p>\r\n\r\n<p>K&iacute;ch thước tổng thể (DxRxC): 4.810 x 1.900 x 1.695 mm</p>\r\n\r\n<p>Chiều d&agrave;i cơ sở: 2.815 mm</p>\r\n\r\n<p>Số chỗ ngồi: 4&nbsp;chỗ</p>\r\n\r\n<p>Động cơ: Smartstream D2.2</p>\r\n\r\n<p>C&ocirc;ng suất cực đại: 198 Hp / 3.800 rpm</p>\r\n\r\n<p>M&ocirc; men xoắn cực đại: 440 Nm / 1.750 - 2.750 rpm</p>', 8, 685000000, '3', 666000000, '1625070766.jpg', 1, 16, 1, '2021-06-21 19:19:41', '2021-07-06 04:45:51', 3),
(23, 'Camera 360 DCT', 'camera-360-dct', '<p>Camera 360 DCT c&oacute; thể quay h&agrave;nh tr&igrave;nh kể cả khi xe tắt m&aacute;y, gi&aacute;m s&aacute;t t&igrave;nh h&igrave;nh xung quanh xe 24/24. Bất kỳ h&agrave;nh động n&agrave;o ảnh hưởng đến xe của bạn sẽ đều được ghi lại một c&aacute;ch r&otilde; r&agrave;ng. V&agrave; c&ograve;n cực th&ocirc;ng minh khi điện &aacute;p Acquy dưới 12.4 ampe, camera sẽ tự động tắt để bảo vệ ắc quy.</p>', 20, 6500000, '2', 0, '1625071895.jpg', 4, 21, 1, '2021-06-30 09:51:35', '2021-07-06 04:17:30', 3),
(24, 'Màn hình Gotech GT8', 'man-hinh-gotech-gt8', '<ul>\r\n	<li>CPU: 8 nh&acirc;n - 1.8Ghz</li>\r\n	<li>RAM: 3GB - DDR4</li>\r\n	<li>ROM: 32GB</li>\r\n	<li>M&agrave;n h&igrave;nh: QLED HD</li>\r\n	<li>\r\n	<p><strong>Khuyến Mại</strong></p>\r\n	</li>\r\n	<li>Tặng SIM 4G</li>\r\n	<li>Tặng camera h&agrave;nh tr&igrave;nh</li>\r\n	<li>Tặng thẻ nhớ 32GB</li>\r\n	<li>Tặng Vietmap S1 bản quyền</li>\r\n	<li>Lắp đặt tại nh&agrave; miễn ph&iacute;</li>\r\n	<li>Bảo h&agrave;nh 2 năm 1 đổi 1</li>\r\n	<li>D&ugrave;ng thử 30 ng&agrave;y miễn ph&iacute;</li>\r\n</ul>', 18, 15500000, '2', 12800000, '1625146149.png', 4, 15, 1, '2021-07-01 06:29:09', '2021-07-02 09:01:22', 2),
(25, 'Màn hình Gotech GT10 PRO', 'man-hinh-gotech-gt10-pro', '<ul>\r\n	<li>CPU: 8 nh&acirc;n - 1.8Ghz</li>\r\n	<li>RAM: 4GB - DDR4</li>\r\n	<li>ROM: 64GB</li>\r\n	<li>M&agrave;n h&igrave;nh: QLED HD</li>\r\n	<li>\r\n	<p><strong>Khuyến Mại</strong></p>\r\n	</li>\r\n	<li>Tặng SIM 4G</li>\r\n	<li>Tặng camera h&agrave;nh tr&igrave;nh</li>\r\n	<li>Tặng thẻ nhớ 32GB</li>\r\n	<li>Tặng Vietmap S1 bản quyền</li>\r\n	<li>Lắp đặt tại nh&agrave; miễn ph&iacute;</li>\r\n	<li>Bảo h&agrave;nh 2 năm 1 đổi 1</li>\r\n	<li>D&ugrave;ng thử 30 ng&agrave;y miễn ph&iacute;</li>\r\n</ul>', 5, 16500000, '2', 0, '1625146289.png', 4, 15, 1, '2021-07-01 06:31:29', '2021-07-05 21:21:43', 5),
(26, 'Cảm biến áp suất lốp STELLMATE', 'cam-bien-ap-suat-lop-stellmate', '<ul>\r\n	<li>Kết nối qua cổng chờ Zin theo xe.</li>\r\n	<li>Hiển thị v&agrave; c&agrave;i đặt tr&ecirc;n điện thoại.</li>\r\n	<li>Lắp đặt cho d&ograve;ng xe Honda,Toyota, Mitsubishi.</li>\r\n	<li>4 van cảm biến trong.</li>\r\n	<li>Hiển thị cả th&ocirc;ng số &aacute;p suất v&agrave; nhiệt độ lốp.</li>\r\n	<li>Hiển thị &aacute;p suất lốp l&ecirc;n m&agrave;n h&igrave;nh taplo xe (ODO).</li>\r\n	<li>Xem được th&ocirc;ng số đồng thời tr&ecirc;n smartphone qua kết nối bluetooth.</li>\r\n	<li>C&agrave;i đặt ngưỡng cảnh b&aacute;o &aacute;p suất, đảo lốp, khớp lốp tr&ecirc;n điện thoại dễ d&agrave;ng.</li>\r\n</ul>', 17, 3650000, '1', 0, '1625146506.jpg', 4, 28, 1, '2021-07-01 06:35:06', '2021-07-05 06:59:04', 8),
(27, 'Cảm biến áp suất lốp Caramd', 'cam-bien-ap-suat-lop-caramd', '<p>C&aacute;ch để lắp đặt cảm biến &aacute;p suất lốp Carcam cũng rất dễ d&agrave;ng, v&agrave; setup m&agrave;n h&igrave;nh hiển thị cũng rất đơn giản, bạn c&oacute; thể ho&agrave;n to&agrave;n thực hiện tại nh&agrave; theo quyển hướng dẫn của nh&agrave; sản xuất. Với nhiều những đặc điểm nổi bật như vậy th&igrave; gi&aacute; th&agrave;nh cho một chiếc cảm biến &aacute;p suất lốp Carcam l&agrave; hơn 2 triệu đồng cũng c&oacute; thể chấp nhận được.</p>', 1, 2800000, '1', 0, '1625146653.jpg', 4, 28, 1, '2021-07-01 06:37:33', '2021-07-05 21:21:43', 9),
(28, 'Nước hoa ô tô Areon Car Black Perfume 50ml', 'nuoc-hoa-o-to-areon-car-black-perfume-50ml', '<p>Thương hiệu: Areon</p>\r\n\r\n<p>Xuất xứ: Bulgaria</p>\r\n\r\n<p>Khử m&ugrave;i h&ocirc;i, tạo hương thơm m&aacute;t, chống say xe</p>\r\n\r\n<p>Chiết xuất 100% thi&ecirc;n nhi&ecirc;n, an to&agrave;n sức khỏe</p>\r\n\r\n<p>Thời gian sử dụng: 3 th&aacute;ng</p>\r\n\r\n<p>M&ugrave;i hương: T&aacute;o &amp; Quế kết hợp, thanh m&aacute;t, chống say xe</p>', 48, 350000, '0', 300000, '1625146832.jpg', 4, 17, 1, '2021-07-01 06:40:32', '2021-07-05 06:59:04', 2),
(29, 'Túi treo da Areon Gold Star VIP', 'tui-treo-da-areon-gold-star-vip', '<p>T&uacute;i thơm xe hơi hạng nhất thế giới</p>\r\n\r\n<p>Thiết kế da sang trọng, cầu k&igrave;, tinh tế</p>\r\n\r\n<p>Chứa đ&aacute; phong thủy &amp; nước hoa Bulgaria</p>\r\n\r\n<p>Đem t&agrave;i lộc, may mắn cho chủ nh&acirc;n</p>', 297, 850000, '0', 0, '1625146982.jpg', 4, 17, 1, '2021-07-01 06:43:02', '2021-07-05 20:19:43', 4),
(30, 'Ốp bậc lên xuống INOX', 'op-bac-len-xuong-inox', '<p>Ốp bậc l&ecirc;n xuống chống trầy xước sơn xe<br />\r\nSản xuất bằng inox 340&nbsp;</p>', 142, 1000000, '0', 890000, '1625147258.jpg', 4, 36, 1, '2021-07-01 06:47:38', '2021-07-05 20:19:43', 8),
(31, 'Nước hoa ô tô Areon Car VIP', 'nuoc-hoa-o-to-areon-car-vip', '<p>Thương hiệu: Areon (thương hiệu nước hoa &ocirc; t&ocirc; số 1 thế giới)</p>\r\n\r\n<p>Xuất xứ: Bulgaria</p>\r\n\r\n<p>Khử m&ugrave;i, l&agrave;m thơm xe &ocirc; t&ocirc;, kh&ocirc;ng gian gia đ&igrave;nh, văn ph&ograve;ng</p>\r\n\r\n<p>C&oacute;&nbsp;l&aacute; lưu hương chuy&ecirc;n dụng đi k&egrave;m</p>\r\n\r\n<p>Thời gian sử dụng: 5 &ndash; 6 th&aacute;ng</p>\r\n\r\n<p>M&ugrave;i hương: Black, c&aacute; t&iacute;nh, mạnh mẽ</p>', 149, 600000, '0', 0, '1625148017.jpg', 4, 17, 1, '2021-07-01 07:00:17', '2021-07-05 06:47:02', 1),
(32, 'Bọc vô lăng SPARCO SPC105BK', 'boc-vo-lang-sparco-spc105bk', '<p>Bọc v&ocirc; lăng da xuất cứ Malaysia</p>\r\n\r\n<p>Chất liệu da b&ecirc; cho cảm gi&aacute;c cầm nắm tốt v&agrave; thoải m&aacute;i nhất cho người l&aacute;i xe</p>', 99, 1100000, '0', 0, '1625148222.jpg', 4, 14, 1, '2021-07-01 07:03:42', '2021-07-05 06:47:02', 1),
(33, 'Bọc vô lăng Sparco chính hãng 1113RS', 'bo??c-vo?la??ng-sparco-chi??nh-ha??ng-1113rs', '<p>V&ocirc; lăng của bạn đ&atilde; thực sự khiến bạn an t&acirc;m v&agrave; thoải m&aacute;i khi lai xe chưa, với thời tiết n&oacute;ng bức th&igrave; mồ h&ocirc;i tay l&agrave; kẻ th&ugrave; cho c&aacute;c b&aacute;c T&agrave;i khi cầm v&ocirc; lăng. Về mặt thẩm mỹ th&igrave; v&ocirc; lăng sẵn tr&ecirc;n xe cũng c&oacute; thể g&acirc;y sự nh&agrave;m ch&aacute;n cho c&aacute;c chủ xe. Bạn đ&atilde; cảm thấy thật sự y&ecirc;n t&acirc;m với chiếc bọc v&ocirc; lăng hiện tại của m&igrave;nh khi l&aacute;i xe đường d&agrave;i trong thời tiết nắng n&oacute;ng tay bị ra nhiều mồ h&ocirc;i g&acirc;y trơn trượt. Hay bạn chưa cảm nhận được sự sang trọng, hiện đại, c&aacute; t&iacute;nh thật sự ở chiếc xế hộp của m&igrave;nh, ngo&agrave;i tăng sự an to&agrave;n cho t&iacute;nh mạng, sản phẩm bọc<strong>&nbsp;v&ocirc; lăng Sparco 1113RS</strong>&nbsp;c&ograve;n gi&uacute;p chiếc xe của bạn nổi bật hơn bao giờ hết.</p>', 199, 250000, '0', 0, '1625148405.jpg', 4, 14, 1, '2021-07-01 07:06:45', '2021-07-05 20:28:40', 1),
(34, 'Vè che mưa các dòng xe', 've-che-mua-cac-dong-xe', '<p>V&egrave; che mưa xuất xứ Singapore&nbsp;<br />\r\nSản xuất bằng chất liệu nhựa cứng c&oacute; khả năng chịu được t&aacute;c dụng của ngoại lực</p>', 241, 950000, '0', 0, '1625148658.jpg', 4, 7, 1, '2021-07-01 07:10:58', '2021-07-05 20:19:43', 9),
(35, 'Màn hình OLED C2', 'man-hinh-oled-c2', '<p>C&ocirc;ng nghệ hiện đại đang ph&aacute;t triển theo từng ng&agrave;y, c&aacute;c sản phẩm d&agrave;nh cho xe &ocirc; t&ocirc; cũng theo đ&oacute; m&agrave; ng&agrave;y c&agrave;ng ph&aacute;t triển theo. Ng&agrave;y nay nhu cầu giải tr&iacute; tr&ecirc;n xe &ocirc; t&ocirc; ng&agrave;y c&agrave;ng được c&aacute;c giới l&aacute;i xe quan t&acirc;m nhiều, đặc biệt đầu DVD &ocirc; t&ocirc; kh&ocirc;ng chỉ để đọc đĩa nhạc, nghe radio FM, m&agrave; kh&aacute;ch h&agrave;ng c&ograve;n đ&ograve;i hỏi nhiều hơn như thế.</p>', 27, 3500000, '2', 0, '1625148821.jpg', 4, 15, 1, '2021-07-01 07:13:41', '2021-07-05 20:19:43', 3),
(36, 'Gối tựa đầu\\ lưng cao su non', 'goi-tua-daulung-cao-su-non', '<p>Gối đầu v&agrave; tựa lưng cao su non mẫu mới thiết kế n&acirc;ng đỡ cơ thể</p>\r\n\r\n<p>- Gối đầu v&agrave; tựa lưng c&oacute; độ d&agrave;y tốt rất &ecirc;m &aacute;i</p>\r\n\r\n<p>- Kiểu d&aacute;ng thiết kế mới cho hiệu quả tuyệt vời</p>\r\n\r\n<p>- Gối đầu v&agrave; tựa lưng được l&agrave;m bằng cao su non đ&agrave;n hồi m&atilde;i m&atilde;i. Đặc t&iacute;nh đ&agrave;n hồi chậm gi&uacute;p n&acirc;ng đỡ ph&acirc;n bố lực đồng đều l&ecirc;n cơ thể</p>\r\n\r\n<p>- B&ecirc;n ngo&agrave;i gối lớp vải 3d cao cấp tho&aacute;ng m&aacute;t, c&oacute; m&agrave;u sắc sang trọng.</p>\r\n\r\n<p>- Kiểu d&aacute;ng thiết kế hiện đại, thời trang, giảm mỏi cổ khi l&aacute;i xe.</p>\r\n\r\n<p>- M&agrave;u sắc sẵn c&oacute; 2 loại l&agrave; m&agrave;u kem v&agrave; m&agrave;u đen</p>', 295, 600000, '0', 550000, '1625240489.jpg', 4, 31, 1, '2021-07-02 08:41:29', '2021-07-05 20:28:40', 5),
(37, 'Tấm tựa lưng lưới', 'tam-tua-lung-luoi', '<p>- Lưới tựa lưng hạt gỗ massage tho&aacute;ng m&aacute;t sử dụng cho &ocirc; t&ocirc;</p>\r\n\r\n<p>- Vật liệu: lưới tựa Nylon + nan tre + khung th&eacute;p</p>\r\n\r\n<p>- Bao gồm cả phần ghế tựa v&agrave; chỗ ngồi</p>\r\n\r\n<p>- C&oacute; c&aacute;c loại m&agrave;u sắc kiểu d&aacute;ng như h&igrave;nh dưới</p>\r\n\r\n<p>- K&iacute;ch thước: Chiều cao 55 cm * 48 cm Dưới 46 cm * 48 cm</p>', 146, 750000, '0', 0, '1625240582.jpg', 4, 31, 1, '2021-07-02 08:43:02', '2021-07-05 20:19:43', 4),
(38, 'Gối tựa đầu khi ngủ', 'goi-tua-dau-khi-ngu', '<p>Gi&aacute; tựa đầu tr&ecirc;n &ocirc; t&ocirc; gi&uacute;p bạn c&oacute; chố tựa đầu khi ngủ, đỡ bị nghẹo cổ g&acirc;y ảnh hưởng cột sống</p>\r\n\r\n<p>- K&iacute;ch thước 41,0 cm * 19,0 cm * 6,0 cm</p>\r\n\r\n<p>- Sản phẩm rất thiết thực v&agrave; cần thiết cho c&aacute;c bạn hay ngủ gật khi đi tr&ecirc;n xe, đảm bảo cột sống kh&ocirc;ng bị ảnh hưởng khi ngủ gật</p>', 192, 300000, '0', 0, '1625240648.jpg', 4, 31, 1, '2021-07-02 08:44:08', '2021-07-05 06:59:04', 8),
(39, 'CR - V', 'cr-v', '<p>Xe HONDA CRV Sx năm 2017</p>\r\n\r\n<p>Đăng k&yacute; năm 2017 Xe biển th&agrave;nh phố ,</p>\r\n\r\n<p>Đ&atilde; chạy 8 vạn km</p>', 4, 795000000, '2', 0, '1625240786.jpg', 2, 40, 1, '2021-07-02 08:46:26', '2021-07-05 06:59:04', 1),
(40, 'Mercedes Benz GLC 200', 'mercedes-benz-glc-200', '<p>Xe MERCEDES GLC 200 Sx năm 2019</p>\r\n\r\n<p>Đăng k&yacute; năm 2019</p>\r\n\r\n<p>T&igrave;nh trạng: Xe biển th&agrave;nh phố , đ&atilde; chạy 1 vạn 6 km</p>', 2, 1550000000, '2', 0, '1625240879.jpg', 2, 24, 1, '2021-07-02 08:47:59', '2021-07-02 09:01:22', 1),
(41, 'Hyundai Tucson', 'hyundai-tucson', '<p>Sx năm 2020</p>\r\n\r\n<p>Đăng k&yacute; năm 2020</p>\r\n\r\n<p>T&igrave;nh trạng: Xe biển tỉnh, đ&atilde; chạy 8000 km</p>', 10, 895000000, '2', 0, '1625240993.jpg', 2, 5, 1, '2021-07-02 08:49:53', '2021-07-02 08:49:53', 0),
(42, 'Camry 2.0G', 'camry-2.0g', '<p>Xe TOYOTA CAMRY<br />\r\nSx năm 2019<br />\r\nĐăng k&yacute; năm 2019<br />\r\nT&igrave;nh trạng: Xe biển tỉnh đ&atilde; chạy 1 vạn 9 km</p>\r\n\r\n<p>Hộp số:Số tự động</p>\r\n\r\n<p>Dẫn động:FWD - Dẫn động cầu trước</p>\r\n\r\n<p>Ti&ecirc;u thụ nhi&ecirc;n liệu:L/100Km</p>\r\n\r\n<p>uất xứ:Nhập khẩu</p>\r\n\r\n<p>T&igrave;nh trạng:Xe đ&atilde; d&ugrave;ng</p>\r\n\r\n<p>D&ograve;ng xe:Sedan</p>\r\n\r\n<p>Số Km đ&atilde; đi:19000 Km</p>\r\n\r\n<p>M&agrave;u ngoại thất:Đen</p>\r\n\r\n<p>M&agrave;u nội thất:Kem</p>\r\n\r\n<p>Số cửa:4 cửa</p>\r\n\r\n<p>Số chỗ ngồi:5 chỗ</p>', 5, 950000000, '2', 0, '1625241247.jpg', 2, 20, 1, '2021-07-02 08:54:07', '2021-07-02 08:54:07', 0),
(43, 'New KIA Sorento Deluxe D2.2 (Máy dầu)', 'new-kia-sorento-deluxe-d2.2-(may-dau)', '<p>K&iacute;ch thước tổng thể (DxRxC): 4.810 x 1.900 x 1.695 mm</p>\r\n\r\n<p>Chiều d&agrave;i cơ sở: 2.815 mm</p>\r\n\r\n<p>Số chỗ ngồi: 6 chỗ</p>\r\n\r\n<p>Động cơ: Smartstream D2.2</p>\r\n\r\n<p>C&ocirc;ng suất cực đại: 198 Hp / 3.800 rpm</p>\r\n\r\n<p>M&ocirc; men xoắn cực đại: 440 Nm / 1.750 - 2.750 rpm</p>\r\n\r\n<p>Hộp số: 6AT</p>', 9, 1079000000, '4', 0, '1625364713.jpg', 1, 4, 1, '2021-07-03 19:11:53', '2021-07-05 07:04:41', 1),
(44, 'New KIA Sorento Signature D2.2 Diesel (7 Ghế Máy dầu)', 'new-kia-sorento-signature-d2.2-diesel-(7-ghe-may-dau)', '<p>K&iacute;ch thước tổng thể (DxRxC): 4.810 x 1.900 x 1.695 mm</p>\r\n\r\n<p>Chiều d&agrave;i cơ sở: 2.815 mm</p>\r\n\r\n<p>Số chỗ ngồi: 7 chỗ</p>\r\n\r\n<p>Động cơ: Smartstream D2.2</p>\r\n\r\n<p>C&ocirc;ng suất cực đại: 198 Hp / 3.800 rpm</p>\r\n\r\n<p>M&ocirc; men xoắn cực đại: 440 Nm / 1.750 - 2.750 rpm</p>\r\n\r\n<p>Hộp số: 6AT</p>\r\n\r\n<p>M&agrave;u : đen&nbsp;</p>', 7, 1350000000, '4', 0, '1625364865.jpg', 1, 4, 1, '2021-07-03 19:14:25', '2021-07-05 06:56:04', 1),
(45, 'KIA Cerato 1.6 AT Deluxe', 'kia-cerato-1.6-at-deluxe', '<p>T&igrave;nh trạng:Xe đ&atilde; d&ugrave;ng</p>\r\n\r\n<p>D&ograve;ng xe:Sedan</p>\r\n\r\n<p>Số Km đ&atilde; đi:19000 Km</p>\r\n\r\n<p>M&agrave;u ngoại thất:Đen</p>\r\n\r\n<p>M&agrave;u nội thất:Kem</p>\r\n\r\n<p>Số cửa:4 cửa</p>\r\n\r\n<p>Số chỗ ngồi:5 chỗ</p>', 2, 529000000, '4', 0, '1625365245.jpg', 2, 23, 1, '2021-07-03 19:20:45', '2021-07-05 20:19:43', 1),
(46, 'SANTAFE 2.2 DẦU PREMIUM 2021', 'santafe-2.2-dau-premium-2021', '<p>K&iacute;ch thước tổng thể (DxRxC): 4.810 x 1.900 x 1.695 mm</p>\r\n\r\n<p>Chiều d&agrave;i cơ sở: 2.815 mm</p>\r\n\r\n<p>Số chỗ ngồi: 7 chỗ</p>\r\n\r\n<p>Động cơ: Smartstream D2.2</p>\r\n\r\n<p>C&ocirc;ng suất cực đại: 198 Hp / 3.800 rpm</p>\r\n\r\n<p>M&ocirc; men xoắn cực đại: 440 Nm / 1.750 - 2.750 rpm</p>\r\n\r\n<p>Hộp số: 6AT</p>\r\n\r\n<p>M&agrave;u : trắng</p>', 12, 1440000000, '4', 1410000000, '1625365529.jpg', 1, 5, 1, '2021-07-03 19:25:29', '2021-07-03 19:37:20', 1),
(47, 'toyota Vios 2020 số sàn', 'toyota-vios-2020-so-san', '<p>Xe TOYOTA VIOS&nbsp;<br />\r\nSx năm 2020<br />\r\nĐăng k&yacute; năm 2020<br />\r\nT&igrave;nh trạng: Xe biển tỉnh đ&atilde; chạy 1 vạn 9 km</p>\r\n\r\n<p>Hộp số:Số s&agrave;n&nbsp;</p>\r\n\r\n<p>Ti&ecirc;u thụ nhi&ecirc;n liệu:L/100Km</p>\r\n\r\n<p>uất xứ:Nhập khẩu</p>\r\n\r\n<p>T&igrave;nh trạng:Xe đ&atilde; d&ugrave;ng</p>\r\n\r\n<p>D&ograve;ng xe:Sedan</p>\r\n\r\n<p>Số Km đ&atilde; đi:19000 Km</p>\r\n\r\n<p>M&agrave;u ngoại thất: V&agrave;ng c&aacute;t</p>\r\n\r\n<p>M&agrave;u nội thất: Đen&nbsp;</p>\r\n\r\n<p>Số cửa:4 cửa</p>\r\n\r\n<p>Số chỗ ngồi:5 chỗ</p>', 9, 480000000, '1', 0, '1625542637.jpg', 2, 25, 1, '2021-07-05 20:37:17', '2021-07-05 21:21:43', 1),
(48, 'Mercedes C300 AMG', 'mercedes-c300-amg', '<ul>\r\n	<li>Năm sản xuất 2019</li>\r\n	<li>T&igrave;nh trạng&nbsp;: Xe Đ&atilde; qua sử dụng</li>\r\n	<li>Xuất xứ Lắp r&aacute;p</li>\r\n	<li>Số km đ&atilde; đi 19.000 km</li>\r\n	<li>Nhi&ecirc;n liệu Xăng</li>\r\n	<li>Kiểu d&aacute;ng Sedan</li>\r\n	<li>Hộp số Số tự động</li>\r\n	<li>Tỉnh th&agrave;nh b&aacute;n xe H&agrave; Nội</li>\r\n	<li>số chỗ ngồi: 5</li>\r\n	<li>Nội thất: Kem&nbsp;</li>\r\n	<li>ngoai thất: đen&nbsp;</li>\r\n</ul>', 4, 1800000000, '2', 1679000000, '1625542862.jpeg', 2, 24, 1, '2021-07-05 20:41:02', '2021-07-05 21:21:43', 1),
(49, 'Huyndai Santa Fe 2019', 'huyndai-santa-fe-2019', '<p>Sx năm 2019<br />\r\nĐăng k&yacute; năm 2019<br />\r\nT&igrave;nh trạng: Xe biển tỉnh đ&atilde; chạy 1 vạn 9 km</p>\r\n\r\n<p>Hộp số:Số tự động</p>\r\n\r\n<p>Dẫn động:FWD - Dẫn động cầu trước</p>\r\n\r\n<p>Ti&ecirc;u thụ nhi&ecirc;n liệu:L/100Km</p>\r\n\r\n<p>uất xứ:Nhập khẩu</p>\r\n\r\n<p>T&igrave;nh trạng:Xe đ&atilde; d&ugrave;ng</p>\r\n\r\n<p>D&ograve;ng xe: SUV</p>\r\n\r\n<p>Số Km đ&atilde; đi:19000 Km</p>\r\n\r\n<p>M&agrave;u ngoại thất:Đỏ đ&ocirc;</p>\r\n\r\n<p>M&agrave;u nội thất: Đen</p>\r\n\r\n<p>Số cửa:4 cửa</p>\r\n\r\n<p>Số chỗ ngồi:7&nbsp;chỗ</p>', 6, 1090000000, '2', 0, '1625543016.jpg', 2, 5, 1, '2021-07-05 20:43:36', '2021-07-06 03:37:43', 0),
(50, 'Hyundai Grand i10 1.2 AT 2021', 'hyundai-grand-i10-1.2-at-2021', '<p>K&iacute;ch thước tổng thể (DxRxC): 4.810 x 1.900 x 1.695 mm</p>\r\n\r\n<p>Chiều d&agrave;i cơ sở: 2.815 mm</p>\r\n\r\n<p>Số chỗ ngồi: 4&nbsp;chỗ</p>\r\n\r\n<p>Động cơ: Smartstream D2.2</p>\r\n\r\n<p>C&ocirc;ng suất cực đại: 198 Hp / 3.800 rpm</p>\r\n\r\n<p>M&ocirc; men xoắn cực đại: 440 Nm / 1.750 - 2.750 rpm</p>\r\n\r\n<p>Hộp số: 6AT</p>\r\n\r\n<p>Ngọa thất: Trắng&nbsp;</p>\r\n\r\n<p>Nội thất: Đen</p>', 10, 324000000, '3', 0, '1625543274.jpg', 1, 42, 1, '2021-07-05 20:47:54', '2021-07-05 21:00:16', 0),
(51, 'Hyundai Grand i10 MT', 'hyundai-grand-i10-mt', '<p>K&iacute;ch thước tổng thể (DxRxC): 4.810 x 1.900 x 1.695 mm</p>\r\n\r\n<p>Chiều d&agrave;i cơ sở: 2.815 mm</p>\r\n\r\n<p>Số chỗ ngồi: 4&nbsp;chỗ</p>\r\n\r\n<p>Động cơ: Smartstream D2.2</p>\r\n\r\n<p>C&ocirc;ng suất cực đại: 198 Hp / 3.800 rpm</p>\r\n\r\n<p>M&ocirc; men xoắn cực đại: 440 Nm / 1.750 - 2.750 rpm</p>\r\n\r\n<p>Hộp số: 6AT</p>\r\n\r\n<p>Ngọai thất: Bạc&nbsp;&nbsp;</p>\r\n\r\n<p>Nội thất: Đen</p>\r\n\r\n<p>Xe đ&atilde; qua sử dụng: 2 vạn km</p>', 4, 196000000, '1', 0, '1625543444.jpg', 2, 5, 1, '2021-07-05 20:50:44', '2021-07-05 20:50:44', 0),
(52, 'Hyundai Accent 2021', 'hyundai-accent-2021', '<p>K&iacute;ch thước tổng thể (DxRxC): 4.810 x 1.900 x 1.695 mm</p>\r\n\r\n<p>Chiều d&agrave;i cơ sở: 2.815 mm</p>\r\n\r\n<p>Số chỗ ngồi: 4&nbsp;chỗ</p>\r\n\r\n<p>Động cơ: Smartstream D2.2</p>\r\n\r\n<p>C&ocirc;ng suất cực đại: 198 Hp / 3.800 rpm</p>\r\n\r\n<p>M&ocirc; men xoắn cực đại: 440 Nm / 1.750 - 2.750 rpm</p>\r\n\r\n<p>Hộp số: 6AT</p>\r\n\r\n<p>Ngọa thất: Trắng&nbsp;</p>\r\n\r\n<p>Nội thất: Đen</p>', 10, 429000000, '3', 419000000, '1625543542.jpeg', 1, 42, 1, '2021-07-05 20:52:22', '2021-07-05 20:52:22', 0),
(53, 'Vinfast Fadil 2021', 'vinfast-fadil-2021', '<p>K&iacute;ch thước tổng thể (DxRxC): 4.810 x 1.900 x 1.695 mm</p>\r\n\r\n<p>Chiều d&agrave;i cơ sở: 2.815 mm</p>\r\n\r\n<p>Số chỗ ngồi: 4&nbsp;chỗ</p>\r\n\r\n<p>Động cơ: Smartstream D2.2</p>\r\n\r\n<p>C&ocirc;ng suất cực đại: 198 Hp / 3.800 rpm</p>\r\n\r\n<p>M&ocirc; men xoắn cực đại: 440 Nm / 1.750 - 2.750 rpm</p>\r\n\r\n<p>Hộp số: 6AT</p>\r\n\r\n<p>Ngọa thất: Xanh đen</p>\r\n\r\n<p>Nội thất: Đen</p>', 0, 336000000, '3', 0, '1625543676.jpg', 1, 2, 1, '2021-07-05 20:54:36', '2021-07-06 05:02:51', 1),
(54, 'Vinfast Lux A2-0', 'vinfast-lux-a2-0', '<p>K&iacute;ch thước tổng thể (DxRxC): 4.810 x 1.900 x 1.695 mm</p>\r\n\r\n<p>Chiều d&agrave;i cơ sở: 2.815 mm</p>\r\n\r\n<p>Số chỗ ngồi: 4&nbsp;chỗ</p>\r\n\r\n<p>Động cơ: Smartstream D2.2</p>\r\n\r\n<p>C&ocirc;ng suất cực đại: 198 Hp / 3.800 rpm</p>\r\n\r\n<p>M&ocirc; men xoắn cực đại: 440 Nm / 1.750 - 2.750 rpm</p>\r\n\r\n<p>Hộp số: 6AT</p>\r\n\r\n<p>Ngọa thất: Đỏ</p>\r\n\r\n<p>Nội thất: Đen</p>\r\n\r\n<p>Đ&atilde; đi: 18000 Km</p>', 5, 865000000, '3', 0, '1625543784.jpg', 2, 27, 1, '2021-07-05 20:56:24', '2021-07-05 20:56:24', 0),
(55, 'Toyota Corolla Cross 2021', 'toyota-corolla-cross-2021', '<p>K&iacute;ch thước tổng thể (DxRxC): 4.810 x 1.900 x 1.695 mm</p>\r\n\r\n<p>Chiều d&agrave;i cơ sở: 2.815 mm</p>\r\n\r\n<p>Số chỗ ngồi: 4&nbsp;chỗ</p>\r\n\r\n<p>Động cơ: Smartstream D2.2</p>\r\n\r\n<p>C&ocirc;ng suất cực đại: 198 Hp / 3.800 rpm</p>\r\n\r\n<p>M&ocirc; men xoắn cực đại: 440 Nm / 1.750 - 2.750 rpm</p>\r\n\r\n<p>Hộp số: 6AT</p>\r\n\r\n<p>Ngọa thất: Xanh đen&nbsp;</p>\r\n\r\n<p>Nội thất: Đen</p>', 1, 1089000000, '4', 0, '1625544168.jpg', 1, 1, 1, '2021-07-05 21:02:48', '2021-07-05 21:02:48', 0),
(56, 'Toyota Land Cruiser VX V8', 'toyota-land-cruiser-vx-v8', '<p>Sx năm 2019<br />\r\nĐăng k&yacute; năm 2019<br />\r\nT&igrave;nh trạng: Xe biển tỉnh đ&atilde; chạy 1 vạn 9 km</p>\r\n\r\n<p>Hộp số:Số tự động</p>\r\n\r\n<p>Dẫn động:FWD - Dẫn động cầu trước</p>\r\n\r\n<p>Ti&ecirc;u thụ nhi&ecirc;n liệu:L/100Km</p>\r\n\r\n<p>uất xứ:Nhập khẩu</p>\r\n\r\n<p>T&igrave;nh trạng:Xe đ&atilde; d&ugrave;ng</p>\r\n\r\n<p>D&ograve;ng xe: SUV</p>\r\n\r\n<p>Số Km đ&atilde; đi:19000 Km</p>\r\n\r\n<p>M&agrave;u ngoại thất:Đỏ đ&ocirc;</p>\r\n\r\n<p>M&agrave;u nội thất: Đen</p>\r\n\r\n<p>Số cửa:4 cửa</p>\r\n\r\n<p>Số chỗ ngồi:7&nbsp;chỗ</p>', 5, 3500000000, '2', 0, '1625544313.jpg', 2, 20, 1, '2021-07-05 21:05:13', '2021-07-05 21:05:13', 0),
(57, 'Toyota Innova', 'toyota-innova', '<p>T&igrave;nh trạng:Xe đ&atilde; d&ugrave;ng</p>\r\n\r\n<p>D&ograve;ng xe: SUV</p>\r\n\r\n<p>Số Km đ&atilde; đi:19000 Km</p>\r\n\r\n<p>M&agrave;u ngoại thất:X&aacute;m&nbsp;</p>\r\n\r\n<p>M&agrave;u nội thất: Đen</p>\r\n\r\n<p>Số cửa:4 cửa</p>\r\n\r\n<p>Số chỗ ngồi:7&nbsp;chỗ</p>', 6, 635000000, '1', 0, '1625544517.jpg', 2, 25, 1, '2021-07-05 21:08:37', '2021-07-05 21:08:37', 0),
(58, 'Toyota Fortuner  2018', 'toyota-fortuner-2018', '<p>T&igrave;nh trạng:Xe đ&atilde; d&ugrave;ng</p>\r\n\r\n<p>D&ograve;ng xe: SUV</p>\r\n\r\n<p>Số Km đ&atilde; đi:19000 Km</p>\r\n\r\n<p>M&agrave;u ngoại thất:Trắng</p>\r\n\r\n<p>M&agrave;u nội thất: Đen</p>\r\n\r\n<p>Số cửa:4 cửa</p>\r\n\r\n<p>Số chỗ ngồi:7&nbsp;chỗ</p>', 7, 950000000, '1', 0, '1625544610.jpg', 2, 25, 1, '2021-07-05 21:10:10', '2021-07-05 21:10:10', 0),
(59, 'Mercedes Benz C200 Exclusive', 'mercedes-benz-c200-exclusive', '<p>Động cơ: Smartstream D2.2</p>\r\n\r\n<p>C&ocirc;ng suất cực đại: 198 Hp / 3.800 rpm</p>\r\n\r\n<p>M&ocirc; men xoắn cực đại: 440 Nm / 1.750 - 2.750 rpm</p>\r\n\r\n<p>Hộp số: 6AT</p>\r\n\r\n<p>Ngọa thất: đen</p>\r\n\r\n<p>Nội thất: Đen</p>\r\n\r\n<p>Xe đ&atilde; đi 10000 Km&nbsp;</p>', 1, 1579000000, '1', 0, '1625544723.jpg', 2, 24, 1, '2021-07-05 21:12:03', '2021-07-05 21:12:03', 0),
(60, 'mercedes S450 2018', 'mercedes-s450-2018', '<p>T&igrave;nh trạng:Xe đ&atilde; d&ugrave;ng</p>\r\n\r\n<p>D&ograve;ng xe: Sedan</p>\r\n\r\n<p>Số Km đ&atilde; đi:19000 Km</p>\r\n\r\n<p>M&agrave;u ngoại thất:Trắng&nbsp;</p>\r\n\r\n<p>M&agrave;u nội thất: Đen</p>\r\n\r\n<p>Số cửa:4 cửa</p>\r\n\r\n<p>Số chỗ ngồi:4 chỗ</p>', 4, 3360000000, '2', 0, '1625544857.jpeg', 2, 24, 1, '2021-07-05 21:14:17', '2021-07-05 21:14:17', 0),
(61, 'Mercedes G63 AMG Edition 1', 'mercedes-g63-amg-edition-1', '<p>Sx năm 2019<br />\r\nĐăng k&yacute; năm 2019<br />\r\nT&igrave;nh trạng: Xe biển tỉnh đ&atilde; chạy 1 vạn 9 km</p>\r\n\r\n<p>Hộp số:Số tự động</p>\r\n\r\n<p>Dẫn động:FWD - Dẫn động cầu trước</p>\r\n\r\n<p>Ti&ecirc;u thụ nhi&ecirc;n liệu:L/100Km</p>\r\n\r\n<p>uất xứ:Nhập khẩu</p>\r\n\r\n<p>T&igrave;nh trạng:Xe đ&atilde; d&ugrave;ng</p>\r\n\r\n<p>D&ograve;ng xe:SUV</p>\r\n\r\n<p>Số Km đ&atilde; đi:3500 Km</p>\r\n\r\n<p>M&agrave;u ngoại thất:Đen</p>\r\n\r\n<p>M&agrave;u nội thất:Kem</p>\r\n\r\n<p>Số cửa:4 cửa</p>\r\n\r\n<p>Số chỗ ngồi:5 chỗ</p>', 3, 11650000000, '3', 0, '1625566059.jpeg', 2, 24, 1, '2021-07-06 03:07:39', '2021-07-06 03:07:39', 0),
(62, 'Mercedes C180 AMG 2021', 'mercedes-c180-amg-2021', '<p>K&iacute;ch thước tổng thể (DxRxC): 4.810 x 1.900 x 1.695 mm</p>\r\n\r\n<p>Chiều d&agrave;i cơ sở: 2.815 mm</p>\r\n\r\n<p>Số chỗ ngồi: 5&nbsp;chỗ</p>\r\n\r\n<p>Động cơ: Smartstream D2.2</p>\r\n\r\n<p>C&ocirc;ng suất cực đại: 198 Hp / 3.800 rpm</p>\r\n\r\n<p>M&ocirc; men xoắn cực đại: 440 Nm / 1.750 - 2.750 rpm</p>\r\n\r\n<p>Hộp số: 6AT</p>', 1, 1499000000, '2', 0, '1625566183.jpg', 1, 3, 1, '2021-07-06 03:09:43', '2021-07-06 03:09:43', 0),
(63, 'Xpander Cross', 'xpander-cross', '<p>K&iacute;ch thước tổng thể (DxRxC): 4.810 x 1.900 x 1.695 mm</p>\r\n\r\n<p>Chiều d&agrave;i cơ sở: 2.815 mm</p>\r\n\r\n<p>Số chỗ ngồi: 5&nbsp;chỗ</p>\r\n\r\n<p>Động cơ: Smartstream D2.2</p>\r\n\r\n<p>C&ocirc;ng suất cực đại: 198 Hp / 3.800 rpm</p>\r\n\r\n<p>M&ocirc; men xoắn cực đại: 440 Nm / 1.750 - 2.750 rpm</p>', 10, 679000000, '3', 669000000, '1625566342.jpg', 1, 43, 1, '2021-07-06 03:12:22', '2021-07-06 03:12:22', 0),
(64, 'Mitsubishi Xpander', 'mitsubishi-xpander', '<p>K&iacute;ch thước tổng thể (DxRxC): 4.810 x 1.900 x 1.695 mm</p>\r\n\r\n<p>Chiều d&agrave;i cơ sở: 2.815 mm</p>\r\n\r\n<p>Số chỗ ngồi: 7 chỗ</p>\r\n\r\n<p>Động cơ: Smartstream D2.2</p>\r\n\r\n<p>C&ocirc;ng suất cực đại: 198 Hp / 3.800 rpm</p>\r\n\r\n<p>M&ocirc; men xoắn cực đại: 440 Nm / 1.750 - 2.750 rpm</p>', 5, 450000000, '1', 0, '1625566408.jpg', 2, 44, 1, '2021-07-06 03:13:28', '2021-07-06 03:13:28', 0),
(65, 'BMW 320i', 'bmw-320i', '<p>K&iacute;ch thước tổng thể (DxRxC): 4.810 x 1.900 x 1.695 mm</p>\r\n\r\n<p>Chiều d&agrave;i cơ sở: 2.815 mm</p>\r\n\r\n<p>Số chỗ ngồi: 5&nbsp;chỗ</p>\r\n\r\n<p>Động cơ: Smartstream D2.2</p>\r\n\r\n<p>C&ocirc;ng suất cực đại: 198 Hp / 3.800 rpm</p>\r\n\r\n<p>M&ocirc; men xoắn cực đại: 440 Nm / 1.750 - 2.750 rpm</p>\r\n\r\n<p>Hộp số: 6AT</p>', 8, 1769000000, '3', 0, '1625566741.jpg', 1, 9, 1, '2021-07-06 03:19:01', '2021-07-06 03:19:01', 0),
(66, 'Ford Ranger', 'ford-ranger', '<p>K&iacute;ch thước tổng thể (DxRxC): 4.810 x 1.900 x 1.695 mm</p>\r\n\r\n<p>Chiều d&agrave;i cơ sở: 2.815 mm</p>\r\n\r\n<p>Số chỗ ngồi: 5&nbsp;chỗ</p>\r\n\r\n<p>Động cơ: Smartstream D2.2</p>\r\n\r\n<p>C&ocirc;ng suất cực đại: 198 Hp / 3.800 rpm</p>\r\n\r\n<p>M&ocirc; men xoắn cực đại: 440 Nm / 1.750 - 2.750 rpm</p>\r\n\r\n<p>M&agrave;u nội thất: Đen</p>\r\n\r\n<p>M&agrave;u ngoại thất: Cam&nbsp;</p>', 6, 616000000, '3', 0, '1625566974.jpg', 1, 45, 1, '2021-07-06 03:22:54', '2021-07-06 03:22:54', 0),
(67, 'Ford Everest 2021', 'ford-everest-2021', '<p>K&iacute;ch thước tổng thể (DxRxC): 4.810 x 1.900 x 1.695 mm</p>\r\n\r\n<p>Chiều d&agrave;i cơ sở: 2.815 mm</p>\r\n\r\n<p>Số chỗ ngồi: 7 chỗ</p>\r\n\r\n<p>Động cơ: Smartstream D2.2</p>\r\n\r\n<p>C&ocirc;ng suất cực đại: 198 Hp / 3.800 rpm</p>\r\n\r\n<p>M&ocirc; men xoắn cực đại: 440 Nm / 1.750 - 2.750 rpm</p>\r\n\r\n<p>Hộp số: 6AT</p>\r\n\r\n<p>M&agrave;u : đen&nbsp;</p>', 13, 1190000000, '3', 0, '1625567073.jpg', 1, 45, 1, '2021-07-06 03:24:33', '2021-07-06 03:24:33', 0),
(68, 'Lexus LX 570 2021 Super Sport MBS', 'lexus-lx-570-2021-super-sport-mbs', '<p>K&iacute;ch thước tổng thể (DxRxC): 4.810 x 1.900 x 1.695 mm</p>\r\n\r\n<p>Chiều d&agrave;i cơ sở: 2.815 mm</p>\r\n\r\n<p>Số chỗ ngồi: 4&nbsp;chỗ</p>\r\n\r\n<p>Động cơ: Smartstream D2.2</p>\r\n\r\n<p>C&ocirc;ng suất cực đại: 198 Hp / 3.800 rpm</p>\r\n\r\n<p>M&ocirc; men xoắn cực đại: 440 Nm / 1.750 - 2.750 rpm</p>\r\n\r\n<p>Hộp số: 6AT</p>\r\n\r\n<p>M&agrave;u : đen&nbsp;</p>', 4, 10250000000, '4', 0, '1625567224.jpg', 1, 47, 1, '2021-07-06 03:27:04', '2021-07-06 05:02:51', 1),
(69, 'Lexus LX 570 2020', 'lexus-lx-570-2020', '<p>K&iacute;ch thước tổng thể (DxRxC): 4.810 x 1.900 x 1.695 mm</p>\r\n\r\n<p>Chiều d&agrave;i cơ sở: 2.815 mm</p>\r\n\r\n<p>Số chỗ ngồi: 4&nbsp;chỗ</p>\r\n\r\n<p>Động cơ: Smartstream D2.2</p>\r\n\r\n<p>C&ocirc;ng suất cực đại: 198 Hp / 3.800 rpm</p>\r\n\r\n<p>M&ocirc; men xoắn cực đại: 440 Nm / 1.750 - 2.750 rpm</p>\r\n\r\n<p>Hộp số: 6AT</p>\r\n\r\n<p>M&agrave;u : đen&nbsp;</p>\r\n\r\n<p>Nội thất: N&acirc;u da b&ograve;</p>', 5, 9579000000, '4', 0, '1625567333.jpg', 1, 47, 1, '2021-07-06 03:28:53', '2021-07-06 03:28:53', 0),
(70, 'Lexus RX 350 F-Sport', 'lexus-rx-350-f-sport', '<p>K&iacute;ch thước tổng thể (DxRxC): 4.810 x 1.900 x 1.695 mm</p>\r\n\r\n<p>Chiều d&agrave;i cơ sở: 2.815 mm</p>\r\n\r\n<p>Số chỗ ngồi: 4&nbsp;chỗ</p>\r\n\r\n<p>Động cơ: Smartstream D2.2</p>\r\n\r\n<p>C&ocirc;ng suất cực đại: 198 Hp / 3.800 rpm</p>\r\n\r\n<p>M&ocirc; men xoắn cực đại: 440 Nm / 1.750 - 2.750 rpm</p>\r\n\r\n<p>Hộp số: 6AT</p>\r\n\r\n<p>M&agrave;u : trắng&nbsp;</p>\r\n\r\n<p>Nội thất : đỏ - đen&nbsp;</p>', 9, 4480000000, '2', 0, '1625567433.jpeg', 1, 47, 1, '2021-07-06 03:30:33', '2021-07-06 03:30:33', 0),
(71, 'Lexus LS 460L 2013', 'lexus-ls-460l-2013', '<p>K&iacute;ch thước tổng thể (DxRxC): 4.810 x 1.900 x 1.695 mm</p>\r\n\r\n<p>Chiều d&agrave;i cơ sở: 2.815 mm</p>\r\n\r\n<p>Số chỗ ngồi: 4&nbsp;chỗ</p>\r\n\r\n<p>Động cơ: Smartstream D2.2</p>\r\n\r\n<p>C&ocirc;ng suất cực đại: 198 Hp / 3.800 rpm</p>\r\n\r\n<p>M&ocirc; men xoắn cực đại: 440 Nm / 1.750 - 2.750 rpm</p>\r\n\r\n<p>Hộp số: 6AT</p>\r\n\r\n<p>M&agrave;u : trắng&nbsp;</p>\r\n\r\n<p>Nội thất : Kem&nbsp;</p>', 3, 446000000, '2', 0, '1625567550.jpg', 2, 48, 1, '2021-07-06 03:32:30', '2021-07-06 03:32:30', 0),
(72, 'Mazda 3 AT Luxury năm 2020', 'mazda-3-at-luxury-nam-2020', '<p>K&iacute;ch thước tổng thể (DxRxC): 4.810 x 1.900 x 1.695 mm</p>\r\n\r\n<p>Chiều d&agrave;i cơ sở: 2.815 mm</p>\r\n\r\n<p>Số chỗ ngồi: 4&nbsp;chỗ</p>\r\n\r\n<p>Động cơ: Smartstream D2.2</p>\r\n\r\n<p>C&ocirc;ng suất cực đại: 198 Hp / 3.800 rpm</p>\r\n\r\n<p>M&ocirc; men xoắn cực đại: 440 Nm / 1.750 - 2.750 rpm</p>\r\n\r\n<p>Hộp số: 6AT</p>\r\n\r\n<p>M&agrave;u : trắng&nbsp;</p>\r\n\r\n<p>Nội thất : Kem&nbsp;</p>\r\n\r\n<p>Đ&atilde; đi 25000Km&nbsp;</p>', 6, 695000000, '2', 0, '1625567707.jpg', 2, 26, 1, '2021-07-06 03:35:07', '2021-07-06 03:35:07', 0),
(73, 'Màn hình hiển thị thông minh HUD Vietmap H1N', 'man-hinh-hien-thi-thong-minh-hud-vietmap-h1n', '<ul>\r\n	<li>Xuất xứ: Ch&iacute;nh h&atilde;ng Vietmap<br />\r\n	Nguồn: 12V &ndash; 3A</li>\r\n	<li>Nhiệt độ hoạt động: 20&deg;C &ndash; 80&deg;C</li>\r\n	<li>Cảnh b&aacute;o giao th&ocirc;ng: C&oacute; (Tốc độ giới hạn + biển b&aacute;o giao th&ocirc;ng + camera phạt nguội + trạm thu ph&iacute; k&egrave;m gi&aacute; v&eacute;)</li>\r\n	<li>Kết nối điện thoại: C&oacute; (Ứng dụng VIETMAP HUD)</li>\r\n	<li>Cảm biến TPMS: Kh&ocirc;ng</li>\r\n	<li>Nhiệt độ k&eacute;t nước: C&oacute;</li>\r\n	<li>Điện &aacute;p: C&oacute;</li>\r\n	<li>Hiển thị dẫn đường: C&oacute;</li>\r\n	<li>V&ograve;ng tua m&aacute;y: C&oacute;</li>\r\n	<li>Th&ocirc;ng tin cuộc gọi: C&oacute;</li>\r\n</ul>', 100, 5850000, '2', 0, '1625568101.png', 4, 32, 1, '2021-07-06 03:41:41', '2021-07-06 03:41:41', 0),
(74, 'móc chìa khóa ô tô JOBON', 'moc-chia-khoa-o-to-jobon', '<p><em>Đẹp v&agrave; sang trọng ch&iacute;nh c&aacute;i nh&igrave;n đầu ti&ecirc;n cho d&ograve;ng sản phẩm cao cấp n&agrave;y. Với những m&oacute;c ch&igrave;a kh&oacute;a Jobon đều l&agrave; hợp kim chống gỉ, da thật, nhiều m&oacute;c c&ograve;n c&oacute; c&aacute;c chức năng sử dụng như một đ&egrave;n pin, cắt m&oacute;ng tay hay bật lửa kh&aacute; th&uacute; vị. Dưới đ&acirc;y l&agrave; một số mẫu m&oacute;c kh&oacute;a d&agrave;nh cho &ocirc; t&ocirc; Qu&yacute; kh&aacute;ch tham khảo.</em></p>', 300, 650000, '0', 0, '1625568194.png', 4, 33, 1, '2021-07-06 03:43:14', '2021-07-06 03:43:14', 0),
(75, 'Móc treo chìa khóa bằng da', 'moc-treo-chia-khoa-bang-da', '<p>Khi mua c&aacute;c sản phẩm rẻ nhiều người nghĩ rằng thế l&agrave; được rồi. Nhưng khi d&ugrave;ng c&aacute;c sản phẩm cao cấp m&agrave; gi&aacute; th&agrave;nh cũng chỉ nhỉn hơn ch&uacute;t mới cảm nhận sự sang trọng của n&oacute;. UBNICE-Nappa cung cấp sản phẩm m&oacute;c treo ch&igrave;a kh&oacute;a d&agrave;nh cho c&aacute;c h&atilde;ng &ocirc; t&ocirc; cực cao cấp nhưng gi&aacute; rất hợp l&yacute;. Qu&yacute; kh&aacute;ch c&ugrave;ng tham khảo c&aacute;c mẫu ch&igrave;a kh&oacute;a của UBNICE-Nappa nh&eacute;.</p>', 247, 300000, '0', 0, '1625568270.jpg', 4, 33, 1, '2021-07-06 03:44:30', '2021-07-06 05:02:51', 3),
(76, 'Thảm Taplo da Cacbon', 'tham-taplo-da-cacbon', '<p>Ngăn &aacute;nh nắng trực tiếp từ b&ecirc;n ngo&agrave;i chiếu l&ecirc;n bề mặt taplo xe h&acirc;y hư hỏng, nứt nẻ, phồng rộp bề mặt taplo. Ngăn ph&aacute;t t&aacute;n c&aacute;c chất g&acirc;y hại trong bộ phận nhựa của xe hơi.<br />\r\nLoại bỏ phản quang, b&oacute;ng ảnh g&acirc;y hạn chế tầm nh&igrave;n cho người l&aacute;i xe khi đang di chuyển tr&ecirc;n đường. Gi&uacute;p l&aacute;i xe c&oacute; thể di chuyển an to&agrave;n tr&ecirc;n c&aacute;c cung đường v&agrave;o những ng&agrave;y nắng n&oacute;ng.</p>', 498, 749000000, '1', 0, '1625568362.jpg', 4, 30, 1, '2021-07-06 03:46:02', '2021-07-06 05:02:51', 2),
(77, 'Máy khử mùi ô tô - Máy khử mùi xe Webvision A8', 'may-khu-mui-o-to-may-khu-mui-xe-webvision-a8', '<p>Khử m&ugrave;i h&ocirc;i tr&ecirc;n xe cực hiệu quả</p>\r\n\r\n<p>Khử bụi mịn tr&ecirc;n xe( Mỗi lần mở cửa bụi mịn sẽ bay trực tiếp v&agrave;o mũi, nhiều người bị xoang thường cảm nhận được ngay)</p>\r\n\r\n<p>Kh&ocirc;ng gian nhỏ thường cảm thấy kh&oacute; chịu hơn b&igrave;nh thường, m&aacute;y cũng l&agrave;m m&aacute;t v&agrave; điều h&ograve;a kh&ocirc;ng kh&iacute;</p>', 129, 2450000, '2', 0, '1625568459.jpg', 4, 37, 1, '2021-07-06 03:47:39', '2021-07-06 05:02:51', 1),
(78, 'Máy khử mùi xe trên ô tô - Phát tán không khí', 'may-khu-mui-xe-tren-o-to-phat-tan-khong-khi', '<ul>\r\n	<li>Điện &aacute;p: 12v ( Ph&ugrave; hợp mọi d&ograve;ng xe con &ocirc; t&ocirc;).</li>\r\n	<li>Sử dụng : Cắm trực tiếp v&agrave;o tổng</li>\r\n	<li>Kết cấu: C&oacute; phần k&iacute;ch tỏa kh&ocirc;ng kh&iacute; với m&ugrave;i hương v&agrave; phần lọc kh&ocirc;ng kh&iacute; với bộ lọc h&uacute;t thở</li>\r\n</ul>', 1, 2040000, '2', 1785000, '1625568546.jpg', 4, 37, 1, '2021-07-06 03:49:06', '2021-07-06 03:49:06', 0),
(79, 'Bậc bước chân ô tô cao cấp', 'bac-buoc-chan-o-to-cao-cap', '<ul>\r\n	<li>Hỗ trợ người bước l&ecirc;n xuống đễ d&agrave;ng ( Nhất l&agrave; người gi&agrave; v&agrave; trẻ nhỏ)</li>\r\n	<li>Tăng vẻ cứng c&aacute;p cho c&aacute;c d&ograve;ng xe &ocirc; t&ocirc;</li>\r\n	<li>Tạo vẻ đẹp v&agrave; sự hầm hố cho c&aacute;c d&ograve;ng xe</li>\r\n	<li>Tr&aacute;nh được t&igrave;nh trạng m&oacute;p sườn xe</li>\r\n</ul>', 299, 2800000, '2', 0, '1625568658.jpg', 4, 36, 1, '2021-07-06 03:50:58', '2021-07-06 05:02:51', 1),
(80, 'Bậc lên xuống MAZDA CX5 Hàng chính hãng', 'bac-len-xuong-mazda-cx5-hang-chinh-hang', '<ul>\r\n	<li>Hỗ trợ người bước l&ecirc;n xuống đễ d&agrave;ng ( Nhất l&agrave; người gi&agrave; v&agrave; trẻ nhỏ)</li>\r\n	<li>Tăng vẻ cứng c&aacute;p cho c&aacute;c d&ograve;ng xe &ocirc; t&ocirc;</li>\r\n	<li>Tạo vẻ đẹp v&agrave; sự hầm hố cho c&aacute;c d&ograve;ng xe</li>\r\n	<li>Tr&aacute;nh được t&igrave;nh trạng m&oacute;p sườn xe</li>\r\n</ul>', 50, 1889000, '1', 0, '1625568758.png', 4, 36, 1, '2021-07-06 03:52:38', '2021-07-06 03:52:38', 0),
(81, 'Gạt mưa nano chính hãng cho mọi dòng xe', 'gat-mua-nano-chinh-hang-cho-moi-dong-xe', '<p>&ndash; Th&ocirc;ng thường, c&ocirc;ng nghệ phủ k&iacute;nh NANO phổ biến hiện nay tr&ecirc;n thị trường l&agrave; ở thể lỏng. Khi bạn phủ NANO l&ecirc;n, qua qu&aacute; tr&igrave;nh sử dụng hoặc vệ sinh xe gặp phải chất tẩy rửa mạnh, qu&aacute; tr&igrave;nh n&agrave;y lặp đi lặp lại nhiều lần sẽ khiến lớp phủ NANO dễ bị mất đi. Vậy l&agrave; bạn phải bỏ tiền t&uacute;i ra để phủ lại lớp NANO mới nhằm đảm bảo chế độ l&aacute;i an to&agrave;n cho m&igrave;nh trong những ng&agrave;y mưa r&agrave;o.<br />\r\n&ndash; Nhưng với sản phẩm&nbsp;<strong>GẠT MƯA NANO</strong>&nbsp;n&agrave;y, ch&uacute;ng ta ho&agrave;n to&agrave;n c&oacute; thể y&ecirc;n t&acirc;m v&igrave; sản phẩm n&agrave;y sẽ cho ph&eacute;p bạn phủ nano l&ecirc;n k&iacute;nh nhiều lần đến khi n&agrave;o m&ograve;n&nbsp;<strong>&ldquo;L&aacute; L&uacute;a&rdquo;</strong><em><strong>(Chổi gạt mưa)</strong></em>&nbsp;th&igrave; th&ocirc;i . Khả năng cho ph&eacute;p t&aacute;i sử dụng nhiều lần l&agrave; l&yacute; do ch&iacute;nh gi&uacute;p bạn tiết kiệm chi ph&iacute;.</p>', 350, 560000, '0', 0, '1625568838.jpg', 4, 6, 1, '2021-07-06 03:53:58', '2021-07-06 03:53:58', 0),
(82, 'Gạt mưa ô tô ASD', 'gat-mua-o-to-asd', '<p>&ndash; Th&ocirc;ng thường, c&ocirc;ng nghệ phủ k&iacute;nh NANO phổ biến hiện nay tr&ecirc;n thị trường l&agrave; ở thể lỏng. Khi bạn phủ NANO l&ecirc;n, qua qu&aacute; tr&igrave;nh sử dụng hoặc vệ sinh xe gặp phải chất tẩy rửa mạnh, qu&aacute; tr&igrave;nh n&agrave;y lặp đi lặp lại nhiều lần sẽ khiến lớp phủ NANO dễ bị mất đi. Vậy l&agrave; bạn phải bỏ tiền t&uacute;i ra để phủ lại lớp NANO mới nhằm đảm bảo chế độ l&aacute;i an to&agrave;n cho m&igrave;nh trong những ng&agrave;y mưa r&agrave;o.</p>', 350, 469000, '0', 0, '1625568927.jpeg', 4, 6, 1, '2021-07-06 03:55:27', '2021-07-06 03:55:27', 0),
(83, 'Loa BLAUPUNKT 180A', 'loa-blaupunkt-180a', '<p><em><strong>* Subwoofer</strong></em></p>\r\n\r\n<p><em>&ndash; speaker size : 8&rdquo;</em></p>\r\n\r\n<p><em>&ndash; cone composition : aluminium</em></p>\r\n\r\n<p><em>&ndash; magnet type : ferrite</em></p>\r\n\r\n<p><em>&ndash; enclosure housing material : aluminium</em></p>\r\n\r\n<p><em>&ndash; enclosure dimension ( LxWxH): 345x260x75mm</em></p>\r\n\r\n<p><em>&ndash;&nbsp;enclosure net weight : 2,65kg</em></p>\r\n\r\n<p><em><strong>*&nbsp;</strong></em><strong><em>Amplifier</em></strong></p>\r\n\r\n<p><em><strong>&ndash;&nbsp;</strong>amplifier technology : class AB</em></p>\r\n\r\n<p><em>&ndash; Normal output power (RMS) : 180W</em></p>\r\n\r\n<p><em>&ndash; Max output power : 400w</em></p>\r\n\r\n<p><em>&ndash; Audio in sensitivity : RCA input 0.2V-10V , sperker input 0.6v-12v</em></p>\r\n\r\n<p><em>&ndash; Singnal/noise ratio : 91db</em></p>\r\n\r\n<p><em>&ndash; Output lmpedance : 2ohms</em></p>\r\n\r\n<p><em>&ndash; Frequency response : 30Hz-150Hz</em></p>\r\n\r\n<p><em>&ndash; Low pass filter ( LPF) : 50 Hz-150Hz</em></p>\r\n\r\n<p><em>&ndash; Voltage : 14.4v (10v-16v)</em></p>\r\n\r\n<p><em>&ndash; Phase Switch : 0 deg / 180 deg</em></p>\r\n\r\n<p><em>&ndash; Automatic on/off : high level only</em></p>', 50, 4490000, '2', 0, '1625569028.png', 4, 38, 1, '2021-07-06 03:57:08', '2021-07-06 03:57:08', 0),
(84, 'Loa cánh JBL GTO 638 3Way chính hãng', 'loa-canh-jbl-gto-638-3way-chinh-hang', '<p>Th&ocirc;ng số kĩ thuật về bộ Loa cửa sau JBL GTO 638</p>\r\n\r\n<ul>\r\n	<li>Patented plus one woofer cone</li>\r\n	<li>Home theater-quality tweeter</li>\r\n	<li>True sound from built-in crossover network</li>\r\n	<li>6-1/2 / 6-3/4-inch three-way loudspeaker</li>\r\n	<li>Plus One woofer cone with rubber surround</li>\r\n	<li>Mylar-titanium tweeter and adjustable supertweeter with level control</li>\r\n	<li>180 Watts peak power handling; 2 ohm impedance</li>\r\n	<li>Two-inch mounting depth</li>\r\n</ul>', 60, 4500000, '2', 0, '1625569100.jpg', 4, 38, 1, '2021-07-06 03:58:20', '2021-07-06 03:58:20', 0),
(85, 'đèn GTR chính hãng', 'den-gtr-chinh-hang', '<p><strong>Chi tiết kỹ thuật:</strong></p>\r\n\r\n<p>Ch&acirc;n b&oacute;ng: D2H<br />\r\nNhiệt m&agrave;u: 4500K &ndash; 5500K<br />\r\nC&ocirc;ng suất B&oacute;ng: 45W<br />\r\nC&ocirc;ng suất Ballast: 45W<br />\r\nĐiện thế: 12V</p>\r\n\r\n<p>Bộ sản phẩm bao gồm 2 Xenon v&agrave; 2 Ballast, cho hiệu quả tăng s&aacute;ng tối ưu.<br />\r\nPh&ugrave; hợp với nhu cầu n&acirc;ng cấp &aacute;nh s&aacute;ng cơ bản,<br />\r\nchất lượng sản phẩm vượt trội đi k&egrave;m với nhiều Nhiệt m&agrave;u từ 4300K &ndash; 5500K cho đến 6500K</p>', 40, 5000000, '2', 0, '1625569204.jpg', 4, 34, 1, '2021-07-06 04:00:04', '2021-07-06 04:00:04', 0),
(86, 'Bi Lazer AZ Stars Z22', 'bi-lazer-az-stars-z22', '<p>Th&ocirc;ng số kỹ thuật:</p>\r\n\r\n<p>&ndash; Phân Loại: Bi led</p>\r\n\r\n<p>&ndash; Lumen: 3800lm</p>\r\n\r\n<p>&ndash; Ánh sáng : 6000K</p>\r\n\r\n<p>&ndash; Điện áp : 12V</p>\r\n\r\n<p>&ndash; Công suất chiếu s&aacute;ng : 100w</p>\r\n\r\n<p>&ndash; Chịu được nhiệt độ : từ -40 tơ&iacute; 120 độ C</p>\r\n\r\n<p>&ndash; Tuổi thọ : trên 30.000 giờ.</p>', 60, 12800000, '2', 0, '1625569283.jpg', 4, 34, 1, '2021-07-06 04:01:23', '2021-07-06 04:01:23', 0),
(87, 'Camera hành trình LET’S VIEW SH300M', 'camera-hanh-trinh-let’s-view-sh300m', '<p>C&aacute;c t&iacute;nh năng cảnh b&aacute;o sản phẩm đều đầy đủ trong SH300M. Những đặc biệt m&agrave; chỉ d&ograve;ng Camera h&agrave;nh tr&igrave;nh Let&rsquo;s view n&agrave;y c&oacute; ch&iacute;nh l&agrave;:</p>\r\n\r\n<ul>\r\n	<li>Tự ngắt Camera nhằm đảm bảo điện &aacute;p ổn định v&agrave; tăng tuổi thọ Pin m&aacute;y</li>\r\n	<li>T&iacute;nh năng ghi h&igrave;nh ban đ&ecirc;m cao cấp</li>\r\n	<li>Hiển thị tốc độ tr&ecirc;n m&agrave;n h&igrave;nh</li>\r\n	<li>Cảnh b&aacute;o v&agrave; c&agrave;i đặt giới hạn tốc độ</li>\r\n	<li>Quản l&yacute; lộ tr&igrave;nh GPS</li>\r\n	<li>Cảnh b&aacute;o ADAS</li>\r\n	<li>Tự động ghi đ&egrave;</li>\r\n	<li>Xuất xứ H&agrave;n Quốc</li>\r\n</ul>', 100, 3850000, '2', 0, '1625569399.jpg', 4, 13, 1, '2021-07-06 04:03:19', '2021-07-06 04:03:19', 0);
INSERT INTO `product` (`id`, `name`, `slug`, `description`, `quantity`, `price`, `warranty`, `promotional`, `image`, `idCategory`, `idProductType`, `status`, `created_at`, `updated_at`, `qty_buy`) VALUES
(88, 'Camera hành trình Vietmap C61', 'camera-hanh-trinh-vietmap-c61', '<ul>\r\n	<li>\r\n	<p><strong>Ghi h&igrave;nh Ultra&nbsp;HD (4K)</strong></p>\r\n\r\n	<p>Ghi h&igrave;nh Ng&agrave;y &amp; Đ&ecirc;m si&ecirc;u n&eacute;t</p>\r\n	</li>\r\n	<li>\r\n	<p><strong>GPS t&iacute;ch hợp trong m&aacute;y</strong></p>\r\n\r\n	<p>Ghi h&igrave;nh&nbsp;th&ocirc;ng tin tốc độ, tọa độ</p>\r\n	</li>\r\n	<li>\r\n	<p><strong>G&oacute;c quay rộng 170o</strong></p>\r\n\r\n	<p>Ghi h&igrave;nh to&agrave;n cảnh trước đầu xe</p>\r\n	</li>\r\n	<li>\r\n	<p><strong>WDR ghi h&igrave;nh ngược s&aacute;ng</strong></p>\r\n\r\n	<p>Ghi h&igrave;nh tốt trong m&ocirc;i trường &aacute;nh s&aacute;ng yếu, ngược s&aacute;ng</p>\r\n	</li>\r\n	<li>\r\n	<p><strong>Cảnh b&aacute;o bằng giọng n&oacute;i</strong></p>\r\n\r\n	<p>Hỗ trợ l&aacute;i xe an to&agrave;n</p>\r\n	</li>\r\n	<li>\r\n	<p><strong>Cảnh b&aacute;o tốc độ giới hạn</strong></p>\r\n\r\n	<p>Cảnh b&aacute;o tốc độ giới hạn bằng giọng n&oacute;i</p>\r\n\r\n	<p>&nbsp;</p>\r\n	</li>\r\n	<li>\r\n	<p><strong>Kết nối WIFI</strong></p>\r\n\r\n	<p>Xem v&agrave; tải video trực tiếp<br />\r\n	tr&ecirc;n HĐH Android, iOS</p>\r\n	</li>\r\n	<li>\r\n	<p><strong>M&agrave;n h&igrave;nh 2.4&Prime;</strong></p>\r\n\r\n	<p>Xem lại trực tiếp video<br />\r\n	tr&ecirc;n m&agrave;n h&igrave;nh</p>\r\n	</li>\r\n</ul>', 150, 6500000, '2', 0, '1625569511.jpg', 4, 13, 1, '2021-07-06 04:05:11', '2021-07-06 04:05:11', 0),
(89, 'Khâu nhúng Cacbon Vô lăng SANTAFE', 'khau-nhung-cacbon-vo-lang-santafe', '<p>Bọc v&ocirc; lăng da xuất cứ Malaysia</p>\r\n\r\n<p>Chất liệu c&aacute;cbon&nbsp;cho cảm gi&aacute;c cầm nắm tốt v&agrave; thoải m&aacute;i nhất cho người l&aacute;i xe</p>', 65, 3360000, '2', 0, '1625569686.jpg', 4, 14, 1, '2021-07-06 04:08:06', '2021-07-06 04:08:06', 0),
(90, 'Cảnh báo điểm mù ô tô - Cảnh báo va chạm sớm ô tô', 'canh-bao-diem-mu-o-to-canh-bao-va-cham-som-o-to', '<p>Mắt cảm biến kh&ocirc;ng khoan, cực th&ocirc;ng minh. Với những d&ograve;ng xe c&oacute; &rdquo; N&uacute;t Cảm biến l&ugrave;i th&igrave; việc lắp Cảnh b&aacute;o va chạm kh&ocirc;ng l&agrave;m ảnh hưởng đến thẩm mỹ của xe.</p>\r\n\r\n<p>Hộp cảnh b&aacute;o điểm m&ugrave; gồm c&oacute; cục xử l&yacute; chung t&acirc;m, 2 mắt cảm biến kh&ocirc;ng khoan, 2 mắt cảnh b&aacute;o hiển thị cột chữ A tr&aacute;i phải. Hệ thống cảnh b&aacute;o điểm m&ugrave; &ocirc; t&ocirc; chỉ gồm 2 cảm biến sau cho những xe sau đ&iacute;t đi l&ecirc;n. Hệ thống nhắc nhở kịp thời, tr&aacute;nh những c&aacute;i đ&aacute;nh l&aacute;i m&agrave; mất quan s&aacute;t.</p>', 59, 7850000, '3', 7550000, '1625569780.jpg', 4, 29, 1, '2021-07-06 04:09:40', '2021-07-06 05:02:51', 1),
(91, 'Cảm biến quanh xe PARKING S81 | 8 mắt cho DVD Android', 'cam-bien-quanh-xe-parking-s81-8-mat-cho-dvd-android', '<p>08 mắt cảm biến đo khoảng c&aacute;ch từ xe đến vật cản;</p>\r\n\r\n<p>&ndash; 02 bộ CPU xử l&yacute; t&iacute;n hiệu v&agrave; m&atilde; h&oacute;a t&iacute;n hiệu;</p>\r\n\r\n<p>&ndash; 01 bộ điều hợp cho đầu android S80;</p>\r\n\r\n<p>&ndash; Một n&uacute;t bấm zin theo xe với k&iacute;ch thước, kiểu d&aacute;ng, m&agrave;u sắc đ&egrave;n nền, độ s&aacute;ng đều được ICAR VIỆT NAM căn chỉnh lại để h&ograve;a hợp với m&agrave;u của đ&eacute;n n&uacute;t nguy&ecirc;n bản tr&ecirc;n xe (hiện tại chỉ hỗ trợ n&uacute;t bấm zin cho xe mitshubishi, trong thời gian tới ICAR sẽ cập nhật th&ecirc;m c&aacute;c mẫu n&uacute;t bấm zin theo xe kh&aacute;c. Ở thời điểm hiện tại c&aacute;c xe chưa c&oacute; n&uacute;t bấm ZIN c&oacute; thể sử dụng n&uacute;t bấm đa năng);</p>\r\n\r\n<p>&ndash; 01 mũi khoan kho&eacute;t lỗ ph&ugrave; hợp cho việc lắp đặt c&aacute;c mắt cảm biến v&agrave;o vỏ xe;</p>\r\n\r\n<p>&ndash; 01 d&acirc;y c&aacute;p nguồn 12V;</p>', 100, 6550000, '3', 0, '1625569862.jpg', 4, 29, 1, '2021-07-06 04:11:02', '2021-07-06 04:11:02', 0),
(92, 'Phim cách nhiệt 3M chính hãng', 'phim-cach-nhiet-3m-chinh-hang', '<p>&ndash; Giảm đến 99,9 % tia cực t&iacute;m, 97% tia hồng ngoại sức n&oacute;ng của mặt trời t&aacute;c động trực tiếp l&ecirc;n k&iacute;nh xe.</p>\r\n\r\n<p>&ndash; Tiết kiệm nhi&ecirc;n liệu l&ecirc;n đến 30% điện năng m&aacute;y lạnh.</p>\r\n\r\n<p>&ndash; Giảm ch&oacute;i, giảm nắng n&oacute;ng kh&ocirc;ng g&acirc;y hạn chế tầm nh&igrave;n của ngưới ngồi trong xe.</p>\r\n\r\n<p>&ndash; Bảo vệ l&agrave;n da kh&ocirc;ng bị đen, xạm da, cũng như chống lại c&aacute;c t&aacute;c nh&acirc;n g&acirc;y ung thư da&hellip;do tia Uv g&acirc;y ra.</p>\r\n\r\n<p>&ndash; Bảo vệ nội thất c&oacute; b&ecirc;n trong xe được bền bỉ hơn, kh&ocirc;ng bị bay m&agrave;u, bong tr&oacute;c, nổ&hellip;</p>\r\n\r\n<p>&ndash; Mang lại kh&ocirc;ng gian ri&ecirc;ng tư, thoải m&aacute;i, k&iacute;n đ&aacute;o người ngo&agrave;i kh&ocirc;ng thể nh&igrave;n xuy&ecirc;n v&agrave;o trong xe được.</p>\r\n\r\n<p>&ndash; T&iacute;nh an to&agrave;n cao, bởi khi kh&ocirc;ng may bị va đạp b&ecirc;n ngo&agrave;i v&agrave;o hay tai nạn ngo&agrave;i mong muốn&hellip;l&uacute;c n&agrave;y tấm phim c&aacute;ch nhiệt sẽ giữ lại những mảnh k&iacute;nh vỡ, gi&uacute;p ch&uacute;ng kh&ocirc;ng bay v&agrave;o người ngồi trong xe cũng như đồ nội thất kh&ocirc;ng bị x&acirc;y xước.</p>\r\n\r\n<p>&ndash; Tạo cảm gi&aacute;c thoải m&aacute;i, m&aacute;t dịu khi ngồi b&ecirc;n trong xe.</p>\r\n\r\n<p>&ndash; Tăng thẩm mỹ, mang lại vẻ đẹp sang trọng cho xe.</p>\r\n\r\n<p>&ndash; Sản phẩm c&oacute; tuổi thọ l&acirc;u d&agrave;i, bền đẹp.</p>\r\n\r\n<p>&ndash; Chế độ bảo h&agrave;nh d&agrave;i hạn l&ecirc;n đến 10 năm.</p>', 200, 7600000, '2', 0, '1625569980.jpg', 4, 18, 1, '2021-07-06 04:13:00', '2021-07-06 04:51:34', 0),
(93, 'Phim cách nhiệt Classis AUDI A1 | 100% Chính Hãng', 'phim-cach-nhiet-classis-audi-a1-100-chinh-hang', '<p>&ndash; Giảm đến 99,9 % tia cực t&iacute;m, 97% tia hồng ngoại sức n&oacute;ng của mặt trời t&aacute;c động trực tiếp l&ecirc;n k&iacute;nh xe.</p>\r\n\r\n<p>&ndash; Tiết kiệm nhi&ecirc;n liệu l&ecirc;n đến 30% điện năng m&aacute;y lạnh.</p>\r\n\r\n<p>&ndash; Giảm ch&oacute;i, giảm nắng n&oacute;ng kh&ocirc;ng g&acirc;y hạn chế tầm nh&igrave;n của ngưới ngồi trong xe.</p>\r\n\r\n<p>&ndash; Bảo vệ l&agrave;n da kh&ocirc;ng bị đen, xạm da, cũng như chống lại c&aacute;c t&aacute;c nh&acirc;n g&acirc;y ung thư da&hellip;do tia Uv g&acirc;y ra.</p>\r\n\r\n<p>&ndash; Bảo vệ nội thất c&oacute; b&ecirc;n trong xe được bền bỉ hơn, kh&ocirc;ng bị bay m&agrave;u, bong tr&oacute;c, nổ&hellip;</p>\r\n\r\n<p>&ndash; Mang lại kh&ocirc;ng gian ri&ecirc;ng tư, thoải m&aacute;i, k&iacute;n đ&aacute;o người ngo&agrave;i kh&ocirc;ng thể nh&igrave;n xuy&ecirc;n v&agrave;o trong xe được.</p>\r\n\r\n<p>&ndash; T&iacute;nh an to&agrave;n cao, bởi khi kh&ocirc;ng may bị va đạp b&ecirc;n ngo&agrave;i v&agrave;o hay tai nạn ngo&agrave;i mong muốn&hellip;l&uacute;c n&agrave;y tấm phim c&aacute;ch nhiệt sẽ giữ lại những mảnh k&iacute;nh vỡ, gi&uacute;p ch&uacute;ng kh&ocirc;ng bay v&agrave;o người ngồi trong xe cũng như đồ nội thất kh&ocirc;ng bị x&acirc;y xước.</p>\r\n\r\n<p>&ndash; Tạo cảm gi&aacute;c thoải m&aacute;i, m&aacute;t dịu khi ngồi b&ecirc;n trong xe.</p>\r\n\r\n<p>&ndash; Tăng thẩm mỹ, mang lại vẻ đẹp sang trọng cho xe.</p>\r\n\r\n<p>&ndash; Sản phẩm c&oacute; tuổi thọ l&acirc;u d&agrave;i, bền đẹp.</p>\r\n\r\n<p>&ndash; Chế độ bảo h&agrave;nh d&agrave;i hạn l&ecirc;n đến 10 năm.</p>', 349, 8600000, '2', 0, '1625570108.jpg', 4, 18, 1, '2021-07-06 04:15:08', '2021-07-06 05:02:51', 1),
(94, 'Camera 360 DCT cho Mercedes', 'camera-360-dct-cho-mercedes', '<p>Camera 360 DCT c&oacute; thể quay h&agrave;nh tr&igrave;nh kể cả khi xe tắt m&aacute;y, gi&aacute;m s&aacute;t t&igrave;nh h&igrave;nh xung quanh xe 24/24. Bất kỳ h&agrave;nh động n&agrave;o ảnh hưởng đến xe của bạn sẽ đều được ghi lại một c&aacute;ch r&otilde; r&agrave;ng. V&agrave; c&ograve;n cực th&ocirc;ng minh khi điện &aacute;p Acquy dưới 12.4 ampe, camera sẽ tự động tắt để bảo vệ ắc quy.</p>', 50, 9850000, '2', 0, '1625570369.jpg', 4, 21, 1, '2021-07-06 04:19:29', '2021-07-06 04:19:29', 0),
(95, 'Camera 360 DCT cho Toyota Camry', 'camera-360-dct-cho-toyota-camry', '<p>Camera 360 DCT c&oacute; thể quay h&agrave;nh tr&igrave;nh kể cả khi xe tắt m&aacute;y, gi&aacute;m s&aacute;t t&igrave;nh h&igrave;nh xung quanh xe 24/24.</p>\r\n\r\n<p>Bất kỳ h&agrave;nh động n&agrave;o ảnh hưởng đến xe của bạn sẽ đều được ghi lại một c&aacute;ch r&otilde; r&agrave;ng.</p>\r\n\r\n<p>V&agrave; c&ograve;n cực th&ocirc;ng minh khi điện &aacute;p Acquy dưới 12.4 ampe, camera sẽ tự động tắt để bảo vệ ắc quy.</p>', 50, 7560000, '2', 0, '1625570452.jpg', 4, 21, 1, '2021-07-06 04:20:52', '2021-07-06 04:20:52', 0),
(96, 'Màn Android Gotech', 'man-android-gotech', '<p>Camera 360 DCT c&oacute; thể quay h&agrave;nh tr&igrave;nh kể cả khi xe tắt m&aacute;y, gi&aacute;m s&aacute;t t&igrave;nh h&igrave;nh xung quanh xe 24/24.</p>\r\n\r\n<p>Bất kỳ h&agrave;nh động n&agrave;o ảnh hưởng đến xe của bạn sẽ đều được ghi lại một c&aacute;ch r&otilde; r&agrave;ng.</p>\r\n\r\n<p>V&agrave; c&ograve;n cực th&ocirc;ng minh khi điện &aacute;p Acquy dưới 12.4 ampe, camera sẽ tự động tắt để bảo vệ ắc quy.</p>', 200, 10550000, '3', 10000000, '1625570556.jpg', 4, 21, 1, '2021-07-06 04:22:36', '2021-07-06 04:22:36', 0),
(97, 'Màn Hình Android Elliview S4 – Camera 360 độ liền màn thế hệ mới', 'man-hinh-android-elliview-s4-??camera-360-do-lien-man-the-he-moi', '<ul>\r\n	<li>T&iacute;ch hợp sẵn hệ thống camera 360 độ</li>\r\n	<li>Đ&aacute;nh l&aacute;i theo v&ocirc; lăng (xe c&oacute; c&acirc;n bằng điện tử)</li>\r\n	<li>CPU: UIS7862 8 Nh&acirc;n 1.8GHz</li>\r\n	<li>Ram + Rom: 3GB+32GB</li>\r\n	<li>M&agrave;n h&igrave;nh: 1280&times;720 QLED</li>\r\n	<li>Kết nối: Bluetooth, 4G LTE + WIFI</li>\r\n</ul>\r\n\r\n<p>Tặng k&egrave;m:</p>\r\n\r\n<ul>\r\n	<li>Phần mềm bản đồ Vietmap S1</li>\r\n	<li>Mặt dưỡng</li>\r\n	<li>USB 16GB</li>\r\n</ul>', 49, 18980000, '3', 0, '1625571016.jpg', 4, 15, 1, '2021-07-06 04:30:16', '2021-07-06 05:02:51', 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `producttype`
--

CREATE TABLE `producttype` (
  `id` int(10) UNSIGNED NOT NULL,
  `idCategory` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `producttype`
--

INSERT INTO `producttype` (`id`, `idCategory`, `name`, `slug`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 'TOYOTA', 'toyota', 1, '2021-04-27 09:17:35', '2021-04-27 09:17:35'),
(2, 1, 'VinFast', 'vinfast', 1, '2021-04-27 09:17:45', '2021-04-27 09:17:45'),
(3, 1, 'Mercedes-Benz', 'mercedes-benz', 1, '2021-04-27 09:21:37', '2021-07-05 21:16:52'),
(4, 1, 'KIA', 'kia', 1, '2021-04-27 09:21:46', '2021-04-27 09:21:46'),
(5, 2, 'HUYNDAI', 'huyndai', 1, '2021-04-27 09:22:12', '2021-04-27 09:22:12'),
(6, 4, 'Cần gạt mưa', 'can-gat-mua', 1, '2021-04-27 09:22:33', '2021-04-27 09:22:33'),
(7, 4, 'Vè che mưa', 've-che-mua', 1, '2021-04-28 01:32:45', '2021-04-28 01:32:45'),
(8, 4, 'Logo các hãng xe', 'logo-cac-hang-xe', 1, '2021-04-28 01:33:09', '2021-04-28 01:33:09'),
(9, 1, 'BMW', 'bmw', 1, '2021-05-12 06:28:40', '2021-05-12 06:28:40'),
(10, 2, 'DAEWOO', 'daewoo', 1, '2021-05-12 06:29:23', '2021-05-12 06:29:23'),
(11, 2, 'Chevrolet', 'chevrolet', 1, '2021-05-12 06:29:51', '2021-05-12 06:29:51'),
(12, 1, 'Audi', 'audi', 1, '2021-05-12 06:31:00', '2021-05-12 06:31:00'),
(13, 4, 'Camera hành trình', 'camera-hanh-trinh', 1, '2021-05-12 06:58:32', '2021-05-12 06:58:32'),
(14, 4, 'Bọc vô lăng', 'boc-vo-lang', 1, '2021-05-12 06:58:44', '2021-05-12 06:58:44'),
(15, 4, 'Màn hình Androi DVD', 'man-hinh-androi-dvd', 1, '2021-05-12 06:59:07', '2021-05-12 06:59:07'),
(16, 1, 'Mazda', 'mazda', 1, '2021-05-18 03:30:04', '2021-05-18 03:30:04'),
(17, 4, 'Nước hoa - Túi thơm dành cho ô tô', 'nuoc-hoa-tui-thom-danh-cho-o-to', 1, '2021-05-18 05:54:43', '2021-05-18 05:54:43'),
(18, 4, 'Film cách nhiệt', 'film-cach-nhiet', 1, '2021-05-18 05:55:29', '2021-05-18 05:55:29'),
(20, 2, 'TOYOTA', 'toyota', 1, '2021-06-29 00:53:07', '2021-06-29 00:53:07'),
(21, 4, 'Camera 360', 'camera-360', 1, '2021-06-30 09:50:18', '2021-06-30 09:50:18'),
(23, 2, 'KIA', 'kia', 1, '2021-07-01 06:08:37', '2021-07-01 06:08:37'),
(24, 2, 'Mercedes-Benz', 'mercedes-benz', 1, '2021-07-01 06:08:58', '2021-07-05 21:18:28'),
(25, 2, 'BMW', 'bmw', 1, '2021-07-01 06:09:24', '2021-07-06 03:20:23'),
(26, 2, 'Mazda', 'mazda', 1, '2021-07-01 06:09:42', '2021-07-01 06:09:42'),
(27, 2, 'VinFast', 'vinfast', 1, '2021-07-01 06:09:58', '2021-07-01 06:09:58'),
(28, 4, 'Cảm biến áp suât lốp', 'cam-bien-ap-suat-lop', 1, '2021-07-01 06:16:21', '2021-07-01 06:16:21'),
(29, 4, 'Cảm biến va chạm', 'cam-bien-va-cham', 1, '2021-07-01 06:16:39', '2021-07-01 06:16:39'),
(30, 4, 'Bọc táp lô', 'boc-tap-lo', 1, '2021-07-01 06:17:10', '2021-07-01 06:17:10'),
(31, 4, 'Gối tựa đầu', 'goi-tua-dau', 1, '2021-07-01 06:17:35', '2021-07-01 06:17:35'),
(32, 4, 'Hiển thị thông tin HUD', 'hien-thi-thong-tin-hud', 1, '2021-07-01 06:18:00', '2021-07-01 06:18:00'),
(33, 4, 'Móc chìa khóa ô tô', 'moc-chia-khoa-o-to', 1, '2021-07-01 06:18:27', '2021-07-01 06:18:27'),
(34, 4, 'Đèn bi LED', 'den-bi-led', 1, '2021-07-01 06:18:52', '2021-07-01 06:18:52'),
(35, 4, 'Nước hoa - Túi thơm dành cho ô tô', 'nuoc-hoa-tui-thom-danh-cho-o-to', 1, '2021-07-01 06:19:35', '2021-07-01 06:19:35'),
(36, 4, 'Ốp bậc lên xuống', 'op-bac-len-xuong', 1, '2021-07-01 06:19:51', '2021-07-01 06:19:51'),
(37, 4, 'Máy khử mùi hôi trên xe', 'may-khu-mui-hoi-tren-xe', 1, '2021-07-01 06:20:41', '2021-07-01 06:21:07'),
(38, 4, 'Loa SUB', 'loa-sub', 1, '2021-07-01 06:21:55', '2021-07-01 06:21:55'),
(39, 1, 'Honda', 'honda', 1, '2021-07-01 06:22:39', '2021-07-01 06:22:39'),
(40, 2, 'Honda', 'honda', 1, '2021-07-01 06:22:56', '2021-07-01 06:22:56'),
(42, 1, 'HUYNDAI', 'huyndai', 1, '2021-07-05 20:47:37', '2021-07-05 20:47:37'),
(43, 1, 'Mitsubishi', 'mitsubishi', 1, '2021-07-06 03:10:41', '2021-07-06 03:10:41'),
(44, 2, 'Mitsubishi', 'mitsubishi', 1, '2021-07-06 03:10:55', '2021-07-06 03:10:55'),
(45, 1, 'Ford', 'ford', 1, '2021-07-06 03:20:56', '2021-07-06 03:20:56'),
(46, 2, 'Ford', 'ford', 1, '2021-07-06 03:21:11', '2021-07-06 03:21:11'),
(47, 1, 'Lexus', 'lexus', 1, '2021-07-06 03:26:35', '2021-07-06 03:26:35'),
(48, 2, 'Lexus', 'lexus', 1, '2021-07-06 03:26:51', '2021-07-06 03:26:51');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `social_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avarta` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role` int(11) NOT NULL DEFAULT 1,
  `status` int(11) NOT NULL DEFAULT 0,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `social_id`, `avarta`, `role`, `status`, `email_verified_at`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Chu Văn Nam Anh', 'abc@gmail.com', '$2y$10$f1uh/XU3v6qsBGGtRyhkuOQUOaIOqd1RD4A09Q24Jry2pV4UwV1jC', NULL, NULL, 1, 0, NULL, 'bMdpTAOCq4q8VMxp710jW1NPeFLB5H7snUCgdiTvSFpSxN3NUTDufxgeaHyL', '2021-05-11 20:58:38', '2021-05-11 20:58:38'),
(2, 'Nguyễn Văn B', 'b@gmail.com', '$2y$10$f1uh/XU3v6qsBGGtRyhkuOQUOaIOqd1RD4A09Q24Jry2pV4UwV1jC', NULL, NULL, 2, 0, NULL, '6ATtXE0rDCI2FhuXfr9CG7aPKgSiHvgWyNuz9dsJSfsm9mLcjcihd2izB9Ef', '2021-05-11 21:03:16', '2021-05-11 21:03:16'),
(4, 'Trần Thu Phương', 'ph@gmail.com', '$2y$10$f1uh/XU3v6qsBGGtRyhkuOQUOaIOqd1RD4A09Q24Jry2pV4UwV1jC', NULL, NULL, 3, 0, NULL, 'Vq9d1OYQeno2pPkNhrOk28XLA2oVzEF3bUERhoHPnOBW7FLA1tiXFFuEoX5R', '2021-05-12 02:00:07', '2021-05-12 02:00:07'),
(6, 'Nguyễn Văn Cường', 'cuong@gmail.com', '$2y$10$u8iqvXg9tFCcZSuaOMHmjeGba/ubjjnK67J3x.Pg2SswdMecjy8rC', NULL, NULL, 2, 0, NULL, 'soW5CJ4IDnQk5lchzYhB1tyY2qH3eMyTFHtavJ57UuKeHpKidgPyZmS2m13b', '2021-05-14 01:05:07', '2021-06-20 06:19:56'),
(11, 'Trần Quang Anh', 'anh@gmail.com', '$2y$10$7WKbRPJoMZGOCcWK21AQQ.pMD.LarsJaQzsM6CEAexSt6doNzmWpe', NULL, NULL, 0, 0, NULL, NULL, '2021-06-29 04:20:44', '2021-06-29 04:20:44'),
(12, 'Nguyễn Quang Hải', 'hai@gmail.com', '$2y$10$cXyJ0vmMIqWZ7WZMcvr0secdBx7rEa2G8byV1xGzFHo73AQU3ZNJK', NULL, NULL, 3, 0, NULL, NULL, '2021-06-29 04:47:39', '2021-06-29 04:47:39'),
(13, 'Đoàn Văn Hậu', 'hau@gmail.com', '$2y$10$y00xCnpK49OK5LAnUG8eUuE6VmYiIlarLeyQXqF5F91Jo2dPzSoNO', NULL, NULL, 3, 0, NULL, 'qHzRF1LFbqsw7H2s9c6R35rwlTC6M4Vw465D1i4GIxfdyG09OhExU19veVY1', '2021-06-29 04:47:59', '2021-06-29 04:47:59'),
(14, 'Nguyễn Trọng Hoàng', 'hoang@gmail.com', '$2y$10$CVYhXArmnS8qVhy//uYQg.p3PqMPECh4nXn2kPrnkm2.hWX3GR4Hm', NULL, NULL, 2, 0, NULL, NULL, '2021-06-29 04:48:32', '2021-06-29 04:48:32'),
(15, 'Đỗ Ngọc Anh', 'anh@gmail.com', '$2y$10$2dhaUcJijWcUb12x1HyNvOVxuYrvBt0exjMSWtg9xoh3cQaEKZwtu', NULL, NULL, 2, 0, NULL, NULL, '2021-06-29 04:48:57', '2021-06-29 04:48:57'),
(16, 'Trần Phương Anh', 'phanh@gmail.com', '$2y$10$r4uoWjagHR5WyDOhyo9oxub3pJYbxVkkqMhIjTUPU9JFD9unR.oaG', NULL, NULL, 0, 0, NULL, 'VFLbhkM8P7O07Os13tPJeZaVmre1Dl9gECpkuB4P93J5Xsaask9rWbarfMjb', '2021-06-29 04:49:22', '2021-06-29 04:49:22'),
(17, 'Nguyễn Phương Anh', 'ng@gmail.com', '$2y$10$4CtJ9lhAK7oAhEU/.fl1GeZsI8Yuew8rstGVGG6dTqqFIh8yhcxLy', NULL, NULL, 0, 0, NULL, 'tOySDvzztJvsPQeUaKg4hi1fnBm9ZQI0v7hzBf29zMI4yVnC1Tos5bUzi7g0', '2021-06-29 04:50:24', '2021-06-29 04:50:24'),
(18, 'Bùi Bích Phương', 'bui@gmail.com', '$2y$10$UwktMQ8xS74fFBvo.24cU.kHQNU4HinDZnEecrmSr3247I.Lp2p8q', NULL, NULL, 0, 0, NULL, 'exDwGkRyDzfPzn0ny36c1Po0Fag8dhuV3PjlM3u7aW6QO67TX09gWfNzL6YR', '2021-06-29 04:52:48', '2021-06-29 04:52:48'),
(19, 'Dương Gia Phong', 'phong@gmail.com', '$2y$10$3yO0i.b3ooY5m7TFfAoKxOK8kgth8uZh4sK.y2/Dy81FfcgemQODi', NULL, NULL, 0, 0, NULL, 'vJNYZVZ4cwSfMExbymfoQyHvaEwvMZyQhWzjq4iyH7DcbDLNxzasyRIsxT88', '2021-07-05 20:25:33', '2021-07-05 20:25:33'),
(20, 'Nguyễn Phong Hồng', 'hong@gmail.com', '$2y$10$o2imun1LKFiYz.NpW2HzB.p9W5ITv/3yOhsx0T/jv7Q2fL3xFH7JG', NULL, NULL, 0, 0, NULL, NULL, '2021-07-05 21:19:36', '2021-07-05 21:19:36');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `warranties`
--

CREATE TABLE `warranties` (
  `id` int(10) UNSIGNED NOT NULL,
  `idOrder` int(10) UNSIGNED NOT NULL,
  `idUser` int(11) NOT NULL,
  `idProduct` int(10) UNSIGNED NOT NULL,
  `reason` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `warranty_period` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Ngày hêt hạn bảo hành',
  `status` tinyint(1) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `warranties`
--

INSERT INTO `warranties` (`id`, `idOrder`, `idUser`, `idProduct`, `reason`, `created_at`, `updated_at`, `warranty_period`, `status`) VALUES
(9, 24, 1, 20, 'f', '2021-06-28 08:00:46', '2021-06-28 08:00:46', '2023-06-27 19:04:12', 0),
(10, 27, 1, 9, 'fffffff', '2021-06-28 08:17:42', '2021-07-05 07:06:50', '2021-09-27 20:16:45', 2),
(11, 32, 13, 15, 'khong ghi duoc hinh anh', '2021-07-05 07:08:44', '2021-07-05 07:10:49', '2022-06-30 19:21:20', 2),
(12, 36, 18, 10, 'xe bị chảy nhớt', '2021-07-05 20:24:54', '2021-07-05 20:24:54', '2024-07-01 21:06:09', 0);

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `carts`
--
ALTER TABLE `carts`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `order`
--
ALTER TABLE `order`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `orderdetail`
--
ALTER TABLE `orderdetail`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `producttype`
--
ALTER TABLE `producttype`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `warranties`
--
ALTER TABLE `warranties`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_ProductWarrant` (`idProduct`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `category`
--
ALTER TABLE `category`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT cho bảng `customer`
--
ALTER TABLE `customer`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT cho bảng `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT cho bảng `order`
--
ALTER TABLE `order`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT cho bảng `orderdetail`
--
ALTER TABLE `orderdetail`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=114;

--
-- AUTO_INCREMENT cho bảng `product`
--
ALTER TABLE `product`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=98;

--
-- AUTO_INCREMENT cho bảng `producttype`
--
ALTER TABLE `producttype`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT cho bảng `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT cho bảng `warranties`
--
ALTER TABLE `warranties`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- Các ràng buộc cho các bảng đã đổ
--

--
-- Các ràng buộc cho bảng `warranties`
--
ALTER TABLE `warranties`
  ADD CONSTRAINT `FK_ProductWarrant` FOREIGN KEY (`idProduct`) REFERENCES `product` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
